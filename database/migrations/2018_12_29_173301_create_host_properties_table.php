<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('host_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status');
            $table->integer('owner_id');
            $table->string('owner_name');
            $table->string('host_phone');
            $table->string('host_email');
            $table->string('street_address');
            $table->string('condo_apt_no')->nullable();
            $table->string('city');
            $table->string('state_id');
            $table->string('zip_code');
            $table->string('country_id');
            $table->boolean('pet');
            $table->boolean('smoke');
            $table->text('comment')->nullable();
            $table->string('created_by');
            $table->integer('place_type_id');
            $table->integer('accommodates');
            $table->integer('space_id');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('host_properties');
    }
}
