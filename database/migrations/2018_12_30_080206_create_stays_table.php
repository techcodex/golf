<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stays', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default(1);
            $table->integer('availability_id');
            $table->text('availability_summary');
            $table->integer('event_id');
            $table->string('event_name');
            $table->timestamp('event_start_date');
            $table->timestamp('event_end_date');
            $table->integer('host_id');
            $table->string('host_name');
            $table->integer('caddie_id');
            $table->string('caddie_name');
            $table->integer('host_property_id');
            $table->timestamp('stay_start_date');
            $table->timestamp('stay_end_date');
            $table->text('stay_notes');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stays');
    }
}
