<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('availabilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->integer('host_property_id');
            $table->string('display_name');
            $table->integer('host_id');
            $table->string('host_city');
            $table->string('host_street');
            $table->string('event_name');
            $table->integer('event_id');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->text('comment')->nullable();
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('availabilities');
    }
}
