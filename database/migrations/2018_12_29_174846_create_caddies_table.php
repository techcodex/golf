<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaddiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caddies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default(0);
            $table->tinyInteger('featured')->default(0);
            $table->integer('user_id');
            $table->string('caddie_name');
            $table->string('phone');
            $table->string('email');
            $table->string('pga_lpga_id');
            $table->string('pga_lpga');
            $table->boolean('pet');
            $table->boolean('smoke');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caddies');
    }
}
