<?php

use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Caddie',
            'last_name'=>'Sohaib',
            'type'=> '1',
            'home_number'=>'090078601',
            'cell_number'=>'+923237556100',
            'email' => 'caddie@admin.com',
            'profile_picture'=>'images/placeholder.png',
            'created_by'=>'admin',
            'updated_by'=>'admin',
            'email_verified_at' => now(),
            'password' => bcrypt('asdfghjk'), // secret
            'remember_token' => str_random(10),
        ]);

        User::create([
            'first_name' => 'Host',
            'last_name'=>'Sohaib',
            'type'=>'2',
            'home_number'=>'090078601',
            'cell_number'=>'+923237556100',
            'email' => 'host@admin.com',
            'profile_picture'=>'images/placeholder.png',
            'created_by'=>'admin',
            'updated_by'=>'admin',
            'email_verified_at' => now(),
            'password' => bcrypt('asdfghjk'), // secret
            'remember_token' => str_random(10),
        ]);

        User::create([
            'first_name' => 'Admin',
            'last_name'=>'Sohaib',
            'type'=>'3',
            'home_number'=>'090078601',
            'cell_number'=>'+923237556100',
            'email' => 'admin@admin.com',
            'profile_picture'=>'images/placeholder.png',
            'created_by'=>'admin',
            'updated_by'=>'admin',
            'email_verified_at' => now(),
            'password' => bcrypt('asdfghjk'), // secret
            'remember_token' => str_random(10),
        ]);

        User::create([
            'first_name' => 'CasaDeCaddie',
            'last_name'=>'Support',
            'type'=>'3',
            'home_number'=>'+923237556100',
            'cell_number'=>'+923237556100',
            'email' => 'support@casadecaddie.com',
            'profile_picture'=>'images/placeholder.png',
            'created_by'=>'admin',
            'updated_by'=>'admin',
            'email_verified_at' => now(),
            'password' => bcrypt('asdfghjk'), // secret
            'remember_token' => str_random(10),
        ]);
    }
}
