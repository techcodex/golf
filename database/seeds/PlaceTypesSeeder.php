<?php

use App\Place_type;
use Illuminate\Database\Seeder;

class PlaceTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Place_type::create([
            'name'=>'Apartment'
        ]);

        Place_type::create([
            'name'=>'house',
        ]);

        Place_type::create([
            'name'=>'Other'
        ]);
    }
}
