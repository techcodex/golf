<?php

use App\Space;
use Illuminate\Database\Seeder;

class SpaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Space::create([
            'name'=>'Entire Space',
        ]);
        Space::create([
           'name'=>'Private Room',
        ]);
        Space::create([
           'name'=>'Shared',
        ]);
    }
}
