<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [

        'first_name' => 'admin',
        'last_name'=>'admin',
        'type'=>'3',
        'first_name' => 'sohaib',
        'last_name'=>'amjad',
        'type'=> '1',
        'home_number'=>'090078601',
        'cell_number'=>'03237556100',
        'email' => 'admin@admin.com',
        'profile_picture'=>'images/placeholder.png',
        'created_by'=>'admin',
        'updated_by'=>'admin',
        'email_verified_at' => now(),
        'password' => bcrypt('asdfghjk'), // secret
        'remember_token' => str_random(10),

    ];
});

$factory->define(App\Event::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'type'=>'1',
        'source'=>'lpga.com',
        'start_date' => '2019-01-10',
        'end_date'=>'2019-02-10',
        'created_by'=>'admin',
        'updated_by'=>'admin',
        'country_id'=>'1',
        'state_id'=>'1',
        'city'=>$faker->city,
        'country_club_name'=>'Indiana',
        'event_image'=>'images/events/demo.png',
        'slug'=>'a'
    ];
});

$factory->define(App\Message::class, function(Faker $faker){
    $to = 0;
    $from = 0;
    do {
        $to = rand(1,3);
        $from = rand(1,3);
    }while($to == $from);

    return [
        'to' => $to,
        'from' => $from,
        'message' => $faker->sentence(),
        'read_at' => null
    ];
});
