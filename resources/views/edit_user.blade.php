@extends('layouts.dashboard')

@section('header-styles')
<link href="{{asset('jasny/jasny-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
@stop

@section('main-section')
    @if($errors->count())
        @foreach($errors->all() as $error)
            {{$error}}
         @endforeach
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session()->get('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session()->get('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <form method="POST" action="{{route("web.user.update",['id'=>$user->id])}}" class="col-md-12" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="first_name">First Name</label>
            <input autofocus type="text" name="first_name" value="{{$user->first_name}}" class="form-control @if($errors->has('first_name')) is-invalid @endif" id="first_name">
            @if($errors->has('first_name'))
                <div class="invalid-feedback">
                    {{ $errors->first('first_name') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text" name="last_name" id="last_name" value="{{$user->last_name}}" class="form-control @if($errors->has('last_name')) is-invalid @endif">
            @if($errors->has('new_password'))
                <div class="invalid-feedback">
                    {{ $errors->first('last_name') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="last_name">Email</label>
            <input type="email" name="email" id="email" value="{{$user->email}}" class="form-control @if($errors->has('email')) is-invalid @endif">
            @if($errors->has('email'))
                <div class="invalid-feedback">
                    {{ $errors->first('email') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="last_name">Cell Number</label>
            <input type="text" name="cell_number" value="{{$user->cell_number}}" id="cell_number" class="form-control @if($errors->has('cell_number')) is-invalid @endif">
            @if($errors->has('cell_number'))
                <div class="invalid-feedback">
                    {{ $errors->first('cell_number') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="last_name">Home Number</label>
            <input type="text" name="home_number" value="{{$user->home_number}}" id="home_number" class="form-control @if($errors->has('home_number')) is-invalid @endif">
            @if($errors->has('home_number'))
                <div class="invalid-feedback">
                    {{ $errors->first('home_number') }}
                </div>
            @endif
        </div>
        <div class="form-group offset-md-2">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview img-thumbnail" data-trigger="fileinput" style="width: 300px; height: 200px;">
                    @if($user->profile_picture)
                        <img src="{{asset($user->profile_picture)}}" alt="profile Picture">

                    @endif
                </div>
                <div>
                <span class="btn btn-outline-secondary btn-file">
                  <span class="fileinput-new">Select image</span>
                  <span class="fileinput-exists">Change</span>
                  <input type="file" name="myImage">
                </span>
                    <a href="#" class="btn btn-outline-secondary fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="submit" value="Update Profile" class="btn btn-sm btn-success offset-md-4">
        </div>
    </form>
@stop

@section('footer-scripts')

<script src="{{asset('jasny/jasny-bootstrap.min.js')}}"></script>

@stop