@extends('layouts.app_home')


@section('header-styles')


@stop

@section('main-section')

    <div class="page_content_wrap page_paddings_no">
        <div class="content">
            <article class="post_item post_item_single page">
                <div class="post_content">
                    <section>
                        <div class="content_container">
                            <div class="sc_content content_wrap margin_top_large margin_bottom_xmedium" data-animation="animated fadeInUp normal">
                                <h2 class="sc_title margin_top_null margin_bottom_small aligncenter">COME &amp; JOIN Casa De Caddie</h2>
                                <div class="sc_section fsz-1_286em lh-1_3em fw-100">
                                    <div class="sc_section_inner">
                                        <div class="text_column crtext">
                                            <p class="aligncenter f_st-ital lightgrey_color">What we offer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="sc_section sc_section_block margin_top_tiny aligncenter w70_per">
                                    <div class="sc_section_inner">
                                        <div class="sc_iconseparator ">
                                            <div class="sc_iconseparator_item"><span class="sc_iconseparator_holder sc_iconseparator_holder_left"><span class="sc_iconseparator_line"></span></span>
                                                <div class="sc_iconseparator_element_outer">
                                                    <div class="sc_iconseparator_element_iner">
                                                        <span class="icon-vectorsmartobject fsz-3_3em darkgrey_color fw-100"></span>
                                                    </div>
                                                </div><span class="sc_iconseparator_holder sc_iconseparator_holder_right"><span class="sc_iconseparator_line"></span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div  class="sc_services_wrap">
                                    <div class="sc_services sc_services_style_services-1 sc_services_type_icons margin_top_tiny w100_per">
                                        <div class="sc_columns columns_wrap">
                                            <div class="column-1_4 column_padding_bottom">
                                                <div class="sc_services_item odd first">
                                                    <a href="#"><span class="sc_icon icon-trx-envelope54"></span></a>
                                                    <div class="sc_services_item_content">
                                                        <h4 class="sc_services_item_title"><a href="#">Caddie</a></h4>
                                                        <div class="sc_services_item_description">
                                                            <p>Register to become a member and be on your way to being paired with a host at
                                                                an upcoming Tour Event.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><div class="column-1_4 column_padding_bottom">
                                                <div class="sc_services_item even">
                                                    <a href="#"><span class="sc_icon icon-trx-profile29"></span></a>
                                                    <div class="sc_services_item_content">
                                                        <h4 class="sc_services_item_title"><a href="#">FACILITIES</a></h4>
                                                        <div class="sc_services_item_description">
                                                            <p>Hosts list available space near a Tour Event. Caddie make reservation for
                                                                lodging.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><div class="column-1_4 column_padding_bottom">
                                                <div class="sc_services_item odd">
                                                    <a href="#"><span class="sc_icon icon-trx-envelope54"></span></a>
                                                    <div class="sc_services_item_content">
                                                        <h4 class="sc_services_item_title"><a href="#">Host Signup</a></h4>
                                                        <div class="sc_services_item_description">
                                                            <p>Register to host a tour player or caddie at a pro tournament near you.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><div class="column-1_4 column_padding_bottom">
                                                <div class="sc_services_item even">
                                                    <a href="#"><span class="sc_icon icon-trx-flag86"></span></a>
                                                    <div class="sc_services_item_content">
                                                        <h4 class="sc_services_item_title"><a href="#">Events</a></h4>
                                                        <div class="sc_services_item_description">
                                                            <p>Stay up to date about upcoming tour events.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="container-fluid sc_bg_img_5">
                            <div class="row_inv inverse_colors">
                                <div class="sc_content content_wrap margin_top_xlarge" data-animation="animated fadeInUp normal">
                                    <h2 class="sc_title margin_top_null margin_bottom_small aligncenter">STAY LIKE A PRO</h2>
                                    <div class="sc_section fsz-1_286em lh-1_3em fw-100">
                                        <div class="sc_section_inner">
                                            <div class="text_column crtext">
                                                <p class="aligncenter f_st-ital lightgrey_color">Casa De Caddie Registration</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sc_section sc_section_block margin_top_tiny margin_bottom_small aligncenter w70_per">
                                        <div class="sc_section_inner">
                                            <div class="sc_iconseparator">
                                                <div class="sc_iconseparator_item"><span class="sc_iconseparator_holder sc_iconseparator_holder_left"><span class="sc_iconseparator_line"></span></span>
                                                    <div class="sc_iconseparator_element_outer">
                                                        <div class="sc_iconseparator_element_iner"><span class="icon-nation fsz-3_3em darkgrey_color fw-100"></span></div>
                                                    </div><span class="sc_iconseparator_holder sc_iconseparator_holder_right"><span class="sc_iconseparator_line"></span></span>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sc_section fsz-1_714em lh-1_3em fw-100">
                                        <div class="sc_section_inner">
                                            <div class="text_column crtext">
                                                <p class="aligncenter f_st-ital">Casa de Caddie, helps alleviate the lodging costs incurred as a result of travelling on the PGA/LPGA Tours. If a caddie’s player isn’t playing well, the caddie’s pay is minimal. Some caddies have occasionally slept in their cars to avoid costs of staying in hotels. The goal of Casa de Caddie is to pair these tour professionals with host families who are willing to open their homes at pro tour events.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sc_section mb450 sc_section_block margin_top_medium aligncenter">
                                        <div class="sc_section_inner">
                                            <a href="{{route('register')}}" class="sc_button sc_button_style_filled sc_button_size_small margin_right_xsmall">JOIN US </a><a href="{{route('event.list')}}" class="sc_button sc_button_style_filled sc_button_size_small">MAKE RESERVATION</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="content_container">
                            <div class="sc_content content_wrap margin_top_xlarge margin_bottom_medium" data-animation="animated fadeInUp normal">
                                <h2 class="sc_title margin_top_null margin_bottom_small aligncenter">OUR BLOG</h2>
                                <div class="sc_section sc_section_block margin_top_tiny margin_bottom_small aligncenter w70_per">
                                    <div class="sc_section_inner">
                                        <div class="sc_iconseparator">
                                            <div class="sc_iconseparator_item"><span class="sc_iconseparator_holder sc_iconseparator_holder_left"><span class="sc_iconseparator_line"></span></span>
                                                <div class="sc_iconseparator_element_outer">
                                                    <div class="sc_iconseparator_element_iner">
                                                        <span class="icon-vectorsmartobject fsz-3_3em darkgrey_color fw-100"></span>
                                                    </div>
                                                </div>
                                                <span class="sc_iconseparator_holder sc_iconseparator_holder_right"><span class="sc_iconseparator_line"></span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget_area sc_recent_news_wrap vc_recent_news wpb_content_element">
                                    <aside class="widget widget_recent_news">
                                        <div class="sc_recent_news sc_recent_news_style_news-magazine sc_recent_news_with_accented margin_top_null margin_bottom_null">
                                            <div class="columns_wrap">
                                                <div class="column-1_2" style="width:49%;">
                                                    <article class="post_item post_layout_news-magazine">
                                                        <div class="post_featured">
                                                            <div class="post_thumb" data-image="images/image-6.jpg" data-title="When Does Your Golf Season End?">
                                                                <a class="hover_icon hover_icon_link" href="{{route('blog.caddieLife')}}">
                                                                    <img alt="My Life As Caddie" src="{{asset('images/My-Life-as-a-Caddie.jpg')}}">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post_header entry-header">
                                                            <h5 class="post_title entry-title"><a href="{{route('blog.caddieLife')}}">My Life as Caddie</a></h5>
                                                            <div class="post_meta">
                                                                <span class="post_meta_author">Admin</span><span class="post_meta_date"><a href="#">March 17, 2016</a></span>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div><div class="column-1_2">
                                                    <article class="post_item post_layout_news-magazine">
                                                        <div class="post_featured">
                                                            <div class="post_thumb" data-image="images/image-17.jpg" data-title="How to Avoid Slow Play in Golf">
                                                                <a class="hover_icon hover_icon_link" href="{{route('blog.caddieRights')}}">
                                                                    <img  alt="How to Avoid Slow Play in Golf" src="{{asset('images/homeBlog.jpg')}}">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post_header entry-header">
                                                            <h5 class="post_title entry-title"><a href="{{route('blog.caddieRights')}}">Why caddies are fighting for their Rights ?</a></h5>
                                                            <div class="post_meta">
                                                                <span class="post_meta_author">Admin</span><span class="post_meta_date"><a href="#">March 17, 2016</a></span>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="content_container">
                            <div class="sc_content content_wrap margin_top_large margin_bottom_large" data-animation="animated fadeInUp normal">
                                <h2 class="sc_title margin_top_null margin_bottom_small aligncenter">FEATURED PLAYERS</h2>
                                <div class="sc_section fsz-1_286em lh-1_3em fw-100">
                                    <div class="sc_section_inner">
                                        <div class="text_column crtext">
                                            <p class="aligncenter f_st-ital lightgrey_color">What can you do at the N7 Golf Club?</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="sc_section sc_section_block margin_top_tiny margin_bottom_medium aligncenter w70_per">
                                    <div class="sc_iconseparator ">
                                        <div class="sc_iconseparator_item"><span class="sc_iconseparator_holder sc_iconseparator_holder_left"><span class="sc_iconseparator_line"></span></span>
                                            <div class="sc_iconseparator_element_outer">
                                                <div class="sc_iconseparator_element_iner">
                                                    <span class="icon-vectorsmartobject2 fsz-3_3em darkgrey_color fw-100"></span>
                                                </div>
                                            </div><span class="sc_iconseparator_holder sc_iconseparator_holder_right"><span class="sc_iconseparator_line"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="sc_team_wrap">
                                    <div class="sc_team sc_team_style_team-1 w100_per">
                                        <div class="sc_columns columns_wrap">
                                            <div class="column-1_3 column_padding_bottom">
                                                <div class="sc_team_item odd first">
                                                    <div class="sc_team_item_avatar">
                                                        <img alt="Cory Etter" src="images/team-3-370x370.jpg">
                                                        <div class="sc_team_item_hover">
                                                            <div class="sc_team_item_socials">
                                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                                    <div class="sc_socials_item"><a href="#" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="sc_team_item_info">
                                                        <h5 class="sc_team_item_title"><a href="team-single.html">Cory Etter</a></h5>
                                                        <div class="sc_team_item_position">Pine Hill Country Club</div>
                                                    </div>
                                                </div>
                                            </div><div class="column-1_3 column_padding_bottom">
                                                <div class="sc_team_item even">
                                                    <div class="sc_team_item_avatar">
                                                        <img  alt="Eddie Grant" src="images/team-2-370x370.jpg">
                                                        <div class="sc_team_item_hover">
                                                            <div class="sc_team_item_socials">
                                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                                    <div class="sc_socials_item"><a href="#" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="sc_team_item_info">
                                                        <h5 class="sc_team_item_title"><a href="team-single.html">Eddie Grant</a></h5>
                                                        <div class="sc_team_item_position">Bel-Air Country Club</div>
                                                    </div>
                                                </div>
                                            </div><div class="column-1_3 column_padding_bottom">
                                                <div class="sc_team_item odd">
                                                    <div class="sc_team_item_avatar">
                                                        <img alt="Terri Jimenez" src="images/team-1-370x370.jpg">
                                                        <div class="sc_team_item_hover">
                                                            <div class="sc_team_item_socials">
                                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                                    <div class="sc_socials_item"><a href="#" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="sc_team_item_info">
                                                        <h5 class="sc_team_item_title"><a href="team-single.html">Terri Jimenez</a></h5>
                                                        <div class="sc_team_item_position">Butterfield Country Club</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="content_container">
                            <div class="sc_content content_wrap margin_top_large margin_bottom_large" data-animation="animated fadeInUp normal">
                                <h2 class="sc_title margin_top_null margin_bottom_small aligncenter">OUR CADDIES</h2>
                                <div class="sc_section fsz-1_286em lh-1_3em fw-100">
                                    <div class="sc_section_inner">
                                    </div>
                                </div>
                                <div class="sc_section sc_section_block margin_top_tiny margin_bottom_medium aligncenter w70_per">
                                    <div class="sc_iconseparator ">
                                        <div class="sc_iconseparator_item"><span class="sc_iconseparator_holder sc_iconseparator_holder_left"><span class="sc_iconseparator_line"></span></span>
                                            <div class="sc_iconseparator_element_outer">
                                                <div class="sc_iconseparator_element_iner">
                                                    <span class="icon-vectorsmartobject2 fsz-3_3em darkgrey_color fw-100"></span>
                                                </div>
                                            </div><span class="sc_iconseparator_holder sc_iconseparator_holder_right"><span class="sc_iconseparator_line"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="sc_team_wrap">
                                    <div class="sc_team sc_team_style_team-1 w100_per">
                                        <div class="sc_columns columns_wrap">
                                            <div class="column-1_3 column_padding_bottom">
                                                <div class="sc_team_item odd first">
                                                    <div class="sc_team_item_avatar">
                                                        <img alt="Cory Etter" src="{{asset('images/caddie2.png')}}">
                                                        <div class="sc_team_item_hover">
                                                            <div class="sc_team_item_socials">
                                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                                    <div class="sc_socials_item"><a href="#" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="sc_team_item_info">
                                                        <h5 class="sc_team_item_title"><a href="team-single.html">Cory Etter</a></h5>
                                                        <div class="sc_team_item_position">Pine Hill Country Club</div>
                                                    </div>
                                                </div>
                                            </div><div class="column-1_3 column_padding_bottom">
                                                <div class="sc_team_item even">
                                                    <div class="sc_team_item_avatar">
                                                        <img  alt="Eddie Grant" src="{{asset('images/Caddie1.png')}}">
                                                        <div class="sc_team_item_hover">
                                                            <div class="sc_team_item_socials">
                                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                                    <div class="sc_socials_item"><a href="#" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="sc_team_item_info">
                                                        <h5 class="sc_team_item_title"><a href="team-single.html">Eddie Grant</a></h5>
                                                        <div class="sc_team_item_position">Bel-Air Country Club</div>
                                                    </div>
                                                </div>
                                            </div><div class="column-1_3 column_padding_bottom">
                                                <div class="sc_team_item odd">
                                                    <div class="sc_team_item_avatar">
                                                        <img alt="Terri Jimenez" src="{{asset('images/teamcaddie.jpg')}}" height="300px">
                                                        <div class="sc_team_item_hover">
                                                            <div class="sc_team_item_socials">
                                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                                    <div class="sc_socials_item"><a href="#" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="sc_team_item_info">
                                                        <h5 class="sc_team_item_title"><a href="team-single.html">Terri Jimenez</a></h5>
                                                        <div class="sc_team_item_position">Butterfield Country Club</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="cL"></div>
                </div>
            </article>
        </div>
    </div>
@stop


@section('footer-scripts')


@stop



