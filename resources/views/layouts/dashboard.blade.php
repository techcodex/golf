<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/x-icon" href="{{asset('images/favicon.png')}}"/>
    <title>{{$page_title}} - {{ config('app.name', 'Laravel') }}</title>
    <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Scripts -->

    <!-- Fonts -->
    {{-- <link rel="dns-prefetch" href="https://fonts.gstatic.com"> --}}
    {{-- <link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet"> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <style>
        /* body {
            font-family: 'Poppins', sans-serif;
        } */
        .navbar-laravel{
            margin-bottom: 20px;
        }

        li a {
            color: black;
        }

        li a:hover {
            color: #0c5460;
            text-decoration: none;
        }

        .list-item {
            margin-left: 10px;
            border-left: 1px solid #bbb;
            padding-left: 10px;
        }

        i {
            width: 12px;
        }

        .b-r {
            border-right: 1px solid #ccc;
        }

        @media only screen and (max-width: 767px){
            .b-r {
                border-right: none;
                border-bottom: 1px solid #ccc;
            }
        }
    </style>

    <!-- Styles -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    @yield('header-styles')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ route('caddie.dashboard') }}">
                    Caddie De Case | {{$dashboard}}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <li class="nav-item">
                            <a href="{{url('/')}}" class="nav-link">Home</a>
                        </li>
                            @if(Auth::check())
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{Auth::user()->first_name}} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

                            @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-lg-3 mt-3">
                    @include('common.sidebar')
                </div>
                <div class="col-lg-9 mt-3 mb-2">
                    <div class="card">
                        <div class="card-body">
                            <h1>{{$page_heading}}</h1>
                            <hr>
                            @yield('main-section')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    <script src="{{asset('js/toastr.min.js')}}"></script>
    <script>
        toastr.options.closeButton = true;
        toastr.options.closeMethod = 'fadeOut';
        toastr.options.preventDuplicates = true;
        toastr.options.progressBar = true;
        @if(Session::has('success'))
            toastr.success({{Session::get('success')}});
         @endif
         @if(Session::has('error'))
         toastr.error({{Session::get('error')}});
        @endif
        @if(Session::has('info'))
        toastr.info({{Session::get('info')}});
        @endif
    </script>
    @routes
@yield('footer-scripts')
</body>

</html>
