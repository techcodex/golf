<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $page_title }} | {{ config('app.name') }} </title>
    <link rel="icon" type="image/x-icon" href="{{asset('images/favicon.png')}}"/>
    <link property="stylesheet" rel='stylesheet' href='{{ asset('css/fontello/css/fontello.css') }}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{ asset('js/vendor/woocommerce/assets/css/woocommerce-smallscreen.css') }}' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='{{ asset('js/vendor/woocommerce/assets/css/woocommerce-layout.css') }}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{ asset('js/vendor/woocommerce/assets/css/woocommerce.css') }}' type='text/css' media='all' />

    @yield('header-main-style')

    <link property="stylesheet" rel='stylesheet' href='{{ asset('css/style.css') }}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{ asset('js/vendor/woocommerce/assets/css/plugin.woocommerce.css') }}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{ asset('css/core.animation.css') }}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{ asset('js/vendor/magnific/magnific-popup.css') }}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{ asset('css/template.shortcodes.css') }}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{ asset('css/skin.css') }}' type='text/css' media='all'/>
    <link property="stylesheet" rel='stylesheet' href='{{ asset('css/plugin.instagram-widget.css') }}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{ asset('css/responsive.css') }}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{ asset('css/skin.responsive.css') }}' type='text/css' media='all'/>
    <link property="stylesheet" rel='stylesheet' href='{{asset('css/toastr.min.css')}}' type='text/css' media='all'/>
    <link property="stylesheet" rel='stylesheet' href='https://use.fontawesome.com/releases/v5.6.3/css/all.css' type='text/css' media='all'/>
    @yield('header-styles')
</head>

<body class="page body_filled scheme_original sidebar_hide sidebar_outer_hide single single-tribe_events body_filled scheme_original top_panel_show sidebar_hide sidebar_outer_hide events-single tribe-events-style-full tribe-events-style-theme tribe-theme-n7-golf-club">
<div class="body_wrap">
    <div class="page_wrap">
        <div class="top_panel_fixed_wrap"></div>
        @include('common.extra_top_nav')

        <div class="page_content_wrap page_paddings_yes">
            <div class="content_wrap">
                <div class="content">
                    @yield('main-section')

                </div>
            </div>
        </div>

        @include('common.footer')
    </div>
</div>
<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>



<script type='text/javascript' src='{{asset('js/vendor/jquery-3.1.1.js')}}'></script>
<script type='text/javascript' src='{{asset('js/vendor/jquery-migrate.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/vendor/photostack/modernizr.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/vendor/superfish.js')}}'></script>
<script type="text/javascript" src="{{asset('js/custom/_main.js')}}"></script>
<script type='text/javascript' src='{{asset('js/custom/core.utils.js')}}'></script>
<script type='text/javascript' src='{{asset('js/custom/core.init.js')}}'></script>
<script type='text/javascript' src='{{asset('js/custom/template.init.js')}}'></script>
<script type='text/javascript' src='{{asset('js/custom/template.shortcodes.js')}}'></script>

{{--<script type='text/javascript' src='js/vendor/magnific/jquery.magnific-popup.min.js'></script>--}}
{{--<script type='text/javascript' src='js/vendor/core.messages/core.messages.js'></script>--}}
{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAB4BoYuLptxWMVgKR5ed4ec3M7d7s4oVo"></script>--}}
<script type='text/javascript' src='{{asset('js/custom/core.googlemap.js')}}'></script>
<script type="text/javascript" src="{{asset('js/toastr.min.js')}}"></script>
<script>
    $(document).ready(function (e) {
        @if(Session::has('success'))
        toastr.success("{{Session::pull('success')}}");
        @endif

        @if(Session::has('error'))
        toastr.error("{{Session::pull('error')}}");
        @endif

        @if(Session::has('info'))
        toastr.info("{{Session::get('info')}}");
        @endif
    });
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
@routes
@yield('footer-scripts')
</body>
</html>