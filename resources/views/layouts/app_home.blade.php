<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">

    <title>Home - {{config('app.name')}}</title>
    <link rel="icon" type="image/x-icon" href="{{asset('images/favicon.png')}}"/>
    <link property="stylesheet" rel='stylesheet' href="{{asset('css/fontello/css/fontello.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('js/vendor/essential-grid/public/assets/css/settings.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('js/vendor/essential-grid/public/assets/css/lightbox.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('js/vendor/revslider/public/assets/css/settings.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('js/vendor/woocommerce/assets/css/woocommerce-smallscreen.css')}}" type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('')}}js/vendor/woocommerce/assets/css/woocommerce-layout.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('js/vendor/woocommerce/assets/css/woocommerce.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('css/style.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('js/vendor/woocommerce/assets/css/plugin.woocommerce.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('css/core.animation.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('css/core.portfolio.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('js/vendor/magnific/magnific-popup.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('css/skin.css')}}" type='text/css' media='all'/>
    <link property="stylesheet" rel='stylesheet' href="{{asset('css/template.shortcodes.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('css/plugin.instagram-widget.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('js/vendor/swiper/swiper.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('css/responsive.css')}}" type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href="{{asset('css/skin.responsive.css')}}" type='text/css' media='all'/>
    <link property="stylesheet" rel='stylesheet' href="{{asset('css/toastr.min.css')}}" type='text/css' media='all'/>
    @yield('header-styles')
</head>

<body class="home page body_filled scheme_original top_panel_over">
<div class="body_wrap">
    <div class="page_wrap">
        <div class="top_panel_fixed_wrap"></div>
        @include('common.top_nav')

        @include('common.slider')
        <!-- Main section -->

        @yield('main-section')



        @include('common.footer')
    </div>
</div>
<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

<script type='text/javascript' src='{{asset("js/vendor/jquery-3.1.1.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/essential-grid/public/assets/js/lightbox.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/essential-grid/public/assets/js/jquery.themepunch.tools.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/essential-grid/public/assets/js/jquery.themepunch.essential.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/revslider/public/assets/js/jquery.themepunch.revolution.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/swiper/swiper.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/jquery-migrate.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/photostack/modernizr.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/superfish.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/isotope.pkgd.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/jquery.hoverdir.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/countdown/jquery.plugin.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/countdown/jquery.countdown.min.js")}}'></script>
<script type="text/javascript" src="{{asset("js/custom/_main.js")}}"></script>
<script type='text/javascript' src='{{asset("js/custom/core.utils.js")}}'></script>
<script type='text/javascript' src='{{asset("js/custom/core.init.js")}}'></script>
<script type='text/javascript' src='{{asset("js/custom/template.init.js")}}'></script>
<script type='text/javascript' src='{{asset("js/custom/template.shortcodes.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/magnific/jquery.magnific-popup.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/core.messages/core.messages.js")}}'></script>
<script type="text/javascript" src="{{asset('js/toastr.min.js')}}"></script>
<script>
    $(document).ready(function (e) {
        @if(Session::has('success'))
            toastr.success("{{Session::get('success')}}");
        @endif

        @if(Session::has('error'))
            toastr.error("{{Session::get('error')}}");
        @endif

        @if(Session::has('info'))
            toastr.info("{{Session::get('info')}}");
        @endif
    })
</script>
@yield('footer-scripts')

</body>
</html>