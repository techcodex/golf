@routes

<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <title>{{ config('app.name') }} | {{ $page_title }}</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Web font -->
    <script src="{{ asset('font/ajax/libs/webfont/1.6.16/webfont.js') }}"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
    </script>
    <!--end::Web font -->
    <link href="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--begin::Base Styles -->
    <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unpkg.com/nprogress@0.2.0/nprogress.css">
    <style>
        @media only screen and (max-width: 500px) {
            .m-brand__logo img {
                width: 30%;
            }
        }
    </style>
    @yield('styles')
    <!--end::Base Styles -->

    <link rel="icon" type="image/x-icon" href="{{asset('images/favicon.png')}}"/>
</head>
<!-- end::Head -->


<!-- begin::Body -->

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">



    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">


        <!-- BEGIN: Header -->
    @include('partials.header')
        <!-- END: Header -->


        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>

            <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
                <!-- BEGIN: Aside Menu -->
    @include('partials.aside')
                <!-- END: Aside Menu -->
            </div>
            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">{{ $page_heading }}</h3>
                        </div>
                    </div>
                </div>
                <!-- END: Subheader -->
                <div class="m-content">
                    @yield('content')
                </div>
            </div>
        </div>

        @include('partials.footer')
    </div>
    <!-- end:: Page -->

    <!-- begin::Quick Sidebar -->
    {{-- side bar lies here in case --}}
    <!-- end::Quick Sidebar -->
    <!-- begin::Scroll Top -->
    <div id="m_scroll_top" class="m-scroll-top">
        <i class="la la-arrow-up"></i>
    </div>
    <!-- end::Scroll Top -->

    <!--begin::Base Scripts -->
    <script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
    <!--end::Base Scripts -->

    <!--begin::Page Snippets -->
    <script src="{{ asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>
    <script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
    <!--end::Page Snippets -->

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        NProgress.configure({ minimum: 0.3 });

        $(document).ajaxStart(function(){
            NProgress.start();
        });

        $(document).ajaxStop(function(){
            NProgress.done();
        });

        $(document).ready(function () {
            @if(Session::has('success'))
            $.notify({
                message: "{{Session::get('success')}}"
            },{
                // settings
                type: 'success' 
            });
            @endif
            @if(Session::has('error'))
            $.notify({
                message: "{{Session::get('error')}}"
            },{
                // settings
                type: 'danger'
            });
            @endif
        });
    </script>
    <!-- end:: Body -->
    @yield('scripts')

</body>
<!-- end::Body -->

</html>
