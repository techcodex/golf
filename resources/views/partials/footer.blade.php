<footer class="m-grid__item		m-footer " style="height: 60px; !important">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                    <span class="m-footer__copyright">
                       &copy; 2019 Crafted with <i class="fa fa-heart" style="color: #f44336;"></i> by <a href="https://www.casadecaddie.com" target="_blank" class="m-link">Casadecaddie</a>
                    </span>
                </div>
            </div>
        </div>
    </footer>
