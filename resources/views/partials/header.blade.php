<header id="m_header" class="m-grid__item    m-header "  m-minimize-offset="200" m-minimize-mobile-offset="200" >
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <!-- BEGIN: Brand -->
    <div class="m-stack__item m-brand  m-brand--skin-dark ">
        <div class="m-stack m-stack--ver m-stack--general">
            <div class="m-stack__item m-stack__item--middle m-brand__logo">
                <a href="{{ route('home') }}" class="m-brand__logo-wrapper">
                     <img src="{{asset('images/logo.png')}}" alt="logo" width="70%">
                </a>
            </div>
            <div class="m-stack__item m-stack__item--middle m-brand__tools">

                    <!-- BEGIN: Left Aside Minimize Toggle -->
                    <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                        <span></span>
                    </a>
                    <!-- END -->

                                <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                    <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                        <span></span>
                    </a>
                    <!-- END -->

                <!-- BEGIN: Topbar Toggler -->
                <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                    <i class="flaticon-more"></i>
                </a>
                <!-- BEGIN: Topbar Toggler -->
            </div>
        </div>
    </div>
    <!-- END: Brand -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                                    <!-- BEGIN: Horizontal Menu -->
    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>


    <!-- END: Horizontal Menu -->								<!-- BEGIN: Topbar -->
    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">


        <div class="m-stack__item m-topbar__nav-wrapper">
            <ul class="m-topbar__nav m-nav m-nav--inline">
                <li class="m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light"  m-dropdown-toggle="click">
                    <a href="#" class="m-nav__link m-dropdown__toggle">
                        <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
                        <span class="m-nav__link-icon"><i class="flaticon-share"></i></span>
                    </a>
                    <div class="m-dropdown__wrapper">
                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__header m--align-center" style="background: url({{ asset('assets/app/media/img/misc/quick_actions_bg.jpg') }}); background-size: cover;">
                                <span class="m-dropdown__header-title">Quick Actions</span>
                                <span class="m-dropdown__header-subtitle">Shortcuts</span>
                            </div>
                            <div class="m-dropdown__body m-dropdown__body--paddingless">
                                <div class="m-dropdown__content">
                                    <div class="data" data="false.html" data-height="380" data-mobile-height="200">
                                        <div class="m-nav-grid m-nav-grid--skin-light">
                                            <div class="m-nav-grid__row">
                                            <a href="{{ route('admin.dashboard') }}" class="m-nav-grid__item">
                                                    <i class="m-nav-grid__icon flaticon-line-graph"></i>
                                                    <span class="m-nav-grid__text">Dashboard</span>
                                                </a>
                                                <a href="{{ route('user.create') }}" class="m-nav-grid__item">
                                                    <i class="m-nav-grid__icon flaticon-user-settings"></i>
                                                    <span class="m-nav-grid__text">Add New User</span>
                                                </a>
                                            </div>
                                            <div class="m-nav-grid__row">
                                            <a href="{{ route('event.create') }}" class="m-nav-grid__item">
                                                    <i class="m-nav-grid__icon fa fa-calendar-alt"></i>
                                                    <span class="m-nav-grid__text">Show New Event</span>
                                                </a>
                                                <a href="" class="m-nav-grid__item">
                                                    <i class="m-nav-grid__icon flaticon-clipboard"></i>
                                                    <span class="m-nav-grid__text">All Packages</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                    <a href="#" class="m-nav__link m-dropdown__toggle">
                    <span class="m-topbar__userpic">
                         <img src="{{ asset(Auth::user()->profile_picture) }}" class="m--img-rounded m--marginless" alt=""/>
                    </span>
                    <span class="m-topbar__username m--hide">{{ Auth::user()->name }}</span>
                    </a>
                    <div class="m-dropdown__wrapper">
                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__header m--align-center" style="background: url({{ asset('assets/app/media/img/misc/user_profile_bg.jpg') }}); background-size: cover;">
                                <div class="m-card-user m-card-user--skin-dark">
                                    <div class="m-card-user__pic">
                                        <img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt=""/>
                                        <!--
                                        <span class="m-type m-type--lg m--bg-danger"><span class="m--font-light">S<span><span>
                                        -->
                                    </div>
                                    <div class="m-card-user__details">
                                        <span class="m-card-user__name m--font-weight-500">{{ Auth::user()->name }}</span>
                                        <a href="#" class="m-card-user__email m--font-weight-300 m-link">{{ Auth::user()->email }}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav m-nav--skin-light">
                                        <li class="m-nav__section m--hide">
                                            <span class="m-nav__section-text">Section</span>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="{{ route('admin.profile.settings') }}" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                <span class="m-nav__link-title">
                                                    <span class="m-nav__link-wrap">
                                                        <span class="m-nav__link-text">My Profile</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="{{ route('admin.message') }}" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                <span class="m-nav__link-text">Messages</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__separator m-nav__separator--fit">
                                        </li>
                                        <li class="m-nav__item">
                                            <button onclick="document.getElementById('logoutForm').submit()" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                            <form id="logoutForm" action="{{ route('logout') }}" style="display: none;" method="post">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
             </ul>
        </div>
    </div>
    <!-- END: Topbar -->
</div>
            </div>
        </div>
    </header>
