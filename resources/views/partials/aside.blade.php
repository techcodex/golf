<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1"
    m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <li class="m-menu__item  @if($active == 'home') m-menu__item--active @endif" aria-haspopup="true">
            <a href="{{ route('admin.dashboard') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">Dashboard</span>
                            {{-- <span class="m-menu__link-badge">
                                <span class="m-badge m-badge--danger">2</span>
                            </span> --}}
                        </span>
                    </span>
                 </a>
        </li>
        <li class="m-menu__section ">
            <h4 class="m-menu__section-text">Actions</h4>
            <i class="m-menu__section-icon flaticon-more-v2"></i>
        </li>
        <li class="m-menu__item  m-menu__item--submenu @if($active == 'add-user' || $active == 'show-user' || $active== 'caddies' || $active == 'host') m-menu__item--open @endif"
            aria-haspopup="true">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-user-settings"></i>
                    <span class="m-menu__link-text">Users</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item   m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                                <span class="m-menu__link-text">Users</span>
                        </span>
                    </li>
                    <li class="m-menu__item @if($active == 'add-user') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{ route('user.create') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Add New User</span>
                        </a>
                    </li>
                    <li class="m-menu__item @if($active == 'show-user') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{ route('user.users_list') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Show All User</span>
                        </a>
                    </li>
                    <li class="m-menu__item @if($active == 'caddies') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{ route('caddie.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Show All Caddies</span>
                        </a>
                    </li>
                    <li class="m-menu__item @if($active == 'host') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{ route('host.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Show All Hosts</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="m-menu__item  m-menu__item--submenu @if($active == 'add-event' || $active == 'show-event' || $active == 'pga-event' || $active == 'lpga-event') m-menu__item--open @endif"
            aria-haspopup="true">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon fa fa-calendar-alt"></i>
                    <span class="m-menu__link-text">Events</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item   m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                                <span class="m-menu__link-text">Events</span>
                        </span>
                    </li>
                    <li class="m-menu__item @if($active == 'add-event') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{route('event.create')}}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Add New Event</span>
                        </a>
                    </li>
                    <li class="m-menu__item @if($active == 'show-event') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{ route('event.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Show All Events</span>
                        </a>
                    </li>
                    <li class="m-menu__item @if($active == 'pga-event') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{ route('admin.event.pga') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Show All Pga Events</span>
                        </a>
                    </li>
                    <li class="m-menu__item @if($active == 'lpga-event') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{ route('admin.event.lpga') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Show All Lpga Events</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="m-menu__item  m-menu__item--submenu @if($active == 'availability') m-menu__item--open @endif"
            aria-haspopup="true">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon fa fa-eye"></i>
                <span class="m-menu__link-text">Availability</span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item   m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                                <span class="m-menu__link-text">Availabilities</span>
                        </span>
                    </li>
                    <li class="m-menu__item @if($active == 'add-event') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{route('admin.availability.index')}}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">All Availabilities</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="m-menu__item  m-menu__item--submenu @if($active == 'stay') m-menu__item--open @endif"
            aria-haspopup="true">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon fa fa-bed"></i>
                <span class="m-menu__link-text">Stays</span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item   m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                                <span class="m-menu__link-text">Stays</span>
                        </span>
                    </li>
                    <li class="m-menu__item @if($active == 'add-event') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{route('admin.stay.index')}}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">All Stays</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="m-menu__item  m-menu__item--submenu @if($active == 'add-plan' || $active == 'show-plan') m-menu__item--open @endif"
            aria-haspopup="true">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-open-box"></i>
                    <span class="m-menu__link-text">Plans & Billing</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item   m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                                <span class="m-menu__link-text">Plans & Billing</span>
                        </span>
                    </li>
                    <li class="m-menu__item @if($active == 'add-plan') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{route('plan.create')}}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Add New Plan</span>
                        </a>
                    </li>
                    <li class="m-menu__item @if($active == 'show-plan') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{ route('plan.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Show All Plan</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="m-menu__item  m-menu__item--submenu @if($active == 'add-blog' || $active == 'show-blog') m-menu__item--open @endif"
            aria-haspopup="true">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon flaticon-car"></i>
                    <span class="m-menu__link-text">Blogs</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item   m-menu__item--parent" aria-haspopup="true">
                        <span class="m-menu__link">
                                <span class="m-menu__link-text">Blogs</span>
                        </span>
                    </li>
                    <li class="m-menu__item @if($active == 'add-blog') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{route('blog.create')}}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Add New Blog</span>
                        </a>
                    </li>
                    <li class="m-menu__item @if($active == 'show-blog') m-menu__item--active @endif" aria-haspopup="true">
                        <a href="{{ route('blog.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Show All Blogs</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>


        <li class="m-menu__item  @if($active == 'message') m-menu__item--active  @endif" aria-haspopup="true">
            <a href="{{ route('admin.message') }}" class="m-menu__link ">
                <i class="m-menu__link-icon socicon-mail"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Bulk Message</span>
                    </span>
                </span>
            </a>
        </li>
        <li class="m-menu__item  @if($active == 'settings') m-menu__item--active  @endif" aria-haspopup="true">
            <a href="{{ route('admin.profile.settings') }}" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-cogwheel-1"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Settings</span>
                    </span>
                </span>
            </a>
        </li>

    </ul>
</div>
