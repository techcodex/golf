@extends('layouts.dashboard')


@section('header-styles')

@stop

@section('main-section')
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session()->get('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session()->get('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <form method="POST" action="{{route('hostproperty.dashboard.update',['id'=>$property->id])}}" class="col-md-12 " enctype="multipart/form-data">
        @csrf
        <center><h3>Owner Information</h3></center>
        <div class="form-group">
            <label for="owner_name">Owner Name</label>
            <input autofocus type="text" name="fullName" value="{{Auth::user()->first_name." ". Auth::user()->last_name }}" class="form-control @if($errors->has('owner_name')) is-invalid @endif" id="owner_name">
            @if($errors->has('fullName'))
                <div class="invalid-feedback">
                    {{ $errors->first('fullName') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" name="email" value="{{Auth::user()->email }}" class="form-control @if($errors->has('email')) is-invalid @endif" id="email">
            @if($errors->has('email'))
                <div class="invalid-feedback">
                    {{ $errors->first('email') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="phone">Phone</label>
            <input type="text" name="phone" @if(old('phone') == null)value="{{Auth::user()->cell_number }}"@endif @if(old('phone') != null) value="{{old('phone')}}" @endif class="form-control @if($errors->has('phone')) is-invalid @endif" id="phone">
            @if($errors->has('phone'))
                <div class="invalid-feedback">
                    {{ $errors->first('phone') }}
                </div>
            @endif
        </div>
        <hr>

        <div class="form-group">
            <label for="street_address">Street Address</label>
            <input type="text" name="streetAddress" placeholder="Street Address" @if(old('streetAddress')) value="{{old('streetAddress')}}" @endif @if($property->street_address != "") value="{{$property->street_address}}" @endif class="form-control @if($errors->has('streetAddress')) is-invalid @endif" id="streetAddress">
            @if($errors->has('streetAddress'))
                <div class="invalid-feedback">
                    {{ $errors->first('streetAddress') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="street_address">Condo / Apt #</label>
            <input autofocus type="text" name="apt" placeholder="Enter Condo Apt #" @if(old('apt')) value="{{old('apt')}}" @endif @if($property->condo_apt_no != "") value="{{$property->condo_apt_no}}" @endif class="form-control @if($errors->has('apt')) is-invalid @endif" id="condo_apt_no">
            @if($errors->has('apt'))
                <div class="invalid-feedback">
                    {{ $errors->first('apt') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="street_address">No Of Person Accommodates ?</label>
            <input  type="text" name="accommodates" placeholder="How many person your house accomodates" @if(old('accommodates')) value="{{old('accommodates')}}" @endif @if($property->accommodates != "") value="{{$property->accommodates}}" @endif class="form-control @if($errors->has('accommodates')) is-invalid @endif" id="accommodates">
            @if($errors->has('accommodates'))
                <div class="invalid-feedback">
                    {{ $errors->first('accommodates') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="street_address">City</label>
            <input type="text" name="city" placeholder="City" @if(old('city')) value="{{old('city')}}" @endif @if($property->city != "") value="{{$property->city}}" @endif class="form-control @if($errors->has('city')) is-invalid @endif" id="city">
            @if($errors->has('city'))
                <div class="invalid-feedback">
                    {{ $errors->first('city') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="country">Select Country</label>
            <select name="country" id="country_id" class="form-control @if($errors->has('country')) is-invalid @endif" >
                <option value="">-- Select Country --</option>
                <option value="1">Pakistan</option>
                @foreach($countries as $country)
                    <option value="{{$country->id}}" @if($property->country_id == $country->id ) selected @endif>{{$country->name}}</option>
                @endforeach
            </select>
            @if($errors->has('country'))
                <div class="invalid-feedback">
                    {{ $errors->first('country') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="state">Select State</label>
            <select name="state" id="state_id" class="form-control @if($errors->has('state')) is-invalid @endif">
                <option value="">-- Select State --</option>
            </select>
            @if($errors->has('state'))
                <div class="invalid-feedback">
                    {{$errors->first('state')}}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="zip_code">Zip Code</label>
            <input type="text" name="zip" placeholder="Zip Code" @if($property->zip_code != "") value="{{$property->zip_code}}" @endif @if(old('zip')) value="{{old('zip')}}" @endif class="form-control @if($errors->has('zip')) is-invalid @endif" id="zip">
            @if($errors->has('zip'))
                <div class="invalid-feedback">
                    {{ $errors->first('zip') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="places">Type of Place ?</label>
            <select name="place_type_id" class="form-control @if($errors->has('place_type_id')) is-invalid @endif">
                <option value="">-- Select Option --</option>
                @foreach($types as $type)
                    <option value="{{$type->id}}" @if($property->place_type_id == $type->id) selected @endif>{{$type->name}}</option>
                @endforeach
            </select>
            @if($errors->has('place_type_id'))
                <div class="invalid-feedback">
                    {{ $errors->first('place_type_id') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="places">Space Offered ?</label>
            <select name="space_id" class="form-control @if($errors->has('space_id')) is-invalid @endif">
                <option value="">-- Select Option --</option>
                @foreach($spaces as $space)
                    <option value="{{$space->id}}" @if($property->space_id == $space->id) selected @endif>{{$space->name}}</option>
                @endforeach
            </select>
            @if($errors->has('space_id'))
                <div class="invalid-feedback">
                    {{ $errors->first('space_id') }}
                </div>
            @endif
        </div>
        <hr>
        <center><h3>Accommodation Information</h3></center>

        <div class="form-group">
            <label for="places">Do You Have Smoke ?</label>
            <select name="smoke" class="form-control @if($errors->has('smoke')) is-invalid @endif">
                <option value="">-- Select Option --</option>
                <option value="0" @if($property->smoke == 0) selected @endif>Yes</option>
                <option value="1" @if($property->smoke == 1) selected @endif>No</option>
            </select>
            @if($errors->has('smoke'))
                <div class="invalid-feedback">
                    {{ $errors->first('smoke') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="places">Do You Have Pets (Cat/Dog) ?</label>
            <select name="pet" class="form-control @if($errors->has('pet')) is-invalid @endif">
                <option value="">-- Select Option --</option>
                <option value="0" @if($property->smoke == 0) selected @endif>Yes</option>
                <option value="1" @if($property->smoke == 1) selected @endif>No</option>
            </select>
            @if($errors->has('pet'))
                <div class="invalid-feedback">
                    {{ $errors->first('pet') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="comments">Any Comments</label>
            <input type="text" name="comments" @if($property->comment  != "") value="{{$property->comment}}" @endif placeholder="Comments ... " @if(old('comments')) value="{{old('comments')}}" @endif class="form-control @if($errors->has('comments')) is-invalid @endif" id="comment">
            @if($errors->has('comments'))
                <div class="invalid-feedback">
                    {{ $errors->first('comments') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="comments">Property Images</label>
            <input type="file" name="property_images[]" multiple class="form-control @if($errors->has('property_images.0')) is-invalid @endif" id="property_images">
            @if($errors->has('property_images.0'))
                <div class="invalid-feedback">
                    {{ $errors->first('property_images.0') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <input type="submit" value="Edit Property" class="btn btn-primary offset-md-5">
        </div>

    </form>
@stop

@section('footer-scripts')
<script>
    $(document).ready(function (e) {
        var id = $("#country_id").val();
        if(id == "") {
            return ;
        }
        var data = {
            id:id,
        };
        $.ajax({
            url:"{{route('get_states')}}",
            dataType:'JSON',
            type:'POST',
            data:data,
            complete:function (jqXHR,textStatus) {
                if(jqXHR.status == 200) {
                    var result = JSON.parse(jqXHR.responseText);
                    if(result.hasOwnProperty('success')) {
                        var output = "";
                        result.states.forEach(function (state) {
                            output +="<option value='"+state.id+"'>"+state.name+"</option>";
                        });
                        $("#state_id > option~option").remove();
                        $("#state_id").append(output);
                    }
                } else {
                    alert("Contact Admin"+jqXHR.status);
                }
            }
        });

        $("#country_id").change(function (e) {
            var id = $("#country_id").val();
            if(id == "") {
                return ;
            }
            var data = {
                id:id,
            };
            $.ajax({
                url:"{{route('get_states')}}",
                dataType:'JSON',
                type:'POST',
                data:data,
                complete:function (jqXHR,textStatus) {
                    if(jqXHR.status == 200) {
                        var result = JSON.parse(jqXHR.responseText);
                        if(result.hasOwnProperty('success')) {
                            var output = "";
                            result.states.forEach(function (state) {
                                output +="<option value='"+state.id+"'>"+state.name+"</option>";
                            });
                            $("#state_id > option~option").remove();
                            $("#state_id").append(output);
                        }
                    } else {
                        alert("Contact Admin"+jqXHR.status);
                    }
                }
            });
        });
    });
</script>
@stop
