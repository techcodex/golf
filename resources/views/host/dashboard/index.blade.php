@extends('layouts.dashboard')


@section('header-styles')
{{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">

@stop


@section('main-section')
    <div  class="">
        <table class="table table-bordered text-center" id="propertiesData">
            <thead class="bg-primary" style="color:white">
                <tr>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Phone</td>
                    <td>Place Type</td>
                    <td>Address</td>
                    <td>Space</td>
                    <td>Edit/Delete</td>
                </tr>
            </thead>
        </table>
    </div>
@stop


@section('footer-scripts')
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script>
        var propertiesTable;
         $(document).ready(function(e) {
             $("#propertiesData").click(function (e) {
                 if(e.target.classList.contains('delete')) {
                     var id = e.target.getAttribute('data-id');
                     $.ajax({
                         url:route('host.dashboard.destroy',{id:id}),
                         dataType:'JSON',
                         type:'GET',
                         complete:function (jqXHR,textStatus) {
                             if(jqXHR.status == 200) {
                                 var result = JSON.parse(jqXHR.responseText);
                                 if(result.hasOwnProperty('success')) {
                                     alert("Property Deleted Successfully");
                                     propertiesTable.api().ajax.reload(null);
                                 } else if(result.hasOwnProperty('error')) {
                                     alert(result.msg);
                                 }
                             } else {
                                 alert("Error");
                             }
                         }
                     });
                 }
             });
           propertiesTable =  $("#propertiesData").dataTable({
                responsive: !0,
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax:"{{route('host.dashboard.get_properties')}}",
                columns:[
                    {data:'owner_name',name:'owner_name'},
                    {data:'host_email',name:'host_email'},
                    {data:'host_phone',name:'host_phone'},
                    {data:'place_type_id',name:'place_type_id'},
                    {data:'street_address',name:'street_address'},
                    {data:'space_id',name:'space_id'},
                    {data:'action'}
                ],
                columnDefs: [{
                    targets: -1,
                    title: "Edit/Delete",
                    orderable: !1,
                    render: function (a, e, t, n) {
                        var EditRoute = route('hostproperty.dashboard.edit',{id:t.id});
                        return "<a href='"+EditRoute+"' class='btn-sm btn-primary'>Edit</a>&nbsp;&nbsp;<a href='#' data-id='"+t.id+"' class='btn-sm btn-danger delete'>Delete</a> ";
                    }
                }],

            });

        });

    </script>
@stop