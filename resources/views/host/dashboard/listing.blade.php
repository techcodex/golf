@extends('layouts.dashboard')


@section('header-styles')
    {{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">

@stop


@section('main-section')
    <div  class="container">
        <table class="table table-bordered text-center" id="tripsData">
            <thead class="bg-primary" style="color:white;width: 100%;">
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Phone</td>
                <td>Price</td>
                <td>Place Type</td>
                <td>Address</td>
                <td>Space</td>
                <td>Status</td>
            </tr>
            </thead>
            <tbody>
                @foreach($properties as $property)
                    <tr>
                        <td>{{$property->owner_name}}</td>
                        <td>{{$property->host_email}}</td>
                        <td>{{$property->host_phone}}</td>
                        @if($property->price == "")
                            <td>0</td>
                        @else
                        <td>{{$property->price}}</td>
                        @endif
                        <td>{{$property->place_type->name}}</td>
                        <td>{{$property->street_address}}</td>
                        <td>{{ $property->space->name }}</td>
                        @if($property->status == 1)
                            <td><span class="badge badge-primary">Avaialble</span></td>
                        @else
                            <td><span class="badge badge-success">Booked</span></td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop


@section('footer-scripts')
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $("#tripsData").dataTable();
    </script>
@stop