@extends('layouts.app')

@section('header-styles')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
@stop

@section('main-section')
<div class="page_content_wrap page_paddings_yes">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single page">
                <div class="post_content">
                    <section>
                        <div class="container">
                            <div class="sc_table aligncenter ltext margin_top_null margin_bottom_null w100_per" data-animation="animated fadeInUp normal">
                                <p></p>
                                <table id="propertyData">
                                    <tbody>
                                        <tr>
                                            <td width="15%">Owner Name</td>
                                            <td width="15%">Host Email</td>
                                            <td width="15%">Host Phone</td>
                                            <td width="35%">Place Type</td>
                                            <td width="35%">Address</td>
                                            <td width="15%">Edit/Delete</td>
                                        </tr>
                                        @foreach ($properties as $property)
                                            <tr>
                                                <td>{{ $property->owner_name }}</td>
                                                <td>{{ $property->host_email }}</td>
                                                <td>{{ $property->host_phone }}</td>
                                                <td>{{$property->place_type->name}}</td>
                                                <td>{{ $property->street_address }}
                                                    <br/>
                                                <span class="crtext-spanTable">{{ $property->city }}</span><br>
                                                <span class="crtext-spanTable">{{ $property->country_id }}</span>
                                                </td>
                                                <td>
                                                    <a href="{{route('hostproperty.dashboard.edit',['id'=>$property->id])}}"><i class="fa fa-edit" style="color: #FFC107;"></i></a>
                                                    <a href="{{route('host.destroy',['id'=>$property->id])}}"><i class="fa fa-trash" style="color: red;"></i></a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <p></p>
                            </div>
                        </div>
                    </section>
                </div>
            </article>
        </div>
    </div>
</div>

@stop
@section('footer-scripts')
<script>
    $("document").ready(function (e) {
        $('html,body').animate({
            scrollTop:$("#propertyData").offset().top
        }, 2000);
    });
</script>

@stop
