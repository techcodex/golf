@extends('layouts.app')

@section('header-styles')
@stop

@section('main-section')

<div class="sc_section margin_top_large margin_bottom_xxxhuge aligncenter w70_per" data-animation="animated fadeInUp normal">
    <div class="sc_section_inner">
        <h2 class="sc_title margin_top_null margin_bottom_xmedium aligncenter">Edit Host Property</h2>
        <div class="sc_form_wrap">
            <div id="sc_form_1" class="sc_form sc_form_style_form_1 cf1LeftText ">
                <form id="sc_form_1_1" data-formtype="form_1" method="post" action="{{ route('host.store') }}">
                    @csrf
                    <h3>Owner Information</h3>
                    <div class="sc_form_info">
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_firstname">Full Name:</label>
                        <input id="sc_form_firstname" @if(old('fullName') == null) value="{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}" @else value="{{ old('fullName') }}" @endif @if($errors->has('fullName')) style="border:
                            2px solid red" @endif type="text" name="fullName" placeholder="Full Name"> @if($errors->has('fullName'))
                            <span class="help-block">{{ $errors->first('fullName') }}</span> @endif
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_email">E-mail Address</label>
                            <input id="sc_form_email" type="text" @if(old('email') == null) value="{{ Auth::user()->email }}" @else value="{{ old('email') }}" @endif name="email" placeholder="E-mail Address" @if($errors->has('email'))
                            style="border: 2px solid red" @endif> @if($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span> @endif
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_firstname">Phone:</label>
                            <input id="sc_form_firstname" @if(old('phone') == null) value="{{ Auth::user()->cell_number }}" @else value="{{ old('phone') }}" @endif @if($errors->has('phone')) style="border:
                            2px solid red" @endif type="text" name="phone" placeholder="Phone"> @if($errors->has('phone'))
                            <span class="help-block">{{ $errors->first('phone') }}</span> @endif
                        </div>
                        <h3>Owner Address Information</h3>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_firstname">Street Address:</label>
                            <input id="sc_form_firstname" value="{{ old('streetAddress') }}" @if($errors->has('streetAddress')) style="border:
                            2px solid red" @endif type="text" name="streetAddress" placeholder="Street Address"> @if($errors->has('streetAddress'))
                            <span class="help-block">{{ $errors->first('streetAddress') }}</span> @endif
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_firstname">Condo/Apt #:</label>
                            <input id="sc_form_firstname" value="{{ old('apt') }}" @if($errors->has('apt')) style="border:
                            2px solid red" @endif type="text" name="apt" placeholder="Condo/Apt #"> @if($errors->has('apt'))
                            <span class="help-block">{{ $errors->first('apt') }}</span> @endif
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_firstname">City:</label>
                            <input id="sc_form_firstname" value="{{ old('city') }}" @if($errors->has('city')) style="border:
                            2px solid red" @endif type="text" name="city" placeholder="City">
                            @if($errors->has('city')) <span class="help-block">{{ $errors->first('city') }}</span> @endif
                        </div>

                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_type">Country</label>
                            <select name="country" id="sc_form_type" @if($errors->has('country')) style="width: 100%; border: 2px solid red" @else style="width: 100%; border: 2px solid #282828" @endif>
                                <option value="">-- Select Country --</option>
                                <option value="1" @if(old('country') != null && old('country') == '1') selected @endif>Pakistan</option>
                            </select>
                            @if($errors->has('country'))<span class="help-block">{{ $errors->first('country') }}</span> @endif
                        </div>

                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_type">State/Providence</label>
                            <select name="state" id="sc_form_type" @if($errors->has('state')) style="width: 100%; border: 2px solid red" @else style="width: 100%; border: 2px solid #282828" @endif>
                                <option value="">-- Select State --</option>
                                <option value="1" @if(old('state') != null && old('state') == '1') selected @endif>Punjab</option>
                            </select>
                            @if($errors->has('state'))<span class="help-block">{{ $errors->first('state') }}</span> @endif
                        </div>

                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_firstname">Zip Code:</label>
                            <input id="sc_form_firstname" value="{{ old('zip') }}" @if($errors->has('zip')) style="border:
                            2px solid red" @endif type="text" name="zip" placeholder="Zip Code/Mail Stop">
                            @if($errors->has('zip')) <span class="help-block">{{ $errors->first('zip') }}</span> @endif
                        </div>

                        <h3>Accommodation Information</h3>
                        <p style="margin-top: 15px; margin-bottom: 0; color: black; font-size: 18px;">Do You Have Pet(Dogs/Cats) ?</p>
                        <div class="sc_form_item sc_form_field label_over" style="margin-top:0;">
                            <label class="required" for="sc_form_type">Pet:</label>
                            <select name="pet" id="sc_form_type" @if($errors->has('pet')) style="width: 100%; border: 2px solid red" @else style="width: 100%; border: 2px solid #282828" @endif>
                                <option value="">-- Select Option --</option>
                                <option value="0" @if(old('pet') != null && old('pet') == '0') selected @endif>Yes</option>
                                <option value="1" @if(old('pet') != null && old('pet') == '1') selected @endif>No</option>
                            </select>
                            @if($errors->has('pet'))<span class="help-block">{{ $errors->first('pet') }}</span> @endif
                        </div>

                        <p style="margin-top: 15px; margin-bottom: 0; color: black; font-size: 18px;">Do you or members of family smoke ?</p>
                        <div class="sc_form_item sc_form_field label_over" style="margin-top:0;">
                            <label class="required" for="sc_form_type">Smoke</label>
                            <select name="smoke" id="sc_form_type" @if($errors->has('smoke')) style="width: 100%; border: 2px solid red" @else style="width: 100%; border: 2px solid #282828" @endif>
                                <option value="">-- Select Option --</option>
                                <option value="0" @if(old('smoke') != null && old('smoke') == '0') selected @endif>Yes</option>
                                <option value="1" @if(old('smoke') != null && old('smoke') == '1') selected @endif>No</option>
                            </select>
                            @if($errors->has('smoke'))<span class="help-block">{{ $errors->first('smoke') }}</span> @endif
                        </div>

                        <div class="sc_form_item sc_form_field label_over" style="margin-top: 10px;">
                            <label class="required" for="sc_form_firstname">City:</label>
                            <input id="sc_form_firstname" value="{{ old('commnets') }}" @if($errors->has('comments')) style="border:
                            2px solid red" @endif type="text" name="comments" placeholder="Any Comments">
                            @if($errors->has('comments')) <span class="help-block">{{ $errors->first('comments') }}</span> @endif
                        </div>
                    </div>
                    <div class="sc_form_item sc_form_button">
                        <button type="submit">Create Property</button>
                    </div>
                    <div class="result sc_infobox"></div>
                </form>
            </div>
        </div>
    </div>
</div>









@stop
@section('footer-scripts')
<script>
    $("document").ready(function (e) {
        $('html,body').animate({
            scrollTop:$(".sc_section_inner").offset().top
        }, 2000);
        $("#sc_form_email").focus();
    });
</script>

@stop
