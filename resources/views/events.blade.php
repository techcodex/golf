@extends('layouts.app')


@section('header-styles')


@stop


@section('main-section')
    <div class="page_content_wrap page_paddings_yes">
        <div class="content">
            <article class="post_item post_item_single page">
                <div class="post_content">
                    <section class="margin_bottom_large">
                        <div class="content_container">
                            <div class="columns_wrap sc_columns">
                                <div class="column-1_2 sc_column_item odd first" data-animation="animated fadeInLeft normal">
                                    <figure class="sc_image sc_image_shape_square w570 h477">
                                        <img src="images/page_about_us_01-570x477.jpg" alt="" />
                                    </figure>
                                </div><div class="column-1_2 sc_column_item even" data-animation="animated fadeInRight normal">
                                    <h2 class="sc_title sc_title_regular margin_top_null margin_bottom_small">WELCOME</h2>
                                    <div class="text_column">
                                        <p>N7 Golf Club opened for play in April of 1989. Our Golf Club ranked as one of the top 100 new courses in the United States. Our course was designed by the talented and successful golfer. The course offers a completely different feel, test and experience, and detail for a challenging and highly enjoyable golf course to life.</p>
                                        <p>Apart from the beautiful golf course, we offers a wide range of facilities to meet the needs of any individual or group of golfers. Facilities include:</p>
                                    </div>
                                    <ul class="sc_list sc_list_style_iconed margin_top_small">
                                        <li class="sc_list_item odd first">
                                            <span class="sc_list_icon icon-stop accent_color"></span>
                                            <div class="text_column">
                                                <p>A full-length practice fairway</p>
                                            </div>
                                        </li>
                                        <li class="sc_list_item even">
                                            <span class="sc_list_icon icon-stop accent_color"></span>
                                            <div class="text_column">
                                                <p>Large chipping and putting greens</p>
                                            </div>
                                        </li>
                                        <li class="sc_list_item odd">
                                            <span class="sc_list_icon icon-stop accent_color"></span>
                                            <div class="text_column">
                                                <p>Fully serviced Pro Shop</p>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="text_column">
                                        <p>The professional staff are on hand at all times to give lessons and to make all visitors feel welcome making sure they thoroughly enjoy their golfing holidays.</p>
                                    </div>
                                    <a href="#" class="sc_button sc_button_square sc_button_style_border_b sc_button_size_small margin_top_xsmall">Contact US</a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </article>
        </div>
    </div>
@stop


@section('footer-scripts')


@stop