@extends('layouts.app')


@section('header-styles')
    <style>
        .top{
            margin-top: 10px;
        }
    </style>

@stop


@section('main-section')
    <article class="post_item post_item_single post type-post">
        <section class="post_featured">
            <div class="post_thumb" data-image="images/image-2.jpg" data-title="What’s Most Important in a Golf Course">
                <center>
                    <img  alt="What’s Most Important in a Golf Course" src="{{asset('images/image-2-1170x659.jpg')}}">
                </center>
            </div>
        </section>
        <section class="post_content">
            <h2><u>My Life As a Caddy:</u></h2>
            <p class="top">
                Newspaper sports sections are big on nostalgia. They like nothing better than to take readers down memory lane, where fabled rivalries are recounted by their gimpy survivors, and “great moments in sports” are captured in grainy old action shots. But, as the indispensable Nation columnist Dave Zirin often reminds us, the world of sports also involves work that’s boring, poorly compensated and hardly remembered. A few sports-related jobs have even fallen victim to automation. For example, caddying on golf courses — the money-making pursuit of my teenage years — was rendered nearly obsolete by the widespread use of motorized four-wheel vehicles and two-wheel pull carts.
            </p>
            <p class="top">
                Much to my surprise, caddying is now making a comeback. Bill Morris’s recent New York Times story, “Polishing an Antique Notion, A Club Recruits Caddies,” instantly triggered old memories for me, as well as amazement at the modern form of caddy recruitment and career development. Morris describes an effort by private clubs in the New York metropolitan area “to attract and train new caddies” — reviving a trade he traces back to the “ambulatory valets” who began “carrying clusters of clubs through the seaside sand and gorse” for “men clubbing little balls on the eastern coast of Scotland some six centuries ago.” Over time, a good caddie became “a packhorse, a companion, a course expert, and a psychologist.” When good caddies weren’t juggling all those roles, writes Morris, they were “a tireless maintenance machine, replacing divots, raking sand traps, and repairing ball marks on greens—all chores many cart-riding golfers often forgo.”
            </p>
            <p class="top">
                But clubs can’t discourage golfers from motoring around like members of Rommel’s tank corps if there’s no one available to lug their 40-pound leather bags. That’s why private club managers, teaching pros and surviving caddy masters are trolling local high schools, trying to raise a new model army of “ambulatory valets.” At one weekend training academy, raw recruits were “shown a video, given some instruction, then taken out on a course to watch a golfer and a caddie interact.” At the Lawrence Yacht and Country Club, where Peter Procops teaches golf, a caddy can make $30 for carrying a single bag for eighteen holes (a four or five hour tour of duty depending on how crowded the course is). Every year, about 200 caddies now benefit from a $1.5 million scholarship fund, set up by the golf associations of New Jersey, Westchester County, and Long Island as an additional fringe benefit.
            </p>
            <p class="top">
                To boost this precious supply of labor at his own club, Procops has enlisted the help of high school guidance counselors with this message:
            </p>
            <section class="post_author author vcard">
                <div class="post_author_info">
                    <p class="top">“Caddying is very much a mirror of life. It teaches kids how to be around adults. It gets them out of their shells and they learn a trade. We’re taking kids and turning them into young adults.”</p>
                </div>
            </section>
            <p class="top">
                Rich Uva, president of the Metropolitan Caddie Masters Association, offers even more promising vistas to the “kids from the inner city, from the Bronx and Westchester” that he steers toward his Quaker Ridge Golf Club in Scarsdale:
            </p>
            <section class="post_author author vcard">
                <div class="post_author_info">
                    <p class="top">“[No] matter who you are, there’s something beneficial about caddying. Money might be the main thing, but there’s the experience of meeting people. I can’t tell you how many times I’ve seen a golfer take a liking to a caddy and then hire him to come work for him. That’s priceless.”</p>
                </div>
            </section>
            <p class="top">
                These earnest revivers of the caddy shack are absolutely right that the job provides an opportunity for “meeting people” in a workplace that’s still a “mirror of life.” But, when I caddied at the Winged Foot Golf Club in the mid-1960s, what I saw in the “mirror” were the perks and privileges of the power elite and the hardscrabble world of my older coworkers, who caddied full-time.s
            </p>
            <p class="top">
                Winged Foot is located in a Westchester suburb that could have been the setting for Mad Men or the fiction of John Cheever. Wealthy WASPS and their upscale Irish-Catholic neighbors took the “Stamford local” to Grand Central Station, worked in Manhattan, and played golf on weekends, while their wives stayed at home to look after the kids (or played golf on weekday mornings). African Americans were confined to a small neighborhood on the proverbial “other side of the tracks.” Working-class whites, mainly Italian American, staffed the local public safety departments, while their male relatives worked as gardeners or construction workers. There was some cross-class mingling and even a little racial integration at the local high school, where about 30 percent of the graduates were headed for military service, beauty school, or some blue-collar trade, instead of the Ivy League, the state university, or a community college.
            </p>
            <p class="top">
                Designed in 1921 by famous course architect A.W. Tillinghast and the scene of major championships like the U.S. Open and the PGA tournament, Winged Foot was strictly “invitation only.” When I worked there, no blacks, Jews, Asians, or Latinos — no matter how rich — had ever been admitted. Even moneyed Italian-Americans were in very short supply. Women got to play there only by virtue of their husbands’ membership.
            </p>
            <p class="top">
                My job at the club, along with a multi-racial crew of adult males and other kids my own age, lacked any regularly scheduled hours or hiring hall protections. At Winged Foot, you showed up when you wanted, sat in the caddy pen, and waited for the caddy master to call your name and send you out on a job. You could wait four minutes, forty minutes, or four hours — or, on a slow day, go home not having worked at all. It was a pure shape up system. Everything depended on the whim of the caddy master, including whether you got to carry one bag or two. If the caddy master didn’t like you, you spent a lot of time on the bench. For new caddies, there was no job training, testing, or instructional videos to watch; it was all trial and error on the job. If you were lucky, one of the “professionals” — a nomadic breed of drinkers, gamblers, and golf hustlers — would give you a tip or two about how not to fuck up and get fired.
            </p>
            <p class="top">
                These older full-timers in our midst were assured of more regular work and always went out “carrying doubles.” This meant that their minimum pay, before tips, totaled $10 for bag toting that could last four or five hours, depending on how busy the course was and how badly or slowly the members played. They were the only real “course experts” or “golf psychologists” in our ranks. They took pride in their work and actively discouraged their younger coworkers from engaging in the kind of juvenile delinquency (rifling a member’s bag, swinging one of his clubs, and stealing golf balls or cigarettes) that were firing offenses. They were always a much-respected source of elder wisdom about card playing, dice throwing, and the fine art of reading racing forms. Every winter, they migrated to Florida for caddying jobs at private clubs and only returned when the snow had melted and the fairways were dry enough for members to play again.
            </p>
            <p class="top">
                During my tenure at Winged Foot, the caddy master was a fat, gravel-voiced, cigar smoker named Gene Hayden. Gruff and unsmiling, Gene bore a striking resemblance to George Meany, then president of the AFL-CIO. He always showed up for work dressed like a club member, in white shoes, golf shirt, and pants that were some ridiculous shade of pink or yellow. He rarely removed his stogie from his mouth or strayed from his perch on a stool at the front of the caddy pen. He sweated profusely, even in the shade, wiped his brow often with a monogrammed handkerchief, and wielded absolute power with the help of a single microphone.
            </p>
            <p class="top">
                When a member alerted him that a particular foursome was ready to play and needed caddies to collect their bags and report to the first tee, Gene would bark out the last names of those getting the assignment into the mic, along with the names of the members whose bags were to be picked up. Unfortunately, his announcement of these two critical pieces of information was often inaudible due to his own growly delivery and the poor condition of the sound system. This frequently led to much confusion; more than one caddy popped up mistakenly thinking he had finally landed work for the day. You might show up up at the first tee with the wrong member’s bag, or the right bag at the wrong tee (The club had easily confused East and West courses.) Neither mistake was a good way to start the day.
            </p>
            <p class="top">
                Because there was no formal roster of caddies, our caddy master would sometimes have to waddle down to the end of the pen and back to his stool to determine which able bodies (and idle minds) were available. As Gene did this, all the usual joking, swearing, and general fooling around in the caddy pen immediately ceased. He would eye us all sitting quietly on the single benches on either side or hanging out where the professional caddies always gathered to smoke or play cards at the back of the pen. During these inspection tours, Gene would never give anyone the nod directly. Instead, he would return to his command post, slowly ponder his next member-caddy match up, and then bark out their names as indecipherably as ever.
            </p>
            <p class="top">
                My most memorable experience with this ideal arrangement (which still required paying attention to where the ball landed) involved Tommy Armour, a Winged Foot member known as “the Silver Scot.” A professional golf champion in the 1920s and ‘30s, Armour was long retired and in his early seventies. He and Sandy, his even older Scottish immigrant brother, were still great ball strikers, but either due to advanced age or too much time at “the 19th hole” (the clubhouse bar) both suffered from the yips on the green. So, on most holes, they just hit their approach shots in that general direction and motored off to the next tee, leaving me to fetch the balls wherever they landed (usually near the pin) and catch up with them, on foot, as fast as I could.
            </p>
            <p class="top">
                Winged Foot had other celebrities, far more au courant than this pair of crusty old Scots. Where else could you rub elbows with big name sports and entertainment figures like Frank Gifford, recently retired from the Giants; Wellington Mara, co-owner of Gifford’s old football team; or Ed Sullivan, the variety show host who introduced the Beatles to primetime American audiences? The head pro at Winged Foot was the portly, always-tanned Claude Harmon, who regularly jetted off to give private lessons to the King of Morocco. Claude’s own royal family included several rather entitled sons, who had caddies their own age. One of them, Butch, later became a well-known golf teacher himself, working to improve the swing of Tiger Woods and other PGA stars. I caddied for all of these folks (except Tiger, the Beatles, and the King), yet none ever asked me to come to work for them. Ed Sullivan, at least, was a good tipper.
            </p>
            <p class="top">
                In my day, Winged Foot had no scholarship programs. Instead, the club sponsored a once-a-year golfing bacchanalia known as the “caddy feast.” As the name implies, the help was fed at this event— on the terrace behind Winged Foot’s huge stone clubhouse, never inside—and we were allowed to play all day long on both championship courses. This led to a mad dash up and down the fairways, accompanied by all kinds of wild betting, arguing, and club throwing as caddies, young and older, tried to play as many holes as they could before dark. The next day, everybody would be back in the caddy pen, under the baleful eye of Gene Hayden, allowed to eat only at the dingy little snack bar nearby.
            </p>
            <p class="top">
                The intersection of class and race at Winged Foot was pretty hard to miss. The experience of day laboring is consciousness raising in any setting, though not everyone reacts the same way I did to seeing grown men, without much money, functioning as beasts of burden for other guys, their own age or younger, with lots of dough. For me, it began to raise a few questions about how wealth and income is distributed in America and who, in the majestic equality of the law, gets to sleep under the stars and in sand traps after a bad night with the bottle.
            </p>
            <p class="top">
                Not long after my caddying career ended, I managed to exact a small measure of revenge for the injustices of the local private club system. In the summer of 1969, I organized other young people in the area to protest the blatant discrimination against non-whites reflected in both membership and guest policies. (Some Westchester clubs, in those days, were not embarrassed to inform the few members who hosted Fresh Air Program kids that their dark-skinned summer visitors from the city were not welcome at the pool.) Our picketing and leafleting at Winged Foot, the Larchmont Yacht Club, and similar targets was a faint suburban echo of real civil rights campaigning down south. But the New York Times covered our protest activity, along with several TV channels from the city. It gave me great personal pleasure to know that it ruffled a few feathers among the local nabobs — including some wealthy golfers whose bags I had carried, ball marks I had repaired, and divots I had replaced on the green fairways of Winged Foot just a few summers before.
            </p>
            <p class="top">
                A golf fanatic in his caddying days, Steve Early now believes that golf is the most boring game in the world. If you must play, he recommends using a lightweight bag, carrying it yourself, and walking rather than riding in a cart. Be your own caddy! He can be reached at <a href="">Lsupport@aol.com</a>
            </p>
        </section>
    </article>
@stop


@section('footer-scripts')

@stop
