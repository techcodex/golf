@extends('layouts.app')


@section('header-styles')
<style>
    .top{
        margin-top: 10px;
    }
</style>

@stop


@section('main-section')
    <article class="post_item post_item_single post type-post">
        <section class="post_featured">
            <div class="post_thumb" data-image="images/image-2.jpg" data-title="What’s Most Important in a Golf Course">
                <center>
                    <img  alt="What’s Most Important in a Golf Course" src="{{asset('images/Golfs-Other-Half-1024x652.png')}}">
                </center>
            </div>
        </section>
        <section class="post_content">
            <p class="top">

                How golf’s other half lives: why caddies are fighting for their rights
                Last month, amidst a severe lightning storm during the Honda Classic golf tournament in Palm Beach Gardens, Florida, a dozen caddies huddled at the back of an exposed, three-sided metal tent, trying not to get wet and hoping they hadn’t just made the stupidest decision of their lives.

                A concerned male bystander had walked by the caddies a few minutes prior, right before the storm hit, and told them they should head into the clubhouse for shelter.
            </p>
            <p class="top">
                One of the caddies, Steve Catlin, heard the advice and laughed to himself. Unless a caddie is packing a player’s bags – on “cut day” or at the conclusion of the tournament – caddies are forbidden from entering the clubhouse. There are no other exceptions. “The look on the man’s face when we told him that was priceless,” Catlin said.
            </p>
            <p class="top">
                Catlin’s video of the caddies’ predicament went viral on Twitter – in the golf world, at least. Catlin, who has been a caddie for 13 years, said he knew rain was coming but he had had no idea that the storm would feature lightning and winds strong enough to knock down the scoreboard. By the time Catlin and his fellow caddies realized their mistake, they had nowhere else to go.
            </p>
            <p class="top">
                No one was injured in the storm, but the anecdote served as just another reminder for caddies, who recently formed their own trade association, that they want better treatment.
            </p>
            <p class="top">
                Caddies have a long list of things of things they wish the PGA Tour could improve, everything from not having to park so far away from a course (that problem has been improving), to being compensated for the sponsors’ logos they wear on their bibs (that issue is currently being contested in court). Additionally, since they’re barred from the clubhouse, caddies’ restroom facilities frequently lack running water, making it impossible for them to wash their hands, and the food they are served often lacks healthy options and isn’t up to the standard of the players’ food.
            </p>
            <p class="top">
                While all of these matters would probably be addressed more expeditiously if players were to be more vocal in their support of caddies, thus far the pros have been hesitant to speak up for their employees, with PGA golfer Pat Perez and Hall of Famer Greg Norman among the few to talk about the issue. All four players contacted for this article, including Perez, either declined or did not respond to interview requests. The three caddies interviewed also did not want their players to be spoken to.
            </p>
            <section class="post_author author vcard">
                <div class="post_author_info">
                    <p class="top">“We want to fight our own battles,” caddie Kenny Harms said. “We don’t want this to have to be a distraction to our players. Our players come first, and we come second.”</p>
                </div>
            </section>
            <p class="top">Caddie Heath Holt, who has held his job for 15 years, echoed Harms’ sentiments:</p>
            <section class="post_author author vcard">
                <div class="post_author_info">
                    <p class="top">“Players have their own busy schedule and are trying to earn their own living,” Holt said. “It’s not realistic that they’re going to spend too much time working for us. That’s not a knock on them, that’s just reality. If we can’t stand up for ourselves, I don’t see how we could expect any player to take a stand either.”</p>
                </div>
            </section>
            <p class="top">
                The day after the storm, Honda Classic organizers apologized for the lack of safe options they had given caddies and said they will fix the problem in the future. According to Harms, though, what happened at the Honda was not abnormal. The only difference was that this time it was caught on tape.
            </p>
            <section class="post_author author vcard">
                <div class="post_author_info">
                    <p class="top">“I don’t want to say I was surprised [by the news], because there have probably been 20 times since I started caddying where caddies have been put in a situation that endangered their lives,” said Harms, who did not attend the Honda Classic but has been a professional caddie for 26 years and currently works for Kevin Na. Harms noted that in the weeks since, the tournaments have had better shelter options available to the caddies in the event of an emergency.</p>

                </div>
                <span class="add_block"></span>
            </section>
            <p class="top">
                Caddies have long been told that there is no room to fit them inside clubhouses, but Harms disputes this, pointing out that the LPGA Tour, which has similar 140-player fields, allows caddies inside the very same clubhouses that their PGA counterparts are barred from.
            </p>
            <p class="top">
                “If there’s a serious storm coming through where people’s lives could be in jeopardy, you fit these people in,” Harms said. “If I were the commissioner of the Tour, every single caddie that next week would have had new credentials that allowed clubhouse access. Some of us have been out there 30, 40 years. We’re part of the family, and we need to be treated like it.”
            </p>
            <p class="top">
                Caddies, by definition, are contractors. They work for a player, not the PGA Tour, surviving on a 5-10% cut of their players’ winnings to earn a living. The Tour does subsidize caddies’ healthcare by giving $2,000 to each caddie who works at least 15 tournaments a year, yet for many that stipend is not enough.
            </p>
            <p class="top">
                Caddies who work for less established players have long been known to carpool to tournaments, share low-cost hotel rooms, and eat cheaply. Even then, if their player gets cut after the second round of the tournament, the caddies make minimal money for their week of work, which extends far beyond carrying golf clubs.
            </p>
            <section class="post_author author vcard">
                <div class="post_author_info">
                    <p class="top">“70% of caddies don’t make enough money to contribute to retirement savings,” said Harms, who is a board member for the recently formed Association of Professional Tour Caddies. “I’d say 50% of the guys don’t have health insurance.”</p>

                </div>
                <span class="add_block"></span>
            </section>
            <p class="top">
                Seeking to address that very problem, Harms and his fellow caddies last year identified one so-called injustice – the sponsor-filled bibs every caddie wears, sans payment – and asked the Tour to remedy the situation.
            </p>
            <p class="top">
                As coverage of the Tour has gone up over the past decades, caddies have increasingly become part of the sport’s storylines, fodder for the analysts on the Golf Channel to scrutinize and debate.
            </p>
            <p class="top">
                “On TV, what gets shown more than anything on the caddies is our bib,” Harms said. “That bib could take care of all our healthcare and retirement. It’s on our bodies, and the PGA is making a lot of money from it.”
            </p>
            <p class="top">
                Last September, after having twice been told in meetings with Tour officials that they “had no right to the bib,” Harms did some calculations and met with Andy Pazder, PGA Tour’s Chief of Operations, with a specific proposal. If the approximately 200 caddies who work 15 tournaments were each paid $10,000 for retirement and $10,000 for healthcare – a total of about $4m – the caddies would happily continue to wear the bibs. “I know for a fact that that bib is worth a lot more money than $4m,” Harms said.
            </p>
            <p class="top">
                This time, the caddies’ argument seemed to have fallen on sympathetic ears, with Pazder saying he would bring the issue up with the board, according to Harms. “We ended up hearing that he never brought it up and all he did was talk about a small raise in healthcare,” Harms said. “At that point we knew nothing was going to change.”
            </p>
            <p class="add_block"></p>
            <p class="top">
                The caddies hired a law firm, who told them that through last year’s landmark O’Bannon v NCAA sports image rights case, legal precedent had been set, making the case “a slam dunk”. The PGA declined an interview request for this article, citing the ongoing litigation.
            </p>
            <p class="top">
                In February, with Harms as a co-complainant, 82 caddies filed a $50m class action lawsuit against the PGA Tour, claiming that they deserve to be paid a portion of the sponsorship money the Tour earns from caddies wearing bibs and that the bibs were infringing upon their right to display sponsors’ logos on their own shirts. More caddies have since joined the class, bringing the current total to 167.
            </p>
            <section class="post_author author vcard">
                <div class="post_author_info">
                    <p class="top">“I don’t think anybody involved, myself included, wanted to join the APTC so that we could file a lawsuit,” Holt said. “After trying to negotiate something and being flat turned down, it was inevitable that a lawsuit was going to have to happen.”</p>

                </div>
                <span class="add_block"></span>
            </section>
            <p class="top">
                Since the lawsuit, official relationships between caddies and the PGA have become noticeably more strained. The Tour no longer is holding meetings with the APTC board and recently cancelled an annual dinner it holds for the caddies.
            </p>
            <p class="top">
                Still, Harms insists that none of this is distracting the caddies from their primary jobs. Harms won’t talk to others about the lawsuit while working and says the caddies are going to continue to do their jobs without causing a stir – in other words, no anti-bib strikes are in the offing.
            </p>
            <p class="top">
                “I’ve been very fortunate in working with great players my whole career, and I’ve done well with my money,” Harms said. “This is about the guys that can’t afford healthcare and don’t have retirement. We just want what’s fair.”
            </p>
            <p class="top">
                At least this week caddies know they’ll be treated well at the Masters. They’ll have their own lockers, showers, convenient parking spaces and good food. And, because each caddie will be wearing the Augusta National’s famous white jumpsuits, there won’t be any bibs in sight. Not even sponsors can trump that tradition.
            </p>
        </section>
    </article>

@stop


@section('footer-scripts')
<script>
    $("document").ready(function (e) {
        $('html,body').animate({
            scrollTop:$("#affector").offset().top
        }, 2000);
    });
</script>

@stop