<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <title>{{ $page_title }} | {{ config('app.name') }} </title>
    <link rel="icon" type="image/x-icon" href="{{asset('images/favicon.png')}}"/>
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/fontello/css/fontello.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/woocommerce/assets/css/woocommerce-smallscreen.css")}}' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/woocommerce/assets/css/woocommerce-layout.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/woocommerce/assets/css/woocommerce.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/style.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/woocommerce/assets/css/plugin.woocommerce.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/core.animation.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/skin.css")}}' type='text/css' media='all'/>
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/magnific/magnific-popup.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/template.shortcodes.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/plugin.instagram-widget.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/responsive.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/skin.responsive.css")}}' type='text/css' media='all'/>
</head>

<body class="archive post-type-archive woocommerce body_filled scheme_original sidebar_show sidebar_left sidebar_outer_hide">
<div class="body_wrap">
    <div class="page_wrap">
        <div class="top_panel_fixed_wrap"></div>
        @include('common.extra_top_nav')

        <div class="page_content_wrap page_paddings_yes">
            <div class="content_wrap">
                    <div class="list_products shop_mode_thumbs">
                        <ul class="products">
                            @foreach($caddies as $caddie)
                                @if($caddie->caddie)
                                <li class="product type-product column-1_3">
                                    <div class="post_item_wrap">
                                        <div class="post_featured">
                                            <div class="post_thumb">
                                                <a href="shop-single.html">
                                                    <img src="{{asset($caddie->profile_picture)}}" class="attachment-shop_catalog size-shop_catalog" alt="product-9" title="product-9" style="height:270px !important;"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="post_content">
                                            <h3><a href="shop-single.html">{{$caddie->first_name}}</a></h3>
                                            <span class="price">
                                            <a rel="nofollow" href="{{ route('host.adopt.caddie', ['caddieID' => $caddie]) }}" class="button add_to_cart_button">Adopt a Caddie</a>
                                        </div>
                                    </div>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                        <nav id="pagination" class="pagination_wrap pagination_pages">
                            {{$caddies->links()}}
                        </nav>
                    </div>
            </div>
        </div>
        @include('common.footer')
    </div>
</div>
<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

<script type='text/javascript' src='{{asset("js/vendor/jquery-3.1.1.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/jquery-migrate.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/magnific/jcaquery.magnific-popup.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/photostack/modernizr.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/superfish.js")}}'></script>
<script type="text/javascript" src="{{asset("js/custom/_main.js")}}"></script>
<script type='text/javascript' src='{{asset("js/custom/core.utils.js")}}'></script>
<script type='text/javascript' src='{{asset("js/custom/core.init.js")}}'></script>
<script type='text/javascript' src='{{asset("js/custom/template.init.js")}}'></script>
<script type='text/javascript' src='{{asset("js/custom/template.shortcodes.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/widget.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/mouse.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/slider.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/price-slider.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/vendor/core.messages/core.messages.js")}}'></script>

</body>
</html>
