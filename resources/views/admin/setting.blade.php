@extends('layouts.app_admin')

@section('content')
<div class="row">
	<div class="col-lg-12">

		<!--begin::Portlet-->
		<div class="m-portlet">

			<!--begin::Form-->
          <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" action="{{ route('admin.update.email') }}" method="POST">
                    @csrf
                <div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<div class="col-lg-12 @if($errors->has('email')) has-danger  @endif">
							<label>Email:</label>
                            <input type="email" value="{{ auth()->user()->email }}" name="email" class="form-control m-input m-form--state" placeholder="Update Email Address">
                            @if($errors->has('email')) <span class="form-control-feedback">{{ $errors->first('email') }}</span> @endif
						</div>
					</div>
	            </div>
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" id="button" class="btn btn-primary">Update</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->

	</div>
	<div class="col-lg-12">

		<!--begin::Portlet-->
		<div class="m-portlet">

			<!--begin::Form-->
          <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('admin.update.name') }}" method="POST">
                    @csrf
                <div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<div class="col-lg-6 @if($errors->has('first_name')) has-danger  @endif">
							<label>First Name:</label>
                            <input type="text" value="{{ auth()->user()->first_name }}" name="first_name" class="form-control m-input m-form--state" placeholder="Update First Name">
                            @if($errors->has('first_name')) <span class="form-control-feedback">{{ $errors->first('first_name') }}</span> @endif
						</div>
						<div class="col-lg-6 @if($errors->has('last_name')) has-danger  @endif">
							<label>Last Name:</label>
                            <input type="text" value="{{ auth()->user()->last_name }}" name="last_name" class="form-control m-input m-form--state" placeholder="Update Last Name">
                            @if($errors->has('last_name')) <span class="form-control-feedback">{{ $errors->first('last_name') }}</span> @endif
						</div>
					</div>
	            </div>
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" id="button" class="btn btn-primary">Update</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->

	</div>
	<div class="col-lg-12">

		<!--begin::Portlet-->
		<div class="m-portlet">

			<!--begin::Form-->
          <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" action="{{ route('admin.update.password') }}" method="POST">
                    @csrf
                <div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<div class="col-lg-6 @if($errors->has('old_password')) has-danger  @endif">
							<label>Previous Password:</label>
                            <input type="text" value="{{ old('old_password') }}" name="old_password" class="form-control m-input m-form--state" placeholder="Previous Password">
                            @if($errors->has('old_password')) <span class="form-control-feedback">{{ $errors->first('first_name') }}</span> @endif
						</div>
						<div class="col-lg-6 @if($errors->has('new_password')) has-danger  @endif">
							<label>New Password:</label>
                            <input type="text" value="{{ old('new_password') }}" name="new_password" class="form-control m-input m-form--state" placeholder="New Password">
                            @if($errors->has('new_password')) <span class="form-control-feedback">{{ $errors->first('new_password') }}</span> @endif
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-6 @if($errors->has('new_password_confirmation')) has-danger  @endif">
							<label>Confirm New Password:</label>
                            <input type="text" value="{{ old('new_password_confirmation') }}" name="new_password_confirmation" class="form-control m-input m-form--state" placeholder="Confirm New Password">
                            @if($errors->has('new_password_confirmation')) <span class="form-control-feedback">{{ $errors->first('first_name') }}</span> @endif
						</div>
					</div>
	            </div>
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" id="button" class="btn btn-primary">Update</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->

	</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/summernote.js') }}" type="text/javascript"></script>
@endsection
