@extends('layouts.app_admin')

@section('content')
<div class="row">
	<div class="col-lg-12">
        @if(Session::has('error'))
            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
                <div class="m-alert__icon">
                    <i class="la la-warning"></i>
                </div>
                <div class="m-alert__text">
                        <strong>Error!</strong> {{ Session::get('error') }}
                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                </div>
            </div>
        @endif

		<!--begin::Portlet-->
		<div class="m-portlet">

			<!--begin::Form-->
          <form id="frmTarget" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" action="{{ route('plan.store') }}" method="POST">
                    @csrf
                <div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<div class="col-lg-6 @if($errors->has('title')) has-danger  @endif">
							<label>Plan Title:</label>
                            <input type="text" value="{{ old('title') }}" name="title" class="form-control m-input m-form--state" placeholder="Enter title of Plan">
                            @if($errors->has('title')) <span class="form-control-feedback">{{ $errors->first('title') }}</span> @endif
						</div>
						<div class="col-lg-6 @if($errors->has('price')) has-danger  @endif">
							<label>Amount of Plan:</label>
                            <input type="number" value="{{ old('price') }}" name="price" class="form-control m-input m-form--state" placeholder="Enter price of Plan in USD">
                            @if($errors->has('price')) <span class="form-control-feedback">{{ $errors->first('price') }}</span> @endif
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-12 @if($errors->has('description')) has-danger @endif">
                            <label>Plan Description:</label>
                            <textarea name="description" class="form-control m-input m-form--state" style="resize: none;" rows="5" placeholder="Enter Description of The Plan">{{ old('description') }}</textarea>
                            @if($errors->has('description')) <span class="form-control-feedback">{{ $errors->first('description') }}</span> @endif
						</div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 @if($errors->has('frequency')) has-danger  @endif">
                            <label for="frequency">Plan Frequency:</label>
                            <div class="dropdown bootstrap-select form-control m-bootstrap-select m_">
                                <select name="frequency" id="type" class="form-control m-bootstrap-select m_selectpicker" title="Select Plan Frequency">
                                    <option value="Month" @if(old('frequency') == "1") selected @endif>Monthly</option>
                                    <option value="Year" @if(old('frequency') == "2") selected @endif>Yearly</option>
                                </select>
                            </div>
                            @if($errors->has('frequency')) <span class="form-control-feedback"> {{$errors->first('frequency')}}</span>@endif
                        </div>
                        <div class="col-lg-6 @if($errors->has('interval')) has-danger  @endif">
							<label>Frequency Interval:</label>
                            <input type="number" value="{{ old('interval') }}" name="interval" class="form-control m-input m-form--state" placeholder="Enter Interval of Plan Frequency">
                            @if($errors->has('interval')) <span class="form-control-feedback">{{ $errors->first('interval') }}</span> @endif
						</div>
					</div>
	            </div>
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" id="button" class="btn btn-primary">Save</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->

	</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
@endsection
