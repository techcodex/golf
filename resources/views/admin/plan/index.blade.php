
@extends('layouts.app_admin')
@section('styles')

<!--begin::Page Vendors Styles -->
<link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->

<style>
    tr {
        font-weight: 300 !important;
        font-size: 14px !important;
    }
</style>
@stop

@section('content')
@if(Session::has('error'))
<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
    <div class="m-alert__icon">
        <i class="la la-check"></i>
    </div>
    <div class="m-alert__text">
            {{ Session::get('error') }}
    </div>
    <div class="m-alert__close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        </button>
    </div>
</div>
@endif

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    All Plans Data
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                <a href="{{ route('plan.create') }}" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="flaticon-open-box"></i>
							<span>New Plan</span>
						</span>
					</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="planTable">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Title</td>
                        <td>State</td>
                        <td>Created At</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($plans as $plan)
                        <tr>
                            <td>{{ $plan->id }}</td>
                            <td>{{ $plan->name }}</td>
                            <td><span class="badge badge-success">{{ $plan->state }}</span></td>
                            <td>{{ \Carbon\Carbon::createFromTimestamp(strtotime($plan->create_time))->toFormattedDateString() }}</td>
                        <td><a href="{{ route('plan.destroy', ['id' => $plan->id]) }}" class="btn btn-danger btn-sm">Delete</a> <a href="{{ route('plan.show', ['id' => $plan->id]) }}" class="btn btn-primary btn-sm">Detail</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>


@stop

@section('scripts')

<!--begin::Page Vendors Scripts -->
<script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<!--end::Page Vendors Scripts -->

<script>
    var userTable;
    var DatatablesDataSourceAjaxServer = {
    init: function () {
       userTable = $("#planTable").DataTable();
    }
};
jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init()
});
</script>

@stop
