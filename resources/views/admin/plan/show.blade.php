@extends('layouts.app_admin')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="m-portlet" style="padding: 10px;">

            <div class="row">
                <div class="col-md-6">
                    <b>Plan Name: </b> &nbsp; {{ $plan->name }}
                </div>
                <div class="col-md-6">
                    <b>Plan Desription: </b> &nbsp; {{ $plan->description }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <b>Plan Frequency: </b> &nbsp; {{ $plan->payment_definitions[0]->frequency }}
                </div>
                <div class="col-md-6">
                    <b>Plan Frequency Interval: </b> &nbsp; {{ $plan->payment_definitions[0]->frequency_interval }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <b>State: </b> &nbsp; {{ $plan->state }}
                </div>
            </div>

		</div>
		<!--end::Portlet-->

	</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
@endsection
