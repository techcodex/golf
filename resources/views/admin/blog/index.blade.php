@extends('layouts.app_admin')
@section('styles')

<!--begin::Page Vendors Styles -->
<link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->

<style>
    tr {
        font-weight: 300 !important;
        font-size: 14px !important;
    }
</style>
@stop

@section('content')
@if(Session::has('success'))
<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
    <div class="m-alert__icon">
        <i class="la la-check"></i>
    </div>
    <div class="m-alert__text">
            {{ Session::get('success') }}
    </div>
    <div class="m-alert__close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        </button>
    </div>
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    All Blogs Data
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                <a href="{{ route('blog.create') }}" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="flaticon-car"></i>
							<span>New Blog</span>
						</span>
					</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="eventsTable">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Title</td>
                                <td>Featured</td>
                                <td>Created At</td>
                                <td>Updated At</td>
                                <td>Action</td>
                            </tr>
                        </thead>

                    </table>

    </div>
</div>


@stop
@section('scripts')

<!--begin::Page Vendors Scripts -->
<script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<!--end::Page Vendors Scripts -->

<script>
    var userTable;
    var DatatablesDataSourceAjaxServer = {
    init: function () {
       userTable = $("#eventsTable").DataTable({
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: "{{ route('blogs.get_blogs') }}",
            columns:[
                { data: 'id', name: 'id' },
                { data: 'title', name: 'title' },
                { data: 'featured', name: 'featured' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' },
                { data: 'action' },

            ],
            columnDefs: [
                {
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function(a, e, t, n) {
                        var deleteUrl = route('blog.destroy', {id: t.id});
                        var editUrl = route('blog.edit', {id: t.id});
                        return '\n  <span class="dropdown">\n <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n <i class="la la-ellipsis-h"></i>\n  </a>\n                            <div class="dropdown-menu dropdown-menu-right">\n                                <a class="dropdown-item" href="'+ editUrl +'"><i class="la la-edit"></i> Edit Blog</a>\n  <a class="dropdown-item" href="'+ deleteUrl +'" id="deleteEvent"><i class="la la-trash"></i> Delete Blog</a></div>\n  </span>';
                    }
                }
            ]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init()
});
</script>


























@stop
