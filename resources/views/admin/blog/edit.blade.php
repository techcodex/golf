@extends('layouts.app_admin')

@section('content')
<div class="row">
	<div class="col-lg-12">
        @if(Session::has('success'))
            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
                <div class="m-alert__icon">
                    <i class="la la-check"></i>
                </div>
                <div class="m-alert__text">
                        <strong>Created!</strong> {{ Session::get('success') }}
                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                </div>
            </div>
        @endif
        @if(Session::has('danger'))
            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
                <div class="m-alert__icon">
                    <i class="la la-warning"></i>
                </div>
                <div class="m-alert__text">
                        <strong>Error!</strong> {{ Session::get('danger') }}
                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                </div>
            </div>
        @endif

		<!--begin::Portlet-->
		<div class="m-portlet">

			<!--begin::Form-->
          <form id="frmTarget" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" action="{{ route('blog.update', ['id' => $blog->id]) }}" method="POST">
                    @csrf
                <div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<div class="col-lg-12 @if($errors->has('title')) has-danger  @endif">
							<label>Title:</label>
                            <input type="text" value="{{ $blog->title }}" name="title" class="form-control m-input m-form--state" placeholder="Enter title of blog">
                            @if($errors->has('title')) <span class="form-control-feedback">{{ $errors->first('title') }}</span> @endif
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-12 @if($errors->has('description')) has-danger @endif">
                            <label>Description:</label>
                            <textarea name="description" class="summernote" id="m_summernote_1">{{ $blog->description }}</textarea>

                            @if($errors->has('description')) <span class="form-control-feedback">{{ $errors->first('description') }}</span> @endif
						</div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 @if($errors->has('featured')) has-danger  @endif">
                            <label>Blog Featured Image:</label>
                            <input type="file" name="featured" class="form-control m-input m-form--state" placeholder="Enter full name of place">
                            @if($errors->has('featured')) <span class="form-control-feedback">{{ $errors->first('featured') }}</span> @endif
                        </div>
					</div>
	            </div>
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" id="button" class="btn btn-info">Update</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->

	</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/summernote.js') }}" type="text/javascript"></script>
@endsection
