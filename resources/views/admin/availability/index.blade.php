@extends('layouts.app_admin')
@section('styles')

    <!--begin::Page Vendors Styles -->
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles -->

    <style>
        tr {
            font-weight: 300 !important;
            font-size: 14px !important;
        }
    </style>
@stop

@section('content')
    @if(Session::has('success'))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check"></i>
            </div>
            <div class="m-alert__text">
                <strong>Created!</strong> {{ Session::get('success') }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    @endif
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Events Data
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('event.create') }}" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-user"></i>
							<span>New Event</span>
						</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="eventsTable">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Host Name</td>
                    <td>Display Name</td>
                    <td>Host City</td>
                    <td>Street</td>
                    <td>Event Name</td>
                    <td>Start Date</td>
                    <td>End Date</td>
                    <td>Comment</td>
                    <td>Created By</td>
                    <td>Updated By</td>
                    <td>Status</td>
                    <td>Action</td>
                </tr>
                </thead>

            </table>

        </div>
    </div>


@stop
@section('scripts')

    <!--begin::Page Vendors Scripts -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <!--end::Page Vendors Scripts -->

    <script>
        var userTable;
    </script>

    <script>
        $("document").ready(function (e) {
            $(document).on('click','a.deactive',function(e) {
                e.preventDefault();
               var id = $(this).attr('data-id');
               var data = {
                   id:id,
               }
               $.ajax({
                    url:"{{route('admin.availability.deactive')}}",
                    data:data,
                    dataType:'JSON',
                    type:'POST',
                    complete:function(jqXHR,textStatus) {
                        if(jqXHR.status == 200) {
                            var result = JSON.parse(jqXHR.responseText);
                            if(result.hasOwnProperty('success')) {
                                swal({
                                    type:'success',
                                    text:result.msg,
                                    title:"Success",
                                });
                                userTable.ajax.reload(null,false)
                            } else if(result.hasOwnProperty('error')) {
                                swal({
                                    type:'error',
                                    text:result.msg,
                                    title:"Opss Somethign Went Wrong",
                                });

                            }
                        } else {
                            alert("Contact Admin"+jqXHR.status);
                        }
                    }
               });
            });
            $(document).on('click','a.active',function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                var data = {
                    id:id,
                };
                $.ajax({
                    url:"{{route('admin.availability.active')}}",
                    data:data,
                    dataType:'JSON',
                    type:'POST',
                    complete:function (jqXKR,textStatus) {
                        if(jqXKR.status == 200) {
                            var result = JSON.parse(jqXKR.responseText);
                            if(result.hasOwnProperty('success')) {
                                swal({
                                    type:'success',
                                    title:"Congranss",
                                    text:result.msg,
                                });
                            } else if(result.hasOwnProperty('error')) {
                                swal({
                                    type:'error',
                                    title:"Opss Something Went Wrong",
                                    text:result.msg,
                                });
                            }
                        } else {
                            swal({
                                type:'error',
                                title:"Opss Something went Wrong",
                                text:"Contact Admin "+jqXKR.status,
                            });
                        }
                    }
                });
            })
        });
        var DatatablesDataSourceAjaxServer = {
            init: function () {
                userTable = $("#eventsTable").DataTable({
                    responsive: !0,
                    searchDelay: 500,
                    processing: !0,
                    serverSide: !0,
                    ajax: "{{ route('admin.availability.getAvailabilities') }}",
                    columns:[
                        { data: 'id', name: 'id' },
                        {data: 'host_name', name: 'host_name'},
                        {data:'display_name',name:'display_name'},
                        {data:'host_city',name:'host_city'},
                        {data:'host_street',name:'host_street'},
                        {data:'event_name',name:'event_name'},
                        {data:'start_date',name:'start_date'},
                        {data:'end_date',name:'end_date'},
                        {data:'comment',name:'comment'},
                        {data:'created_by',name:'created_by'},
                        {data:'updated_by',name:'updated_by'},
                        {data:'status',name:'status'},
                        {data:'action',name:'action'},


                    ],
                    columnDefs: [
                        {
                            targets: -1,
                            title: "Actions",
                            orderable: !1,
                            render: function(a, e, t, n) {
                                var deleteUrl = route('event.destroy', {id: t.id});
                                return '\n  <span class="dropdown">\n <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n <i class="la la-ellipsis-h"></i></a><div class="dropdown-menu dropdown-menu-right"> <a class="dropdown-item" href="'+ deleteUrl +'" id="deleteEvent"><i class="la la-trash"></i> Delete Availability</a></div>\n  </span>';
                            }
                        }
                    ]
                })
            }
        };
        jQuery(document).ready(function () {
            DatatablesDataSourceAjaxServer.init()
        });
    </script>
@stop
