@extends('layouts.app_admin')
@section('styles')
<link href="{{asset('css/jasny-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
@stop
@section("content")
    <div class="row">
        <div class="col-lg-12">
            @if(Session::has('success'))
                <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-check"></i>
                    </div>
                    <div class="m-alert__text">
                        <strong>Created!</strong> {{ Session::get('success') }}
                    </div>
                    <div class="m-alert__close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
        @endif

        <!--begin::Portlet-->
            <div class="m-portlet">

                <!--begin::Form-->
                <form id="frmTarget" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data"
                      action="{{route('user.update',['id'=>$user->id])}}" method="POST">
                    @csrf
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('first_name')) has-danger  @endif">
                                <label>First Name:</label>
                                <input type="text" value="{{ $user->first_name }}" name="first_name" class="form-control m-input m-form--state" placeholder="Enter First Name">                            @if($errors->has('first_name')) <span class="form-control-feedback">{{ $errors->first('first_name') }}</span>                            @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('last_name')) has-danger  @endif">
                                <label for="price">Last Name:</label>
                                <input value="{{ $user->last_name }}" type="text" id="last_name" name="last_name" class="form-control m-input" placeholder="Enter Last Name">                            @if($errors->has('last_name')) <span class="form-control-feedback">{{ $errors->first('last_name') }}</span>                            @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('email')) has-danger  @endif">
                                <label>Email:</label>
                                <input type="text" value="{{ $user->email }}" name="email" class="form-control m-input m-form--state" placeholder="Enter Email">                            @if($errors->has('email')) <span class="form-control-feedback">{{ $errors->first('email') }}</span>                            @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('cell_number')) has-danger  @endif">
                                <label for="price">Cell Number:</label>
                                <input value="{{ $user->cell_number }}" type="text" id="cell_number" name="cell_number" class="form-control m-input" placeholder="Enter Cell Number">                            @if($errors->has('cell_number')) <span class="form-control-feedback">{{ $errors->first('cell_number') }}</span>                            @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('home_number')) has-danger  @endif">
                                <label for="price">Home Number:</label>
                                <input value="{{ $user->home_number }}" type="text" id="home_number" name="home_number" class="form-control m-input" placeholder="Enter Home Number">                            @if($errors->has('home_number')) <span class="form-control-feedback">{{ $errors->first('home_number') }}</span>                            @endif
                            </div>
                        </div>

                        <div class="form-group m-form__group row">

                            <div class="col-lg-6 offset-lg-4">
                                <label for="Profile Image">Profile Image:</label><br>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style=" width: 300px; height: 200px; border:1px solid grey">
                                        <img src="{{asset($user->profile_picture)}}">
                                    </div>
                                    <div class="">
                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="myImage"></span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                                <br>
                                @if($errors->has('myImage')) <span class="form-control-feedback has-danger" style="color:red;">{{ $errors->first('myImage') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-6 offset-md-5">
                                    <button type="submit" id="button" class="btn btn-primary">Edit User</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->

        </div>
    </div>


@stop
@section('scripts')
<script src="{{asset('js/jasny-bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>







@stop
