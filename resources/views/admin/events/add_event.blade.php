@extends('layouts.app_admin')
@section('styles')
    <link href="{{asset('jasny/jasny-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <style>
        .fileinput-new,.fileinput-exists {
            color:blue;
        }
    </style>
@stop
@section("content")
    <div class="row">
        <div class="col-lg-12">
            @if(Session::has('success'))
                <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-check"></i>
                    </div>
                    <div class="m-alert__text">
                        <strong>Created!</strong> {{ Session::get('success') }}
                    </div>
                    <div class="m-alert__close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
        @endif

        <!--begin::Portlet-->
            <div class="m-portlet">

                <!--begin::Form-->
                <form id="frmTarget" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data"
                      action="{{route('event.store')}}" method="POST">
                    @csrf
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('name')) has-danger  @endif">
                                <label>Event Name:</label>
                                <input type="text" value="{{ old('name') }}" name="name" class="form-control m-input m-form--state" placeholder="Enter Event Name">
                                @if($errors->has('name')) <span class="form-control-feedback">{{ $errors->first('name') }}</span>                            @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('type')) has-danger  @endif">
                                <label for="price">Event Type:</label>
                                <div class="dropdown bootstrap-select form-control m-bootstrap-select m_">
                                    <select class="form-control m-bootstrap-select m_selectpicker" name="type">
                                        <option value="">--Select Event Type--</option>
                                        <option value="1" @if(old('type') == "1") selected @endif>Pga</option>
                                        <option value="2" @if(old('type') == "2") selected @endif>Lpga</option>
                                    </select>
                                </div>
                                @if($errors->has('type')) <span class="form-control-feedback"> {{$errors->first('type')}}</span>@endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('source')) has-danger  @endif">
                                <label for="price">Event Source</label>
                                <div class="dropdown bootstrap-select form-control m-bootstrap-select m_">
                                    <select name="source" id="type" class="form-control m-bootstrap-select m_selectpicker">
                                        <option value="">--Select Event Source--</option>
                                        <option value="1" @if(old('source') == "1") selected @endif>PGA.com</option>
                                        <option value="2" @if(old('source') == "2") selected @endif>LPGA.com</option>
                                    </select>
                                </div>
                                @if($errors->has('source')) <span class="form-control-feedback"> {{$errors->first('source')}}</span>@endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('start_date')) has-danger  @endif">
                                <label for="price">Event Start Date:</label>
                                    <div class="input-group date" >
                                        <input type="text" class="form-control m-input datepicker" readonly  name="start_date" value="mm-dd-yyyy" id=""/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>

                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('end_date')) has-danger  @endif">
                                <label for="price">Event End Date:</label>
                                <div class="input-group date" >
                                    <input type="text" class="form-control m-input datepicker" readonly name="end_date"  value="mm-dd-yyyy" id=""/>
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-6 @if($errors->has('country_id')) has-danger  @endif">
                                <label for="price">Country:</label><span class="help-block ajaxloader"></span>
                                <div class="dropdown bootstrap-select form-control m-bootstrap-select m_">
                                    <select name="country_id" id="country_id" class="form-control m-bootstrap-select m_selectpicker">
                                        <option>--Select Country --</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('country_id')) <span class="form-control-feedback">{{ $errors->first('country_id') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('state_id')) has-danger  @endif">
                                <label for="price">State:</label>
                                <div class="dropdown bootstrap-select form-control m-bootstrap-select m_">
                                    <select name="state_id" id="state_id" class="form-control m-bootstrap-select m_selectpicker">
                                        <option value="">--Select State --</option>
                                    </select>
                                </div>
                                @if($errors->has('state_id')) <span class="form-control-feedback">{{ $errors->first('state_id') }}</span>
                                @endif
                            </div>

                            <div class="col-lg-6 @if($errors->has('country_club_id')) has-danger  @endif">
                                <label for="price">Country Club::</label>
                                <div class="input-group" >
                                    <select class="form-control m-select2" id="clubs" name="country_club_id">
                                        <option></option>
                                    </select>
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <a href="#" id="country_club"><i class="la la-plus"></i></a>
                                            </span>
                                    </div>
                                    @if($errors->has('country_club_id')) <span class="form-control-feedback">{{ $errors->first('country_club_id') }}</span>
                                    @endif
                                </div>

                            </div>

                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('city')) has-danger  @endif">
                                <label>City Name:</label>
                                <input type="text" value="{{ old('city') }}" name="city" class="form-control m-input m-form--state" placeholder="Enter City Name">
                                @if($errors->has('city')) <span class="form-control-feedback">{{ $errors->first('city') }}</span>                            @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('event_image')) has-danger  @endif">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview img-thumbnail" data-trigger="fileinput" style="width: 300px; height: 150px;"></div>
                                    <div>
                                    <span class="btn btn-outline-secondary btn-file">
                                      <span class="fileinput-new">Select Event image</span>
                                      <span class="fileinput-exists">Change</span>
                                      <input type="file" name="event_image">
                                    </span>
                                        <a href="#" class="btn btn-outline-secondary fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                                @if($errors->has('event_image')) <div class="form-control-feedback">{{ $errors->first('event_image') }}</div>@endif
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-6 offset-md-5">
                                    <button type="submit" id="button" class="btn btn-primary">Add New Event</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->

        </div>
    </div>
    <!-- Modal-->
    <!-- Modal to add new country club -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New Country Club Name</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="country_club_name">Country Club Name:</label>
                        <input type="text" name="country_club_name" placeholder="Country Club Name" class="form-control" id="countryClubName">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@stop
@section('scripts')

    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/demo/default/custom/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
    <script src="{{asset('jasny/jasny-bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function (e) {
            $("#country_club").click(function (e) {
                $("#modal").modal();
                $(".btn_close").click(function (e) {
                    var name = $("#countryClubName").val();
                    var output = "<option value='"+name+"'>"+name+"</option>";
                    $("#clubs > option").remove();
                    $("#clubs").append(output);
//                    $("#countryClubName").val("");
                })
            });
            $(".datepicker").datepicker({
                format:"yyyy-mm-dd",
            });
            //seting up states
            var date = new Date();
            var month = date.getMonth();
            var fullDate = date.getDate();
            if(month < 13)
                month = month+1;
            if(month <10)
                month = '0'+month;
            if(fullDate < 10)
                fullDate = '0' + fullDate;
            $(".datepicker").val(date.getFullYear() + "-" + month + "-" + fullDate);
            //for getting states
            $("#country_id").change(function(e) {
                var id = document.getElementById('country_id').value;
                var data = {
                    id:id,
                };
                $.ajax({
                    url:"{{route('get_states')}}",
                    data:data,
                    dataType:'JSON',
                    type:'POST',
                    complete:function (jqXHR,textStatus) {
                        if(jqXHR.status == 200) {
                            var result = JSON.parse(jqXHR.responseText);
                            if(result.hasOwnProperty('success')) {
                                if(result.hasOwnProperty('states')) {
                                    var output = "";
//                                    console.log(result.states);
                                    result.states.forEach(function(state) {
                                        output += "<option value='"+state.id+"'>"+state.name+"</option>";
                                    });
                                    console.log(output);
                                    $("#state_id > option ~ option").remove();
                                    $("#state_id").append(output);
                                    $('#state_id').selectpicker('refresh');

                                } else{
                                    swal({
                                        type:'error',
                                        text:'Contact Admin',
                                        title:'Opss Something went wrong',
                                    });
                                }
                            } else {
                                swal({
                                    type:'error',
                                    text:'Contact Admin',
                                    title:'Opss Something went worng',
                                });
                            }
                        }
                    }
                });
            });
        });
    </script>
    <script>
        function formatFlag(value) {
//            console.log("template",value);
            if(!value.id) {
                return value.text;
            }
            var baseUrl = "http://www.golfworldapi.com"+value.flag;
            var imageUrl = "http://www.golfworldapi.com"+value.countryFlag;
            var country = $("<span><img height='30px' width='30px' src='"+baseUrl+"' class='img-flag'>   &nbsp;&nbsp;"+value.text +" ( "+value.provinceName+")  <img src='"+imageUrl+"' class='img-flag pull-right'> </span>");
            return country;
        }
        $(document).ready(function(e) {
            $("#clubs").select2({
                minimumInputLength:3,
                templateResult:formatFlag,
                placeholder:'Select Country Club Name',
                ajax:{
                    delay:250,
                    url:route('club.get_clubs'),
                    dataType:'JSON',
                    type:'GET',
                    data:function (params) {
                        var query = {
                            search:params.term,
                        };
                        return query;
                        console.log(query);
                    },
                    processResults:function (data) {
                        return {
                            results:$.map(data.items,function (val,i) {
                                console.log(val);
                                return {id:val.ClubName,text:val.ClubName,flag:val.ClubImageURL,countryFlag:val.Country.CountryFlag,provinceName:val.Province.StateProvFull}
                            })
                        }
                    },
                },

            });
        });
    </script>
@stop
