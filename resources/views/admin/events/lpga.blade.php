@extends('layouts.app_admin')
@section('styles')

<!--begin::Page Vendors Styles -->
<link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->

<style>
    tr {
        font-weight: 300 !important;
        font-size: 14px !important;
    }
</style>
@stop

@section('content')
@if(Session::has('success'))
<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
    <div class="m-alert__icon">
        <i class="la la-check"></i>
    </div>
    <div class="m-alert__text">
            <strong>Created!</strong> {{ Session::get('success') }}
    </div>
    <div class="m-alert__close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        </button>
    </div>
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    All Pga Events Data
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                <a href="{{ route('event.create') }}" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-user"></i>
							<span>Add New Event</span>
						</span>
					</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="eventsTable">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Name</td>
                                <td>Type</td>
                                <td>Source</td>
                                <td>Start Date</td>
                                <td>End Date</td>
                                <td>Country ID</td>
                                <td>City</td>
                                <td>State ID</td>
                                <td>Country Club ID</td>
                                <td>Created</td>
                                <td>Updated</td>
                                <td>Created At</td>
                                <td>Updated At</td>
                                <td>Action</td>
                            </tr>
                        </thead>

                    </table>

    </div>
</div>


@stop
@section('scripts')

<!--begin::Page Vendors Scripts -->
<script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<!--end::Page Vendors Scripts -->

<script>
    var userTable;
</script>

<script>
    var DatatablesDataSourceAjaxServer = {
    init: function () {
       userTable = $("#eventsTable").DataTable({
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: "{{ route('event.get_lpga_events') }}",
            columns:[
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'type', name: 'type' },
                { data: 'source', name: 'source' },
                { data: 'start_date', name: 'start_date' },
                { data: 'end_date', name: 'end_date' },
                { data: 'country_id', name:'country_id' },
                { data: 'city', name:'city' },
                { data: 'state_id', name:'state_id' },
                { data: 'country_club_name', name:'country_club_name' },
                { data: 'created_by', name: 'created_by' },
                { data: 'updated_by', name: 'updated_by' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' },
                { data: 'action' },

            ],
            columnDefs: [
                {
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function(a, e, t, n) {
                        var deleteUrl = route('event.destroy', {id: t.id});
                        return '\n  <span class="dropdown">\n <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n <i class="la la-ellipsis-h"></i>\n  </a>\n                            <div class="dropdown-menu dropdown-menu-right">\n                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Event</a>\n  <a class="dropdown-item" href="'+ deleteUrl +'" id="deleteEvent"><i class="la la-trash"></i> Delete Event</a></div>\n  </span>';
                    }
                }
            ]
        })
    }
};
jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init()
});
</script>


























@stop
