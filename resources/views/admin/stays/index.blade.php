@extends('layouts.app_admin')
@section('styles')

    <!--begin::Page Vendors Styles -->
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles -->

    <style>
        tr {
            font-weight: 300 !important;
            font-size: 14px !important;
        }
    </style>
@stop
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Stays Data
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="usersTable">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Caddie Name</td>
                    <td>Host Name</td>
                    <td>Host Cell No</td>
                    <td>Caddie Cell No</td>
                    <td>Event Name</td>
                    <td>Created by</td>
                    <td>Updated by</td>
                </tr>
                </thead>

            </table>

        </div>
    </div>


@stop
@section('scripts')

    <!--begin::Page Vendors Scripts -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <!--end::Page Vendors Scripts -->

    <script>
        var userTable;
    </script>

    <script>

        var DatatablesDataSourceAjaxServer = {
            init: function () {
                userTable = $("#usersTable").DataTable({
                    responsive: !0,
                    searchDelay: 500,
                    processing: !0,
                    serverSide: !0,
                    ajax: "{{ route('admin.stay.getStays') }}",
                    columns:[
                        { data: 'id', name: 'id' },
                        { data: 'caddie_name', name: 'caddie_name' },
                        { data: 'host_name', name: 'host_name' },
                        { data: 'host_cell_no', name: 'host_cell_no' },
                        { data: 'caddie_cell_no', name: 'caddie_cell_no' },
                        { data: 'event_name',name:'event_name'},
                        { data: 'created_by', name: 'created_by' },
                        { data: 'updated_by', name: 'updated_by' },


                    ],
                })
            }
        };
        jQuery(document).ready(function () {
            DatatablesDataSourceAjaxServer.init()
        });
    </script>



@stop
