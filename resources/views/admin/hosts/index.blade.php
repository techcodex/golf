@extends('layouts.app_admin')
@section('styles')

    <!--begin::Page Vendors Styles -->
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles -->

    <style>
        tr {
            font-weight: 300 !important;
            font-size: 14px !important;
        }
    </style>
@stop
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Hosts Data
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('user.create') }}" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-user"></i>
							<span>Add New Host</span>
						</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="usersTable">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>First</td>
                    <td>Last</td>
                    <td>Email</td>
                    <td>House</td>
                    <td>Cell</td>
                    <td>Status</td>
                    <td>Created</td>
                    <td>Updated</td>
                    <td>Created At</td>
                    <td>Updated At</td>
                    <td>Action</td>
                </tr>
                </thead>

            </table>

        </div>
    </div>


@stop
@section('scripts')

    <!--begin::Page Vendors Scripts -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <!--end::Page Vendors Scripts -->

    <script>
        var userTable;
        $("#usersTable").click(function (e) {
            if(!e.target.classList.contains('deactive')) {
                return ;
            }
            var id = e.target.getAttribute('data-id');
            var data = {
                id:id,
            };
            var url = "{{route('users.deactive',':id')}}";
            url = url.replace(":id",id);
            $.ajax({
                url:url,
                data:data,
                dataType:'JSON',
                type:'POST',
                complete:function (jqXHR,textStatus) {
                    if(jqXHR.status == 200) {
                        var result = JSON.parse(jqXHR.responseText);
                        if(result.hasOwnProperty('success')) {
                            swal({
                                type:'success',
                                text:'User Deactive Successfully',
                                title:'Congrasss',
                            });
                            userTable.ajax.reload(null,false);
                        } else if(result.hasOwnProperty('error')) {
                            swal({
                                type:'error',
                                text:result.msg,
                                title:'Opss Something Went Wrong',
                            });
                        }
                    } else {
                        swal({
                            type:'error',
                            text:'Contact Admin',
                            title:'Opss Something Went Wrong',
                        });
                    }
                }
            });
        });

        $("#usersTable").click(function (e) {
            if(!e.target.classList.contains('active')) {
                return ;
            }
            var id = e.target.getAttribute('data-id');
            var data = {
                id:id,
            };
            var url = "{{route('users.active',':id')}}";
            url = url.replace(":id",id);
//            console.log(url);
            $.ajax({
                url:url,
                data:data,
                dataType:'JSON',
                type:'POST',
                complete:function (jqXHR,textStatus) {
                    if(jqXHR.status == 200) {
                        var result = JSON.parse(jqXHR.responseText);
                        if(result.hasOwnProperty('success')) {
                            userTable.ajax.reload(null,false);
                            swal({
                                type:'success',
                                text:'User Active Successfully',
                                title:'Congrasss',
                            });
                        } else if(result.hasOwnProperty('error')) {
                            swal({
                                type:'error',
                                text:result.msg,
                                title:'Opsss Something Went Wrong',
                            });
                        }
                    } else {
                        swal({
                            type:'error',
                            text:'Contact Admin for futher support',
                            title:'Opss Something Went Wrong',
                        });
                    }
                }
            });
        });

    </script>

    <script>
        var DatatablesDataSourceAjaxServer = {
            init: function () {
                userTable = $("#usersTable").DataTable({
                    responsive: !0,
                    searchDelay: 500,
                    processing: !0,
                    serverSide: !0,
                    ajax: "{{ route('host.host_list') }}",
                    columns:[
                        { data: 'id', name: 'id' },
                        { data: 'first_name', name: 'first_name' },
                        { data: 'last_name', name: 'last_name' },
                        { data: 'email', name: 'email' },
                        { data: 'home_number', name: 'home_number' },
                        { data: 'cell_number', name: 'cell_number' },
                        { data:'status', name:'status' },
                        { data: 'created_by', name: 'created_by' },
                        { data: 'updated_by', name: 'updated_by' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'updated_at', name: 'updated_at' },
                        { data: 'action' },

                    ],
                    columnDefs: [{
                        targets: -1,
                        title: "Actions",
                        orderable: !1,
                        render: function (a, e, t, n) {
                            var editUser = route('user.edit',{id:t.id});
                            return '<a href="'+editUser+'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btn-edit" title="Edit"><i class="la la-edit"></i></a>';

                        }
                    }]
                })
            }
        };
        jQuery(document).ready(function () {
            DatatablesDataSourceAjaxServer.init()
        });
    </script>
@stop
