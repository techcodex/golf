<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="utf-8" />

    <title>{{ config('app.name') }} | Admin Login Page</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="{{ asset('font/ajax/libs/webfont/1.6.16/webfont.js') }}"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
    </script>
    <!--end::Web font -->


    <!--begin::Base Styles -->
    <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->

    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
</head>
<!-- end::Head -->


<!-- begin::Body -->

<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">



    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">


        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--desktop m-grid--ver-desktop m-grid--hor-tablet-and-mobile m-login m-login--6"
            id="m_login">
            <div class="m-grid__item   m-grid__item--order-tablet-and-mobile-2  m-grid m-grid--hor m-login__aside " style="background-image: url({{ asset('assets/app/media/img/bg/bg-4.jpg') }});">
                <div class="m-grid__item">
                    <div class="m-login__logo">
                        <a href="#">
                    <center><img src="{{ asset('images/logo.png') }}" style="width:40%; height: 35%; margin-top:30%;"></center>
				</a>
                    </div>
                </div>

                <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver">
                    <div class="m-grid__item m-grid__item--middle">
                    </div>
                </div>

                <div class="m-grid__item offset-md-3">
                    <div class="m-login__info">
                        <div class="m-login__section ">
                            <a href="#" class="m-link">&copy 2019 {{ config('app.name') }}</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-grid__item m-grid__item--fluid  m-grid__item--order-tablet-and-mobile-1  m-login__wrapper">
                <!--begin::Head-->
                <div class="m-login__head">
                    <span>Don't have an account?</span>
                    <a href="{{ route('register') }}" class="m-link m--font-danger">Sign Up</a>
                </div>
                <!--end::Head-->

                <!--begin::Body-->
                <div class="m-login__body">

                    <!--begin::Signin-->
                    <div class="m-login__signin">
                        <div class="m-login__title">
                            <h3>Create Account</h3>
                        </div>
                        @if(Session::has('error'))
                            <h3 class="text-danger">*{{Session::get('error')}}</h3>
                        @endif
                        <!--begin::Form-->
                        <form class="m-login__form m-form" id="loginForm" action="{{ route('login') }}" method="POST">
                            @if(count($errors) > 0)
                            <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                <span>Incorrect Email or Password. Please try again.</span>
                            </div>
                            @endif @csrf
                            <div class="form-group m-form__group">
                                <input class="form-control m-input" autofocus type="email" placeholder="Email" name="email" autocomplete="off">
                            </div>
                            <div class="form-group m-form__group">
                                <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                            </div>
                        </form>
                        <!--end::Form-->

                        <!--begin::Action-->
                        <div class="m-login__action">
                            <a href="#">
						        <button id="m_login_signin_submit" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">Sign In</button>
					        </a>
                        </div>
                        <!--end::Action-->

                    </div>
                    <!--end::Signin-->
                </div>
                <!--end::Body-->
            </div>
        </div>




    </div>
    <!-- end:: Page -->


    <!--begin::Base Scripts -->
    <script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
    <!--end::Base Scripts -->

    <script>
        $('#m_login_signin_submit').click(function(){
            $("#loginForm").submit();
        });
    </script>


</body>
<!-- end::Body -->

</html>
