@extends('layouts.app_admin')
@section('styles')
    <link href="{{asset('css/jasny-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
@stop
@section("content")
    <div class="row">
        <div class="col-lg-12">
            @if(Session::has('success'))
                <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-check"></i>
                    </div>
                    <div class="m-alert__text">
                        <strong>Created!</strong> {{ Session::get('success') }}
                    </div>
                    <div class="m-alert__close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
        @endif

        <!--begin::Portlet-->
            <div class="m-portlet">

                <!--begin::Form-->
                <form id="frmTarget" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data"
                      action="{{route('caddie.update',['id'=>$caddie->id])}}" method="POST">
                    @csrf
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('caddie_name')) has-danger  @endif">
                                <label>Caddie Name:</label>
                                <input type="text" value="{{ $caddie->caddie_name }}" name="caddie_name" class="form-control m-input m-form--state" placeholder="Enter Caddie Name">
                                @if($errors->has('caddie_name')) <span class="form-control-feedback">{{ $errors->first('caddie_name') }}</span>                            @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('phone')) has-danger  @endif">
                                <label for="price">Phone:</label>
                                <input value="{{ $caddie->phone }}" type="text" id="phone" name="phone" class="form-control m-input" placeholder="Enter Phone No">
                                @if($errors->has('phone')) <span class="form-control-feedback">{{ $errors->first('phone') }}</span>                            @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('email')) has-danger  @endif">
                                <label>Email:</label>
                                <input type="text" value="{{ $caddie->email }}" name="email" class="form-control m-input m-form--state" placeholder="Enter Email">
                                @if($errors->has('email')) <span class="form-control-feedback">{{ $errors->first('email') }}</span>                            @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('pga_lpga_id')) has-danger  @endif">
                                <label for="price">Caddie ID:</label>
                                <input value="{{ $caddie->pga_lpga_id }}" type="text" id="pga_lpga_id" name="pga_lpga_id" class="form-control m-input" placeholder="Enter Pga Lpga ID">
                                @if($errors->has('pga_lpga')) <span class="form-control-feedback">{{ $errors->first('pga_lpga') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('pet')) has-danger  @endif">
                                <label for="price">Allgries To Pet ?:</label>
                                <div class="dropdown bootstrap-select form-control m-bootstrap-select m_">
                                    <select name="pet" class="form-control m-bootstrap-select m_selectpicker">
                                        <option>--Do you Have Allrgies to pet ? --</option>
                                        <option value="1" @if($caddie->pet == '1') selected @endif>YES</option>
                                        <option value="2" @if($caddie->pet == '2') selected @endif>NO</option>
                                    </select>
                                </div>
                                @if($errors->has('pet')) <span class="form-control-feedback">{{ $errors->first('pet') }}</span>
                                @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('smoke')) has-danger  @endif">
                                <label for="smoke">Allgries To Smoke ?:</label>
                                <div class="dropdown bootstrap-select form-control m-bootstrap-select m_">
                                    <select name="smoke" class="form-control m-bootstrap-select m_selectpicker">
                                        <option>--Do you Have Allrgies to Smoke ? --</option>
                                        <option value="1" @if($caddie->smoke == '1') selected @endif>YES</option>
                                        <option value="2" @if($caddie->smoke == '2') selected @endif>NO</option>
                                    </select>
                                </div>
                                @if($errors->has('smoke')) <span class="form-control-feedback">{{ $errors->first('smoke') }}</span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-6 offset-md-5">
                                    <button type="submit" id="button" class="btn btn-primary">Edit Caddie Profile</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->

        </div>
    </div>


@stop
@section('scripts')
    <script src="{{asset('js/jasny-bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
@stop
