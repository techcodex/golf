@extends('layouts.app_admin')
@section('styles')

    <!--begin::Page Vendors Styles -->
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles -->

    <style>
        tr {
            font-weight: 300 !important;
            font-size: 14px !important;
        }
    </style>
@stop
@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Caddies Data
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('user.create') }}" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-user"></i>
							<span>Add New Caddie</span>
						</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="usersTable">
                <thead>
                <tr class="bg-info text-center" style="color:white">
                    <td>ID</td>
                    <td>First</td>
                    <td>Caddie Name</td>
                    <td>Pga Lpga ID</td>
                    <td>Type</td>
                    <td>Email</td>
                    <td>Cell</td>
                    <td>Featured</td>
                    <td>Status</td>
                    <td>Action</td>
                </tr>
                </thead>

            </table>

        </div>
    </div>


@stop
@section('scripts')

    <!--begin::Page Vendors Scripts -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <!--end::Page Vendors Scripts -->

    <script>
        var userTable;
        $("#usersTable").click(function (e) {
            if(!e.target.classList.contains('deactive')) {
                return ;
            }
            var id = e.target.getAttribute('data-id');
            var data = {
                id:id,
            };
            var url = "{{route('caddie.deactive',':id')}}";
            url = url.replace(":id",id);
            $.ajax({
                url:url,
                data:data,
                dataType:'JSON',
                type:'POST',
                complete:function (jqXHR,textStatus) {
                    if(jqXHR.status == 200) {
                        var result = JSON.parse(jqXHR.responseText);
                        if(result.hasOwnProperty('success')) {
                            swal({
                                type:'success',
                                text:'User Deactive Successfully',
                                title:'Congrasss',
                            });
                            userTable.ajax.reload(null,false);
                        } else if(result.hasOwnProperty('error')) {
                            swal({
                                type:'error',
                                text:result.msg,
                                title:'Opss Something Went Wrong',
                            });
                        }
                    } else {
                        swal({
                            type:'error',
                            text:'Contact Admin',
                            title:'Opss Something Went Wrong',
                        });
                    }
                }
            });
        });

        $("#usersTable").click(function (e) {
            if(e.target.classList.contains('featured')) {
                var id = e.target.getAttribute('data-id');
                var data = {
                    id:id,
                };
                $.ajax({
                    url:route('admin.caddie.featured',{id:id}),
                    type:'POST',
                    dataType:'JSON',
                    data:data,
                    complete:function(jqXHR,textStatus) {
                        if(jqXHR.status == 200) {
                            var result = JSON.parse(jqXHR.responseText);
                            if(result.hasOwnProperty('success')) {
                                swal({
                                    type:'success',
                                    text:'Caddie Featured Successfully',
                                    title:'Congrasss',
                                });
                                userTable.ajax.reload(null,false);
                            } else if(result.hasOwnProperty('error')) {
                                swal({
                                    type:'error',
                                    text:result.msg,
                                    title:'Opss Something Went Wrong',
                                });
                            }
                        } else {
                            swal({
                                type:'error',
                                text:'Contact Admin',
                                title:'Opss Something Went Wrong',
                            });
                        }
                    },
                });
            }
        });

        $("#usersTable").click(function (e) {

            if(e.target.classList.contains('disable')) {
                var id = e.target.getAttribute('data-id');
                var data = {
                    id:id,
                };
                $.ajax({
                    url:route('admin.caddie.disable',{id:id}),
                    type:'POST',
                    dataType:'JSON',
                    data:data,
                    complete:function(jqXHR,textStatus) {
                        if(jqXHR.status == 200) {
                            var result = JSON.parse(jqXHR.responseText);
                            if(result.hasOwnProperty('success')) {
                                swal({
                                    type:'success',
                                    text:'Caddie Disbale Successfully',
                                    title:'Congrasss',
                                });
                                userTable.ajax.reload(null,false);
                            }
                        } else {
                            swal({
                                type:'error',
                                text:'Contact Admin',
                                title:'Opss Something Went Wrong',
                            });
                        }
                    },
                });
            }
        });

        $("#usersTable").click(function (e) {
            if(!e.target.classList.contains('active')) {
                return ;
            }
            var id = e.target.getAttribute('data-id');
            var data = {
                id:id,
            };
            $.ajax({
                url:route('caddie.active',{id:id}),
                data:data,
                dataType:'JSON',
                type:'POST',
                complete:function (jqXHR,textStatus) {
                    if(jqXHR.status == 200) {
                        var result = JSON.parse(jqXHR.responseText);
                        if(result.hasOwnProperty('success')) {
                            userTable.ajax.reload(null,false);
                            swal({
                                type:'success',
                                text:'User Active Successfully',
                                title:'Congrasss',
                            });
                        } else if(result.hasOwnProperty('error')) {
                            swal({
                                type:'error',
                                text:result.msg,
                                title:'Opsss Something Went Wrong',
                            });
                        }
                    } else {
                        swal({
                            type:'error',
                            text:'Contact Admin for futher support',
                            title:'Opss Something Went Wrong',
                        });
                    }
                }
            });
        });

    </script>

    <script>

        var DatatablesDataSourceAjaxServer = {
            init: function () {
                userTable = $("#usersTable").DataTable({
                    responsive: !0,
                    searchDelay: 500,
                    processing: !0,
                    serverSide: !0,
                    ajax: "{{ route('caddie.list') }}",
                    columns:[
                        { data: 'id', name: 'id' },
                        { data: 'first_name', name: 'first_name' },
                        { data:'caddie_name',name:'caddie_name'},
                        { data:'pga_lpga_id',name:'pga_lpga_id'},
                        { data:'pga_lpga',name:'pga_lpga'},
                        { data: 'email', name: 'email' },
                        { data: 'cell_number', name: 'cell_number' },
                        { data: 'featured',name:'featured' },
                        { data:'status',name:'status'},
                        { data: 'action' },



                    ],
                    columnDefs: [{
                        targets: -1,
                        title: "Actions",
                        orderable: !1,
                        render: function (a, e, t, n) {
                            var editUser = route('caddie.edit',{id:t.id});
                            return '<a href="'+editUser+'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill btn-edit" title="Edit Caddie Profile"><i class="la la-edit"></i></a>';

                        }
                    }]
                })
            }
        };
        jQuery(document).ready(function () {
            DatatablesDataSourceAjaxServer.init()
        });

    </script>
@stop
