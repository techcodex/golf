@extends('layouts.app_admin')


@section('styles')


@stop


@section('content')
<!--Begin::Section-->
<div class="m-portlet">
	<div class="m-portlet__body  m-portlet__body--no-padding">
		<div class="row m-row--no-padding m-row--col-separator-xl">
			<div class="col-xl-6">
				<!--begin:: Widgets/Stats2-1 -->
<div class="m-widget1">
	<div class="m-widget1__item">
		<div class="row m-row--no-padding align-items-center">
			<div class="col">
				<h3 class="m-widget1__title">Caddies</h3>
				<span class="m-widget1__desc">Total Caddies</span>
			</div>
			<div class="col m--align-right">
				<span class="m-widget1__number m--font-brand">{{$caddie_count}}</span>
			</div>
		</div>
	</div>
	<div class="m-widget1__item">
		<div class="row m-row--no-padding align-items-center">
			<div class="col">
				<h3 class="m-widget1__title">Hots</h3>
				<span class="m-widget1__desc">Total Hosts</span>
			</div>
			<div class="col m--align-right">
				<span class="m-widget1__number m--font-danger">{{$host_count}}</span>
			</div>
		</div>
	</div>
	<div class="m-widget1__item">
		<div class="row m-row--no-padding align-items-center">
			<div class="col">
				<h3 class="m-widget1__title">Admins</h3>
				<span class="m-widget1__desc">Total Admins</span>
			</div>
			<div class="col m--align-right">
				<span class="m-widget1__number m--font-success">{{$admin_count}}</span>
			</div>
		</div>
	</div>
</div>
<!--end:: Widgets/Stats2-1 -->			</div>
			<div class="col-xl-6">
				<!--begin:: Widgets/Daily Sales-->
<div class="m-widget1">
	<div class="m-widget1__item">
		<div class="row m-row--no-padding align-items-center">
			<div class="col">
				<h3 class="m-widget1__title">Events</h3>
				<span class="m-widget1__desc">Total Events</span>
			</div>
			<div class="col m--align-right">
				<span class="m-widget1__number m--font-brand">{{$events}}</span>
			</div>
		</div>
	</div>
	<div class="m-widget1__item">
		<div class="row m-row--no-padding align-items-center">
			<div class="col">
				<h3 class="m-widget1__title">Stays</h3>
				<span class="m-widget1__desc">Total Stays</span>
			</div>
			<div class="col m--align-right">
				<span class="m-widget1__number m--font-danger">{{$stays}}</span>
			</div>
		</div>
	</div>

</div>
<!--end:: Widgets/Daily Sales-->			</div>
		</div>
	</div>
</div>
<!--End::Section-->
@stop


@section('scripts')


@stop
