@extends('layouts.app_admin')

@section('content')
<div class="row">
	<div class="col-lg-12">
        @if(Session::has('success'))
            <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
                <div class="m-alert__icon">
                    <i class="la la-check"></i>
                </div>
                <div class="m-alert__text">
                        <strong>Created!</strong> {{ Session::get('success') }}
                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                </div>
            </div>
        @endif

		<!--begin::Portlet-->
		<div class="m-portlet">

			<!--begin::Form-->
          <form id="frmTarget" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('admin.message.send') }}" method="POST">
                    @csrf
                <div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<div class="col-lg-12" id="messageDiv">
							<label>Broadcast Message:</label>
                            <textarea id="message" autofocus name="message" rows="10" class="form-control m-input m-form--state" placeholder="Enter Message"> </textarea>
                            <span id="errorSpan" class="form-control-feedback"></span>
						</div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-md-12">
                            <label>Select Users</label>
                            <select class="form-control m-select2" id="m_select2_3" name="users[]" multiple="multiple">
                                 @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->first_name }} {{ $user->last_name }} ({{ $user->email }})</option>
                                 @endforeach
                            </select>
                            <span class="form-control-feedback text-primary">Leave Empty if You want to Send to All Users</span>
                        </div>
                    </div>
	            </div>
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" id="button" class="btn btn-primary">Send</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->

	</div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}" />
@endsection

@section('scripts')
<script src="{{ asset('js/toastr.min.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/demo/default/custom/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>

<script>
    toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
</script>

<script>
    var errorSpanNode = $("#errorSpan");
    var messageDivNode = $("#messageDiv");
    $("#button").click(function(e){
        e.preventDefault();
        var message = $("#message").val().trim();
        var users = $("#m_select2_3").val();
        errorSpanNode.text("");
        messageDivNode.removeClass('has-danger');
        $.ajax({
            url: route('admin.message.send'),
            type: 'POST',
            data: {bulkmessage: message, users: users},
            dataType: 'JSON',
            success: function(e){
                if(e.hasOwnProperty('message')){
                    $("#message").val("");
                    $("#m_select2_3").val("");
                    toastr.success("Message Successfully Sent!");
                }
            },
            error: function(e){
                if(e.responseJSON.hasOwnProperty('errors')){
                    errorSpanNode.text(e.responseJSON.errors.bulkmessage[0]);
                    messageDivNode.addClass('has-danger');
                } else if(e.responseJSON.hasOwnProperty('message')) {
                    toastr.error(e.responseJSON.message);
                } else {
                    toastr.error(e.statusText);
                }
            }
        });
    })
</script>
@endsection
