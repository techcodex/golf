@extends('layouts.dashboard')


@section('header-styles')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
@stop

@section('main-section')
<div class="">
    <table class="table table-bordered text-center" id="availabilityData">
        <thead>
            <tr>
                <td>Name</td>
                <td>City</td>
                <td>Street</td>
                <td>Event Name</td>
                <td>Start Date</td>
                <td>End Date</td>
                <td>Comment</td>
                <td>State</td>
                <td>Status</td>
                <td>Edit/Delete</td>
            </tr>
        </thead>
    </table>
</div>

@stop

@section('footer-scripts')
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script>
        var availability;
        $("document").ready(function () {
            $("#availabilityData").click(function (e) {
                if(e.target.classList.contains('deactive')) {
                    var id = e.target.getAttribute('data-id');
                    var data = {
                        id:id,
                    };
                    $.ajax({
                        url:"{{route('avaialability.dashboard.deactive')}}",
                        data:data,
                        dataType:'JSON',
                        type:'POST',
                        complete:function (jqXHR,textStatus) {
                            if(jqXHR.status == 200) {
                                var result = JSON.parse(jqXHR.responseText);
                                if(result.hasOwnProperty('success')) {
                                    alert("deactive successfulyy");
                                    availability.api().ajax.reload(null);
                                }
                            } else {
                                alert("ssss");
                            }
                        }
                    });
                }
                if(e.target.classList.contains('active')) {
                    var id = e.target.getAttribute('data-id');
                    var data = {
                        id:id,
                    };
                    $.ajax({
                        url:"{{route('avaialability.dashboard.active')}}",
                        data:data,
                        dataType:'JSON',
                        type:'POST',
                        complete:function (jqXHR,textStatus) {
                            if(jqXHR.status == 200) {
                                var result = JSON.parse(jqXHR.responseText);
                                if(result.hasOwnProperty('success')) {
                                    alert("Active successfully");
                                    availability.api().ajax.reload(null);
                                }
                            } else {
                                alert("ssss");
                            }
                        }
                    });
                }
                if(e.target.classList.contains('delete')) {
                    var id = e.target.getAttribute('data-id');
                    $.ajax({
                        url:route("avaialability.dashboard.destroy",{id:id}),
                        dataType:'JSON',
                        type:'GET',
                        complete:function (jqXHR,textStatus) {
                            if(jqXHR.status == 200) {
                                var result = JSON.parse(jqXHR.responseText);
                                if(result.hasOwnProperty('success')) {
                                    alert("Deleted Successfully");
                                    availability.api().ajax.reload(null);
                                } else if(result.hasOwnProperty('error')) {
                                    alert("Opss Something went Wrong");
                                }
                            } else {
                                alert("ssss");
                            }
                        }
                    });
                }
            });
            availability = $("#availabilityData").dataTable({
                responsive: !0,
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax:"{{route('avaialability.dashboard.getAvailability')}}",
                columns:[
                    {data:'display_name',name:'display_name'},
                    {data:'host_city',name:'host_city'},
                    {data:'host_street',name:'host_street'},
                    {data:'event_name',name:'event_name'},
                    {data:'start_date',name:'start_date'},
                    {data:'end_date',name:'end_date'},
                    {data:'comment',name:'comment'},
                    {data:'state',name:'state'},
                    {data:'status',name:'status'},
                    {data:'action',namne:'action'},


                ],
                columnDefs: [
                    {
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    render: function (a, e, t, n) {
                        var editRoute = route('avaialability.dashboard.edit',{id:t.id});
                        return "<a href='"+editRoute+"' class='btn-sm btn-primary'>Edit</a>&nbsp;&nbsp;<a href='#' data-id='"+t.id+"' class='btn-sm btn-danger delete'>Delete</a>";
                    }
                }],
            });

        });
    </script>
@stop