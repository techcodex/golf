@extends('layouts.dashboard')

@section('header-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css">
@stop

@section('main-section')
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session()->get('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session()->get('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if(session()->has('info'))
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            {{ session()->get('info') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <form method="POST" action="{{ route('web.avaiable.store') }}" class="col-md-12 ">
        @csrf
        <div class="form-group">
            <label for="property">Property You Like to Host ?</label>
            <select name="property" id="property" class="form-control @if($errors->has('property')) is-invalid @endif">
                <option value="">-- Select Property --</option>
                @foreach($properties as $property)
                    <option value="{{$property->id}}">{{$property->street_address." ".$property->city." ".$property->zip_code}}</option>
                @endforeach
            </select>
            @if($errors->has('property'))
                <div class="text-danger">
                    {{$errors->first('property')}}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="event">What Event You link to Host On ?</label>
            <select name="event" id="event_id" class="form-control @if($errors->has('event')) is-invalid @endif">
                <option value="">-- Select Event --</option>
            </select>
            @if($errors->has('event'))
                <div class="text-danger">
                    {{ $errors->first('event') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="email">Select Start Date</label>
            <input type="text" class="form-control date @if($errors->has('startDate')) is-invalid @endif" name="startDate" readonly="">
            @if($errors->has('startDate'))
                <div class="text-danger">
                    {{ $errors->first('startDate') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="email">Select End Date</label>
            <input type="text" class="form-control date @if($errors->has('endDate')) is-invalid @endif" name="endDate" readonly>
            @if($errors->has('endDate'))
                <div class="text-danger">
                    {{ $errors->first('endDate') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="Comment">Any Comment You like to sahre it with caddie ?</label>
            <input type="text" name="comment"  class="form-control">
            @if($errors->has('comment'))
                <div class="text-danger">
                    {{ $errors->first('comment') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <input type="submit" value="Add Availiablity" class="btn btn-primary offset-md-5">
        </div>

    </form>
@stop

@section('footer-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>
    <script>
        $(document).ready(function (e) {
            $(".date").datepicker("setDate",new Date());
            $("#property").change(function(e) {
                var id = $("#property").val();
                var data = {
                    id:id,
                };
                $.ajax({
                   url:"{{route('availability.dashboard.get_events')}}",
                   data:data,
                   dataType:'JSON',
                   type:'POST',
                   complete:function (jqXHR,textStatus) {
                       if(jqXHR.status == 200) {
                           var result = JSON.parse(jqXHR.responseText);
                           if(result.hasOwnProperty('success')) {
                                var events = result.events;
                                var output = "";
                                events.forEach(function (event) {
                                    output+= "<option value='"+event.id+"'>"+event.name+"</option>";
                                });

                                $("#event_id > option").remove();
                               $("#event_id").append("<option value=''>--Select Event--</option>")
                                $("#event_id").append(output);
                           } else if(result.hasOwnProperty('error')) {
                                $("#event_id > option").remove();
                                $("#event_id").append("<option value=''>"+result.msg+"</option>");
                           }
                       } else {
                           //swal to be impletement
                           alert("sss");
                       }
                   }
                });
            })
        });
    </script>
@stop
