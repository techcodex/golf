@extends('layouts.app')

@section('header-styles')
    <link property="stylesheet" rel='stylesheet' href='{{asset('js/vendor/the-events-calendar/vendor/bootstrap-datepicker/css/datepicker.css')}}' type='text/css' media='all' />


    <link property="stylesheet" rel='stylesheet' href='{{asset('js/vendor/the-events-calendar/src/resources/css/tribe-events-full.css')}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset('js/vendor/the-events-calendar/src/resources/css/tribe-events-theme.min.css')}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset('js/vendor/the-events-calendar/src/resources/css/tribe-events-full-mobile.min.css')}}' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='{{asset('js/vendor/the-events-calendar/src/resources/css/tribe-events-theme-mobile.min.css')}}' type='text/css' media='only screen and (max-width: 768px)' />
    <link href="{{asset('css/plugin.tribe-events.css')}}" rel="stylesheet" type="text/css">
@stop

@section('main-section')

    <article class="post_item post_item_single tribe_events type-tribe_events">
        <section class="post_content">
            <div id="tribe-events" class="tribe-no-js" data-live_ajax="1" data-datepicker_format="0" data-category="">
                <div id="tribe-events-content-wrapper" class="tribe-clearfix">
                    <input type="hidden" id="tribe-events-list-hash" value="">
                    @if($properties->count() > 0)
                        <div id="tribe-events-bar">
                            <form id="tribe-bar-form" class="tribe-clearfix" name="tribe-bar-form" method="post" action="#">
                                <div id="tribe-bar-collapse-toggle">
                                    Find Property<span class="tribe-bar-toggle-arrow"></span>
                                </div>
                                <div class="tribe-bar-filters">
                                    <div class="tribe-bar-filters-inner tribe-clearfix">
                                        <div class="tribe-bar-search-filter">
                                            <label class="label-tribe-bar-search" for="tribe-bar-search">Search</label>
                                            <input type="text" name="tribe-bar-search" id="tribe-bar-search" value="" placeholder="Search"> </div>
                                        <div class="tribe-bar-submit">
                                            <input class="tribe-events-button tribe-no-param" type="submit" name="submit-bar" value="Find Events" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif
                    <div id="tribe-events-content" class="tribe-events-list">
                        @if($properties->count() == 0)
                            <center><h1>No Properties to Display</h1></center>
                        @else
                            @foreach($properties as $property)
                                <div class="tribe-events-loop">
                                    <div id="post-445" class="type-tribe_events post-445 tribe-clearfix tribe-events-category-tournament tribe-events-venue-439 tribe-events-organizer-437 tribe-events-first">
                                        <h2 class="tribe-events-list-event-title">
                                            Host Name:
                                            <a class="tribe-event-url" href="#" title="Boys&#8217; Amateur Championship" rel="bookmark">
                                                {{$property->display_name}} </a>
                                        </h2>
                                        <div class="tribe-events-event-meta">
                                            <div class="author location">
                                                <div class="tribe-event-schedule-details">
                                                    <span class="tribe-event-date-start"> <b>Booking Start Date:</b> <b>{{$property->start_date->toFormattedDateString()}}</b></span> - <b>Booking End Date:</b> <span class="tribe-event-date-end"><b>{{$property->start_date->toFormattedDateString()}}</b></span> </div>
                                            </div>
                                        </div>
                                        <div class="tribe-events-event-image">
                                            <a href="#"><img src="{{asset($property->host_property->propertyImages[0]->image)}}"  alt="Property Image" /></a>
                                        </div>
                                        <div class="tribe-events-list-event-description tribe-events-content">
                                            <p>Country: <b>{{$property->host_property->country->name}}</b></p>
                                            <p>State / City: <b>{{$property->host_property->state->name}} / {{$property->host_property->city}}</b></p>
                                            <p>Place Type: <b>{{$property->host_property->place_type->name}}</b></p>
                                            <p>Space Available: <b>{{$property->host_property->space->name}}</b></p>
                                            <p>No of Person Accomodates: <b>{{$property->host_property->accommodates}}</b></p>
                                            <a href="{{route('web.avaialability.show',['id'=>$property->id] )}}" class="tribe-events-read-more" rel="bookmark">Book Property &raquo;</a>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div id="tribe-events-footer">
                    <h3 class="screen-reader-text" tabindex="0">Events List Navigation</h3>
                    <ul class="tribe-events-sub-nav">

                        <li class="tribe-events-nav-previous" style="margin-left: 40%;">
                            {{ $properties->links() }}
                        </li>

                    </ul>
                </div>



            </div>
            <div class="tribe-clear"></div>

            </div>
            <div class="tribe-events-after-html"></div>
            </div>
            <div class="cL"></div>
        </section>
    </article>

@stop

@section('footer-scripts')


@stop

