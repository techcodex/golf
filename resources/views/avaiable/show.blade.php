@extends('layouts.app')

@section('header-main-style')
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-full.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-theme.min.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-full-mobile.min.css")}}' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-theme-mobile.min.css")}}' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/plugin.tribe-events.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/lightbox/lightbox.min.css")}}' type='text/css' media='all' />

    <style>
        .property-image  {
            margin-top:1%;
            margin-left:1%;
        }
        img{
            max-width: 300px;
            max-height: 300px;
        }
    </style>
@stop

@section('header-styles')

@stop

@section('main-section')
    <article class="post_item post_item_single tribe_events type-tribe_events">
        <section class="post_content">
            <div id="tribe-events" class="tribe-no-js" data-live_ajax="1" data-datepicker_format="0" data-category="">
                <div class="tribe-events-before-html"></div>
                <span class="tribe-events-ajax-loading">
                    <img class="tribe-events-spinner-medium" src="js/vendor/the-events-calendar/src/resources/images/tribe-loading.gif" alt="Loading Events"/>
                </span>
                <div id="tribe-events-content" class="tribe-events-single">
                    <p class="tribe-events-back">
                        <a href="#"> &laquo; All Events</a>
                    </p>
                    <h1 class="tribe-events-single-event-title">{{strtoupper($availability->display_name)}}</h1>
                    <div class="tribe-events-schedule tribe-clearfix">
                        <h2><span class="tribe-event-date-start">{{$availability->start_date->toFormattedDateString()}}</span> - <span class="tribe-event-date-end"> {{$availability->end_date->toFormattedDateString()}}</span></h2>
                    </div>
                    <div id="tribe-events-header" data-title="Upcoming Events &#8211; Nationwide Shriners Hospitals for Children Open &#8211; N7 Golf Club">
                        <h3 class="tribe-events-visuallyhidden">Event Navigation</h3>
                        <ul class="tribe-events-sub-nav">
                            <li class="tribe-events-nav-previous"><a href="#"><span>&laquo;</span> SENIORS SPRING OPEN MEETING</a></li>
                            <li class="tribe-events-nav-next"></li>
                        </ul>
                    </div>
                    <div id="post-436" class="tribe_events type-tribe_events">
                        {{--<div class="tribe-events-event-image">--}}
                            @foreach($availability->host_property->propertyImages as $image )
                                <a href="{{asset($image->image)}}" data-lightbox="property image"> <img  src="{{asset($image->image)}}" alt="property Images" class="property-image" /></a>
                            @endforeach
                        {{--</div>--}}

                        @if(Auth::user()->type != 1)
                            <div class="tribe-events-single-event-description tribe-events-content">
                                <p style="color:red; margin-left:1%; margin-top: 1%;">Please Singup To participate in events.</p>
                            </div>
                        @else
                            <div class="tribe-events-single-event-description tribe-events-content">
                                <p style="margin:1% 0% 0% 1%;">Book Property for Event.</p>
                            </div>
                            <form action="{{ route('messenger') }}" method="POST">
                                @csrf
                                <input type="hidden" name="host" value="{{ $availability->host_id }}" />
                                <button type="submit" class="tribe-events-ical tribe-events-button sc_button sc_button_style_filled" href="{{ route('messenger') }}" title="Chat">Chat</button>
                            </form>
                            <form action="{{route('stay.store')}}" method="post">
                                @csrf
                                <div class="tribe-events-cal-links">
                                    <input type="hidden" name="availability_id" value="{{$availability->id}}">
                                    <input type="hidden" name="event_id" value="{{$availability->event_id}}">
                                    <input type="hidden" name="host_id" value="{{$availability->host_id}}">
                                    <input type="hidden" name="property_id" value="{{ $availability->host_property_id }}">
                                    <input type="submit" class="tribe-events-gcal tribe-events-button" value="Book Property" title="Singup as Caddie">
                                </div>
                            </form>
                        @endif

                        <div class="tribe-events-single-section tribe-events-event-meta primary tribe-clearfix">
                            <div class="tribe-events-meta-group tribe-events-meta-group-details">
                                <h3 class="tribe-events-single-section-title"> Details </h3>
                                <dl>
                                    <dt> Booking Start: </dt>
                                    <dd>
                                        <abbr class="tribe-events-abbr updated published dtstart" title="2017-06-16"> {{$availability->start_date}} </abbr>
                                    </dd>
                                    <dt> Booking End: </dt>
                                    <dd>
                                        <abbr class="tribe-events-abbr dtend" title="2017-06-20">{{$availability->end_date}}</abbr>
                                    </dd>
                                    <dt>Source:</dt>
                                    <dd class="tribe-events-event-categories"><a href="#" rel="tag">@if($availability->event->source == 1) pga.com @else lpga.com @endif</a></dd>
                                    <dt>Event Type:</dt>
                                    <dd class="tribe-event-tags"><a href="#" rel="tag">@if($availability->event->type == 1) PGA @else Lpga @endif</a></dd>
                                </dl>
                            </div>
                            <div class="tribe-events-meta-group tribe-events-meta-group-organizer">
                                <h3 class="tribe-events-single-section-title">Organizer</h3>
                                <dl>
                                    <dt class="d_none"></dt>
                                    <dd class="tribe-organizer">
                                    </dd>
                                    <dt>
                                        Phone: </dt>
                                    <dd class="tribe-organizer-tel">
                                        +1(800)123-45-67 </dd>
                                    <dt>
                                        Email: </dt>
                                    <dd class="tribe-organizer-email">
                                        support@casadecaddie.com </dd>
                                    <dt>
                                        Host Email: </dt>
                                    <dd class="tribe-organizer-email">
                                        {{$availability->host->email}} </dd>
                                    <dt>
                                        Host Cell Number: </dt>
                                    <dd class="tribe-organizer-email">
                                        {{$availability->host->cell_number}} </dd>
                                </dl>
                            </div>
                        </div>

                    </div>
                    <div id="tribe-events-footer">
                        <h3 class="tribe-events-visuallyhidden">Event Navigation</h3>
                        <ul class="tribe-events-sub-nav">
                            <li class="tribe-events-nav-previous btn_w-m"><a href="{{route('contact')}}"> Contact Us</a></li>
                            <li class="tribe-events-nav-next"></li>
                        </ul>
                    </div>
                </div>
                <div class="tribe-events-after-html"></div>
            </div>
            <div class="cL"></div>
        </section>
    </article>

@stop
@section('footer-scripts')
<script src="{{asset('js/vendor/lightbox/lightbox.min.js')}}"></script>
<script>
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true,
        'maxWidth' : 1000,
        'maxHeight' : 1000,
        'positionFromTop':50,

    });
</script>
@stop
