@extends('layouts.app')

@section('header-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css">
@stop

@section('main-section')

<div class="sc_section margin_top_large margin_bottom_xxxhuge aligncenter w70_per" data-animation="animated fadeInUp normal">
    <div class="sc_section_inner">
        <h2 class="sc_title margin_top_null margin_bottom_xmedium aligncenter">Add Property Avaialability Form</h2>
        <div class="sc_form_wrap">
            <div id="sc_form_1" class="sc_form sc_form_style_form_1 cf1LeftText ">
                <form id="sc_form_1_1" data-formtype="form_1" method="post" action="{{ route('web.avaiable.store') }}">
                    @csrf
                    <div class="sc_form_info">
                        <div class="sc_form_item sc_form_field label_over">
                            <p style="margin-bottom: 0; color: black; font-size: 18px;">What Property would you like to Host?</p>
                            <div class="sc_form_item sc_form_field label_over" style="margin-top:0;">
                                    <label class="required" for="sc_form_type">Property:</label>
                                    <select name="property" id="property" @if($errors->has('property')) style="width: 100%; border: 2px solid red" @else style="width: 100%; border: 2px solid #282828" @endif>
                                        <option value="">--Select Property -- </option>
                                        @foreach($properties as $property)
                                            <option value="{{$property->id}}">{{$property->street_address." ".$property->city." ".$property->zip_code}}</option>
                                        @endforeach
                                    </select>
                                   @if($errors->has('property'))<span class="help-block">{{ $errors->first('property') }}</span> @endif
                            </div>
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <p style="margin-bottom: 0; color: black; font-size: 18px;">What Event would you like to Host For?</p>
                            <div class="sc_form_item sc_form_field label_over" style="margin-top:0;">
                                    <label class="required" for="sc_form_type">Event:</label>
                                    <select name="event" id="event_id" @if($errors->has('event')) style="width: 100%; border: 2px solid red" @else style="width: 100%; border: 2px solid #282828" @endif>
                                        @if($events->count() == 0)
                                            <option value="">-- Select Event --</option>
                                        @else
                                            <option value="">-- Select Event --</option>
                                            @foreach ($events as $event)
                                                <option value="{{ $event->id }}" @if(old('event') == $event->id) selected @endif>{{ $event->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                   @if($errors->has('event'))<span class="help-block">{{ $errors->first('event') }}</span> @endif
                            </div>
                        </div>

                        <div class="sc_form_item sc_form_field label_over">
                            <p style="margin-bottom: 0; color: black; font-size: 18px;">What Dates would you like to host?</p>
                            <div class="sc_form_item sc_form_field label_over" style="margin-top:0;">
                                    <label class="required" for="sc_form_type">Start Date:</label>
                                    <input id="sc_form_type" type="text" readonly data-toggle="datepicker" @if(old('startDate') == null) value="" @else value="{{ old('startDate') }}" @endif name="startDate" placeholder="Select Start Date" @if($errors->has('startDate'))
                                    style="border: 2px solid red" @endif>
                                   @if($errors->has('startDate'))<span class="help-block">{{ $errors->first('startDate') }}</span> @endif
                            </div>
                            <div class="sc_form_item sc_form_field label_over" style="margin-top:0;">
                                    <label class="required" for="sc_form_type">End Date:</label>
                                    <input id="sc_form_type" type="text" readonly data-toggle="datepicker" @if(old('endDate') == null) value="" @else value="{{ old('endDate') }}" @endif name="endDate" placeholder="Select End Date" @if($errors->has('endDate'))
                                    style="border: 2px solid red" @endif>
                                   @if($errors->has('endDate'))<span class="help-block">{{ $errors->first('endDate') }}</span> @endif
                            </div>
                        </div>
                        <p style="margin-bottom: 0; color: black; font-size: 18px;">Any commnets you would like to share with the Caddie?</p>
                        <div class="sc_form_item sc_form_field label_over" style="margin-top: 10px;">
                            <label class="required" for="sc_form_firstname">City:</label>
                            <input id="sc_form_firstname" value="{{ old('commnets') }}" @if($errors->has('comments')) style="border:
                            2px solid red" @endif type="text" name="comment" placeholder="Any Comments">
                            @if($errors->has('comments')) <span class="help-block">{{ $errors->first('comments') }}</span> @endif
                        </div>
                    </div>
                    <div class="sc_form_item sc_form_button">
                        <button type="submit">Add Property Avaialability</button>
                    </div>
                    <div class="result sc_infobox"></div>
                </form>
            </div>
        </div>
    </div>
</div>









@stop
@section('footer-scripts')
<script>
    $("document").ready(function (e) {
        $('html,body').animate({
            scrollTop:$(".sc_section_inner").offset().top
        }, 2000);
        $("#sc_form_email").focus();
    });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>

<script>
$('[data-toggle="datepicker"]').datepicker("setDate",new Date());
$("#property").change(function(e) {
    var id = $("#property").val();
    var data = {
        id:id,
    };
    $.ajax({
        url:"{{route('availability.dashboard.get_events')}}",
        data:data,
        dataType:'JSON',
        type:'POST',
        complete:function (jqXHR,textStatus) {
            if(jqXHR.status == 200) {
                var result = JSON.parse(jqXHR.responseText);
                if(result.hasOwnProperty('success')) {
                    var events = result.events;
                    var output = "";
                    events.forEach(function (event) {
                        output+= "<option value='"+event.id+"'>"+event.name+"</option>";
                    });

                    $("#event_id > option").remove();
                    $("#event_id").append("<option value=''>--Select Event--</option>")
                    $("#event_id").append(output);
                } else if(result.hasOwnProperty('error')) {
                    $("#event_id > option").remove();
                    $("#event_id").append("<option value=''>"+result.msg+"</option>");
                }
            } else {
                //swal to be impletement
                alert("sss");
            }
        }
    });
})
</script>

@stop
