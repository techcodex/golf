@extends('layouts.app')


@section('header-styles')
    <style>
        .bot{
            margin-bottom: 30px;
        }
        .mar{
            margin-top:40px;
        }
    </style>
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/vendor/bootstrap-datepicker/css/datepicker.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-full.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-theme.min.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-full-mobile.min.css")}}' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-theme-mobile.min.css")}}' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/plugin.tribe-events.css")}}' type='text/css' media='all' />

@stop


@section('main-section')
    <article class="post_item post_item_single tribe_events type-tribe_events">
        <section class="post_content">
            <div id="tribe-events" class="tribe-no-js" data-live_ajax="1" data-datepicker_format="0" data-category="">
                <div id="tribe-events-content-wrapper" class="tribe-clearfix">
                    <div id="tribe-events-content" class="tribe-events-list">
                        <section class="post_author author vcard">
                            <div class="post_author_info">
                                <center><h2><u>Vist Our Blog</u></h2></center>
                                <h3 class="top">Access up to date information on events, news and trends in the golf world on our blog. From local up and comers to all time pros and everything in between, you won’t miss a thing!</h3>
                            </div>
                        </section>
                        <span class='tribe-events-list-separator-month'></span>
                        <div class="tribe-events-loop">

                            @foreach($blogs as $blog)
                                <div id="post-445" class="type-tribe_events post-445 tribe-clearfix tribe-events-category-tournament tribe-events-venue-439 tribe-events-organizer-437 tribe-events-first">
                                    <h2 class="tribe-events-list-event-title">
                                    <a class="tribe-event-url bot" href="{{route('blog.show', ['slug' => $blog->slug])}}" title="{{ $blog->title }}" rel="bookmark">
                                        <u>{{ $blog->title }}</u>  </a>
                                    </h2>
                                    <div class="mar tribe-events-event-image " style="margin-top: 20px;">
                                        <a href="{{ route('blog.show', ['slug' => $blog->slug]) }}"><img src="{{ asset($blog->featured) }}" style="width: 300px; height: 163px;"  alt="image-3" /></a>
                                    </div>
                                    <div class="mar tribe-events-list-event-description tribe-events-content ">
                                        <p>{!! str_limit(strip_tags($blog->description),  150) !!}</p>
                                        <a href="{{route('blog.show', ['slug' => $blog->slug])}}" class="tribe-events-read-more" rel="bookmark">Read More &raquo;</a>
                                    </div>
                                </div>
                                <span class='tribe-events-list-separator-month'></span>
                            @endforeach
                            {{ $blogs->links() }}

                        </div>
                    </div>
                    <div class="tribe-clear"></div>
                </div>
                <div class="tribe-events-after-html"></div>
            </div>
            <div class="cL"></div>
        </section>
    </article>
@stop


@section('footer-scripts')

@stop
