<div class="row">
    <div class="col-lg-12">
        @if(Session::has('success'))
        <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check"></i>
            </div>
            <div class="m-alert__text">
                <strong>Created!</strong> {{ Session::get('success') }}
            </div>
            <div class="m-alert__close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
            </div>
        </div>
        @endif

        <!--begin::Portlet-->
        <div class="m-portlet">

            <!--begin::Form-->
            <form id="frmTarget" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data"
                action="{{ route('package.store') }}" method="POST">
                @csrf
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 @if($errors->has('place_name')) has-danger  @endif">
                            <label>Place Name:</label>
                            <input type="text" value="{{ old('place_name') }}" name="place_name" class="form-control m-input m-form--state" placeholder="Enter full name of place">                            @if($errors->has('place_name')) <span class="form-control-feedback">{{ $errors->first('place_name') }}</span>                            @endif
                        </div>
                        <div class="col-lg-6 @if($errors->has('price')) has-danger  @endif">
                            <label for="price">Price:</label>
                            <input value="{{ old('price') }}" type="number" id="" name="price" class="form-control m-input" placeholder="Enter price of the package">                            @if($errors->has('price')) <span class="form-control-feedback">{{ $errors->first('price') }}</span>                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 @if($errors->has('pick_time')) has-danger @endif">
                            <label class="">Pick Time:</label>
                            <div class="input-group timepicker">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                                <i class="la la-clock-o"></i>
                                            </span>
                                </div>
                                <input value="{{ old('pick_time') }}" type='text' name="pick_time" class="form-control" id="m_timepicker_1" readonly placeholder="Select time"
                                    type="text" />
                            </div>
                            @if($errors->has('pick_time')) <span class="form-control-feedback">{{ $errors->first('pick_time') }}</span>                            @endif
                        </div>
                        <div class="col-lg-6 @if($errors->has('drop_time')) has-danger @endif">
                            <label>Drop Time:</label>
                            <div class="input-group timepicker">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                                <i class="la la-clock-o"></i>
                                            </span>
                                </div>
                                <input value="{{ old('drop_time') }}" type="text" name="drop_time" id="m_timepicker_1" class="form-control m-input" readonly=""
                                    placeholder="Drop Time">
                            </div>
                            @if($errors->has('drop_time')) <span class="form-control-feedback">{{ $errors->first('drop_time') }}</span>                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 @if($errors->has('currency')) has-danger @endif">
                            <label>Currency:</label>
                            <div class="dropdown bootstrap-select form-control m-bootstrap-select m_">
                                <select name="currency" class="form-control m-bootstrap-select m_selectpicker" tabindex="-98">
                                <option value="AED" @if(old('currency') != null && old('curreny') == 'AED') selected @endif>AED</option>
                                <option value="USD" @if(old('currency') != null && old('curreny') == 'USD') selected @endif>USD</option>
                            </select>
                            </div>
                            @if($errors->has('currency')) <span class="form-control-feedback">{{ $errors->first('currency') }}</span>                            @endif
                        </div>
                        <div class="col-lg-6 @if($errors->has('location')) has-danger  @endif">
                            <label>Package Location:</label>
                            <input value="{{ old('location') }}" type="text" id="" name="location" class="form-control m-input" placeholder="Enter location of the package">                            @if($errors->has('location')) <span class="form-control-feedback">{{ $errors->first('location') }}</span>                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-12 @if($errors->has('desc')) has-danger @endif">
                            <label>Description:</label>
                            <textarea name="desc" class="summernote" id="m_summernote_1">{{ old('desc') }}</textarea> @if($errors->has('desc'))
                            <span class="form-control-feedback">{{ $errors->first('desc') }}</span> @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 @if($errors->has('images')) has-danger  @endif">
                            <label>Package Images:</label>
                            <input type="file" multiple name="images[]" class="form-control m-input m-form--state" placeholder="Enter full name of place">                            @if($errors->has('images')) <span class="form-control-feedback">{{ $errors->first('images') }}</span>                            @endif
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="row">
                            <div class="col-lg-6">
                                <button type="submit" id="button" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->

    </div>
</div>
