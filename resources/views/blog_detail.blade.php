@extends('layouts.app')


@section('header-styles')
    <style>
        .top{
            margin-top: 10px;
        }
    </style>

@stop


@section('main-section')
    <article class="post_item post_item_single post type-post">
        <section class="post_featured">
            <div class="post_thumb" data-image="$blog->featured" data-title="{{$blog->title}}">
                <center>
                <img  alt="{{ $blog->title }}" src="{{asset($blog->featured)}}">
                </center>
            </div>
        </section>
        <section class="post_content">
           {!! $blog->description !!}
        </section>
    </article>
@stop


@section('footer-scripts')

@stop
