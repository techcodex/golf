@extends('layouts.app')


@section('header-styles')


@stop


@section('main-section')

    <article class="post_item post_item_single page">
        <div class="post_content">
            <section>
                <div class="content_container">

                    <div class="sc_section margin_top_large margin_bottom_xxxhuge aligncenter w70_per" data-animation="animated fadeInUp normal">
                        <div class="sc_section_inner">
                            <h2 class="sc_title margin_top_null margin_bottom_xmedium aligncenter">We Would Love to Hear From You! </h2>
                            <div  class="sc_form_wrap">
                                <div id="sc_form_1" class="sc_form sc_form_style_form_1 cf1LeftText ">
                                    <form id="sc_form_1_1" data-formtype="form_1" method="post" action="{{route('contact.store')}}">
                                        {{csrf_field()}}
                                        <div class="sc_form_info">
                                            <div class="sc_form_item sc_form_field label_over">
                                                <label class="required" for="sc_form_username">User Name:</label>
                                                <input id="sc_form_username" type="text" name="username" placeholder="User Name">
                                                @if($errors->has('user_name'))<span class="help-block">{{ $errors->first('user_name') }}</span> @endif
                                            </div>
                                            <div class="sc_form_item sc_form_field label_over">
                                                <label class="required" for="sc_form_email">E-mail</label>
                                                <input id="sc_form_email" type="text" name="email" placeholder="E-mail *">
                                                @if($errors->has('email'))<span class="help-block">{{ $errors->first('email') }}</span> @endif
                                            </div>
                                            <div class="sc_form_item sc_form_field label_over">
                                                <label class="required" for="sc_form_subj">Subject</label>
                                                <input id="sc_form_subj" type="text" name="subject" placeholder="Subject">
                                                @if($errors->has('subject'))<span class="help-block">{{ $errors->first('subject') }}</span> @endif
                                            </div>
                                        </div>
                                        <div class="sc_form_item sc_form_message label_over">
                                            <label class="required" for="sc_form_message">Message</label>
                                            <textarea id="sc_form_message" name="message" placeholder="Message"></textarea>
                                            @if($errors->has('message'))<span class="help-block">{{ $errors->first('message') }}</span> @endif
                                        </div>
                                        <div class="sc_form_item sc_form_button">
                                            <button>Send Message</button>
                                        </div>
                                        <div class="result sc_infobox"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </article>

@stop


@section('footer-scripts')


@stop