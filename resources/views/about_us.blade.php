@extends('layouts.app')


@section('header-styles')
    <link property="stylesheet" rel='stylesheet' href='{{asset('js/vendor/swiper/swiper.css')}}' type='text/css' media='all' />

@stop


@section('main-section')
    <section>
        <div class="content_container">
            <div class="columns_wrap sc_columns">
                <div class="column-1_2 sc_column_item odd first" data-animation="animated fadeInLeft normal">
                    <figure class="sc_image sc_image_shape_square w570 h477">
                        <img src="images/page_about_us_01-570x477.jpg" alt="" />
                    </figure>
                </div><div class="column-1_2 sc_column_item even" data-animation="animated fadeInRight normal">
                    <h2 class="sc_title sc_title_regular margin_top_null margin_bottom_small">WELCOME</h2>
                    <div class="text_column">
                        <p>At Casa de Caddie, we invite all credentialed PGA/LPGA Tour Players and Caddies to join our
                            membership family. Casa de Caddie, helps alleviate the lodging costs incurred as a result of
                            travelling on the PGA/LPGA Tours. If a caddie’s player isn’t playing well, the caddie’s pay is minimal.
                            Some caddies have occasionally slept in their cars to avoid costs of staying in hotels. The goal of
                            Casa de Caddie is to pair these tour professionals with host families who are willing to open their
                            homes at pro tour events. It is our hope that caddies and hosts will benefit from enjoying each
                            other’s company as they share their appreciation and love for the game of golf. Your host families at
                            Casa de Caddie provide a home away from home, a place to rest after a long day at the course.
                            Welcome home! Relax and leave your bag at the door!
                        </p>
                    </div>
                    <a href="#" class="sc_button sc_button_square sc_button_style_border_b sc_button_size_small margin_top_xsmall"> Contact Us</a>
                </div>
            </div>
        </div>
    </section>
    <section id="qa">
        <div class="content_container">
            <div class="sc_section margin_top_large margin_bottom_xxxmedium" data-animation="animated fadeInUp normal">
                <div class="sc_section_inner">
                    <div class="columns_wrap sc_columns">
                        <div class="column-1_3 sc_column_item odd first">
                            <div class="sc_section margin_bottom_xsmall aligncenter">
                                <div class="sc_section_inner">
                                    <figure class="sc_image margin_top_null margin_bottom_null">
                                        <span class="fa fa-award fa-4x"></span>
                                    </figure>
                                </div>
                            </div>
                            <div class="sc_section fsz-1_429em lh-1_3em fw-100">
                                <div class="sc_section_inner">
                                    <div class="text_column crtext">
                                        <p class="aligncenter f_st-ital mb-0_45em" style="font-size: 30px;"><b><u>Quality Connection</u></b></p>
                                        <p class="aligncenter f_st-ital lightgrey_color2">
                                            At the time of registration PGA/LPGA Tour Players and Caddies
                                            upload PGA Tour Credentials for verification. Once approved securely
                                            a caddie/player can pay the membership fee and search for dates at
                                            anytime they need lodging. Our database will connect them with the
                                            first available host at the site a pro tournament is taking place.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div><div class="column-1_3 sc_column_item even">
                            <div class="sc_section margin_bottom_xsmall aligncenter">
                                <div class="sc_section_inner">
                                    <figure class="sc_image margin_top_null margin_bottom_null">
                                        <span class="fa fa-wallet fa-4x"></span>
                                    </figure>
                                </div>
                            </div>
                            <div class="sc_section fsz-1_429em lh-1_3em fw-100">
                                <div class="sc_section_inner">
                                    <div class="text_column crtext">
                                        <p class="aligncenter f_st-ital mb-0_45em" style="font-size: 30px;"><b><u>Cost Efficient</u></b></p>
                                        <p class="aligncenter f_st-ital lightgrey_color2 mb-0_45em">
                                            For a minimal fee of $30 per month (less than the cost of a hotel room
                                            for one night) caddies/players can save on lodging costs and stay with
                                            people who share the love of the game. Whether the service is utilized
                                            or not for a particular month, have peace of mind knowing the service
                                            is available. Planning is convenient every stop along tour domestically;
                                            no more searching various hotels and hostels for the best price.
                                            Family members, friends, corporations or generous citizens can adopt
                                            a caddie or several caddies by paying the yearly membership through
                                            our site under the Adopt A Caddie section.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div><div class="column-1_3 sc_column_item odd">
                            <div class="sc_section margin_bottom_xsmall aligncenter">
                                <div class="sc_section_inner">
                                    <figure class="sc_image margin_top_null margin_bottom_null">
                                        <span class="fa fa-desktop fa-4x"></span>
                                    </figure>
                                </div>
                            </div>
                            <div class="sc_section fsz-1_429em lh-1_3em fw-100">
                                <div class="sc_section_inner">
                                    <div class="text_column crtext">
                                        <p class="aligncenter f_st-ital mb-0_45em" style="font-size: 30px;"><b><u>User Friendly</u></b></p>
                                        <p class="aligncenter f_st-ital lightgrey_color2 mb-0_45em">
                                            The date and destination of your tour stop in the United States is
                                            made easy as local golf enthusiasts are willing to open their homes
                                            out of the kindness of their hearts in exchange for a few good stories
                                            and a chance to share some positive memories. Simply enter your
                                            dates and destination information and be paired with a local host
                                            family.</p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop


@section('footer-scripts')
    <script type='text/javascript' src='{{asset('js/vendor/swiper/swiper.min.js')}}'></script>
    <script>
        $("document").ready(function (e) {
            $('html,body').animate({
                scrollTop:$("#qa").offset().top
            }, 2000);
        });
    </script>
@stop