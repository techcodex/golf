<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Property Registered</title>
</head>
<body>
    <div>
        <h1>Thanks for registering your property at CdC</h1>
         <p>Owner Name: {{ $property->owner_name }}</p>
         <p>Host Email: {{ $property->host_email }}</p>
         <p>Host Phone: {{ $property->host_phone }}</p>
         <p>Street Address: {{ $property->street_address }}</p>
         <p>City: {{ $property->city }}</p>
    </div>
</body>
</html>
