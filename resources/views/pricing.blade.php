@extends('layouts.pricing')

@section('header-style')

@endsection

@section('main-section')
    <section>
        <div class="container-fluid sc_bg_img_1">
            <div class="row_inv inverse_colors">
                <div class="sc_content content_wrap margin_top_xlarge margin_bottom_null" data-animation="animated fadeInUp normal">
                    <h2 class="sc_title margin_top_null margin_bottom_small aligncenter">CLUB MEMBERSHIP</h2>
                    <div class="sc_section fsz-1_286em lh-1_3em fw-100">
                        <div class="sc_section_inner">
                            <div class="text_column crtext">
                                <p class="aligncenter f_st-ital">What can you do at the Casadecaddie ?</p>
                            </div>
                        </div>
                    </div>
                    <div class="sc_section sc_section_block margin_top_tiny margin_bottom_small aligncenter w70_per">
                        <div class="sc_section_inner">
                            <div class="sc_iconseparator icons1">
                                <div class="sc_iconseparator_item"><span class="sc_iconseparator_holder sc_iconseparator_holder_left"><span class="sc_iconseparator_line"></span></span>
                                    <div class="sc_iconseparator_element_outer">
                                        <div class="sc_iconseparator_element_iner"><span class="icon-vectorsmartobject3 block fw-100 fsz-3_3em white"></span></div>
                                    </div>
                                    <span class="sc_iconseparator_holder sc_iconseparator_holder_right"><span class="sc_iconseparator_line"></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sc_content content_wrap margin_top_tiny margin_bottom_xxlarge">
                    <div class="columns_wrap sc_columns">
                            @foreach($plans as $plan)<div class="column-1_3 sc_column_item odd first" data-animation="animated fadeInLeft normal">
                            <div class="sc_price_block sc_price_block_style_3 scheme_original">
                                <div class="sc_price_block_title"><span>{{ $plan->name }}</span></div>
                                <div class="sc_price_block_money">
                                    <div class="sc_price"><span class="sc_price_currency">$</span><span class="sc_price_money">{{ $plan->price }}</span><span class="sc_price_period">@if($plan->frequency == 'Month') mo @elseif($plan->frequency == 'Year') yr @endif</span></div>
                                </div>
                                <div class="sc_price_block_description">
                                    <p class="align_center">{{ $plan->description }}</p>
                                    <p>
                                </div>
                                <div class="sc_price_block_link"><a href="{{ route('plan.agreement', ['id' => $plan->plan_id]) }}" class="sc_button sc_button_style_border sc_button_size_small">Subscribe</a></div>
                            </div>

                        </div>@endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(e) {
//            $(".content_wrap").removeClass("content_wrap");
//            $(".content").removeClass("content");
//            $(".page_content_wrap page_paddings_yes").removeClass(".page_content_wrap");

        });
    </script>
@stop

