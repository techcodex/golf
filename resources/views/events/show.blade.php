@extends('layouts.app')

@section('header-main-style')
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-full.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-theme.min.css")}}' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-full-mobile.min.css")}}' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("js/vendor/the-events-calendar/src/resources/css/tribe-events-theme-mobile.min.css")}}' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='{{asset("css/plugin.tribe-events.css")}}' type='text/css' media='all' />
@stop

@section('header-styles')

@stop

@section('main-section')
    <article class="post_item post_item_single tribe_events type-tribe_events">
        <section class="post_content">
            <div id="tribe-events" class="tribe-no-js" data-live_ajax="1" data-datepicker_format="0" data-category="">
                <div class="tribe-events-before-html"></div>
                <span class="tribe-events-ajax-loading">
                    <img class="tribe-events-spinner-medium" src="js/vendor/the-events-calendar/src/resources/images/tribe-loading.gif" alt="Loading Events"/>
                </span>
                <div id="tribe-events-content" class="tribe-events-single">
                    <p class="tribe-events-back">
                        <a href="#"> &laquo; All Events</a>
                    </p>
                    <h1 class="tribe-events-single-event-title">{{strtoupper($event->name)}}</h1>
                    <div class="tribe-events-schedule tribe-clearfix">
                        <h2><span class="tribe-event-date-start">{{strtoupper($event->start_date)}}</span> - <span class="tribe-event-date-end">{{strtoupper($event->end_date)}}</span></h2>
                    </div>
                    <div id="tribe-events-header" data-title="Upcoming Events &#8211; Nationwide Shriners Hospitals for Children Open &#8211; N7 Golf Club">
                        <h3 class="tribe-events-visuallyhidden">Event Navigation</h3>
                        <ul class="tribe-events-sub-nav">
                            <li class="tribe-events-nav-previous"><a href="#"><span>&laquo;</span> SENIORS SPRING OPEN MEETING</a></li>
                            <li class="tribe-events-nav-next"></li>
                        </ul>
                    </div>
                    <div id="post-436" class="tribe_events type-tribe_events">
                        <div class="tribe-events-event-image">
                            <img src="{{asset($event->event_image)}}" alt="image-2"/>
                        </div>

                        @if(!Auth::user())
                            <div class="tribe-events-single-event-description tribe-events-content">
                                <p>Please Singup To participate in events.</p>
                            </div>
                            <div class="tribe-events-cal-links"><a class="tribe-events-gcal tribe-events-button" href="{{route('register')}}" title="Signup as caddie">Singup as Caddie</a><a class="tribe-events-ical tribe-events-button" href="{{route('register')}}" title="Singup as host">Signup as Host</a></div>
                        @else
                            <div class="tribe-events-single-event-description tribe-events-content">
                                <p>All Available Host Properties.</p>
                            </div>
                            <div class="tribe-events-cal-links">
                                <a class="tribe-events-gcal tribe-events-button" href="{{route("web.available_properties",['id'=>$event->id])}}" title="Signup as caddie">Host Properties</a>
                                <a class="tribe-events-gcal tribe-events-button" href="{{route("stay.nearestStay",['id'=>$event->id])}}" title="Signup as caddie">Book Nearest Host Property</a>
                            </div>
                        @endif


                        <div class="tribe-events-single-section tribe-events-event-meta primary tribe-clearfix">
                            <div class="tribe-events-meta-group tribe-events-meta-group-details">
                                <h3 class="tribe-events-single-section-title"> Details </h3>
                                <dl>
                                    <dt> Start: </dt>
                                    <dd>
                                        <abbr class="tribe-events-abbr updated published dtstart" title="2017-06-16"> {{$event->start_date}} </abbr>
                                    </dd>
                                    <dt> End: </dt>
                                    <dd>
                                        <abbr class="tribe-events-abbr dtend" title="2017-06-20">{{$event->end_date}}</abbr>
                                    </dd>
                                    <dt>Source:</dt>
                                    <dd class="tribe-events-event-categories"><a href="#" rel="tag">@if($event->source == 1) pga.com @else lpga.com @endif</a></dd>
                                    <dt>Event Type:</dt>
                                    <dd class="tribe-event-tags"><a href="#" rel="tag">@if($event->type == 1) PGA @else Lpga @endif</a></dd>
                                </dl>
                            </div>
                            <div class="tribe-events-meta-group tribe-events-meta-group-organizer">
                                <h3 class="tribe-events-single-section-title">Organizer</h3>
                                <dl>
                                    <dt class="d_none"></dt>
                                    <dd class="tribe-organizer">
                                    </dd>
                                    <dt>
                                        Phone: </dt>
                                    <dd class="tribe-organizer-tel">
                                        +1(800)123-45-67 </dd>
                                    <dt>
                                        Email: </dt>
                                    <dd class="tribe-organizer-email">
                                        support@casadecaddie.com </dd>
                                </dl>
                            </div>
                        </div>

                    </div>
                    <div id="tribe-events-footer">
                        <h3 class="tribe-events-visuallyhidden">Event Navigation</h3>
                        <ul class="tribe-events-sub-nav">
                            <li class="tribe-events-nav-previous btn_w-m"><a href="{{route('contact')}}"> Contact Us</a></li>
                            <li class="tribe-events-nav-next"></li>
                        </ul>
                    </div>
                </div>
                <div class="tribe-events-after-html"></div>
            </div>
            <div class="cL"></div>
        </section>
    </article>

@stop
@section('footer-scripts')

@stop
