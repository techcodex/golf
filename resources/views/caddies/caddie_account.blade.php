@extends('layouts.app')


@section('header-styles')
    <style>
        input[type="checkbox"] {
            margin-left: 10px;

        }
    </style>

@stop


@section('main-section')

<div class="sc_section margin_top_large margin_bottom_xxxhuge aligncenter w70_per" data-animation="animated fadeInUp normal">
    <div class="sc_section_inner">
        <h2 class="sc_title margin_top_null margin_bottom_xmedium aligncenter">My Account</h2>
        <div  class="sc_form_wrap">
            <div id="sc_form_1" class="sc_form sc_form_style_form_1 cf1LeftText ">
                <form id="sc_form_1_1" data-formtype="form_1" method="post" action="{{route('caddie.store')}}">
                    {{csrf_field()}}
                    <div class="sc_form_info">
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_email">Name:</label>
                            <input id="sc_form_email" type="text" name="caddie_name" placeholder="Name *" readonly="" value="{{$full_name}}" style="{{$errors->has('caddie_name') ? 'border:2px solid red' : ''}}">
                        </div>

                    </div>
                    <div class="sc_form_info">
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_email">Email:</label>
                            <input id="email" type="email" style="padding: 18px; {{$errors->has('email') ? 'border:2px solid red' : ''}}" name="email" value="{{$user->email}}" readonly="" placeholder="Email" >
                        </div>
                    </div>

                    <div class="sc_form_info">
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_email">Phone:</label>
                            <input id="phone" type="text" style="padding: 18px;" name="phone" value="{{$user->cell_number}}" readonly="" placeholder="Phone" >
                        </div>
                    </div>

                    <div class="sc_form_info">
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_email">Caddie ID:</label>
                            <input id="pga_lpga_id" type="text" placeholder="Caddie ID" @if(Auth::user()->caddie()->exists()) value ="{{Auth::user()->caddie->pga_lpga_id}}" @endif  @if(old('pga_lpga_id') != null && Auth::user()->pga_lpga_id == null) value="{{old('pga_lpga_id')}}" @endif style="padding: 18px; {{$errors->has('pga_lpga_id') ? 'border:2px solid red;' : ''}}" name="pga_lpga_id"   >
                            @if($errors->has('pga_lpga_id'))
                                <span class="help-block">{{ $errors->first('pga_lpga_id') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="sc_form_item sc_form_field label_over">
                        <label class="required" for="sc_form_type">Type</label>
                        <select name="pga_lpga" id="sc_form_type" @if($errors->has('pga_lpga')) style="width: 100%; border: 2px solid red" @else style="width: 100%; border: 2px solid #282828" @endif>
                            <option value="">-- Select Type --</option>
                            <option value="1" @if(old('pga_lpga') != null && old('pga_lpga') == '1') selected @endif @if(Auth::user()->caddie()->exists() && old('pga_lpga') == null && Auth::user()->caddie->pga_lpga == 1) selected @endif >Pga Caddie</option>
                            <option value="2" @if(old('pga_lpga') != null && old('pga_lpga') == '2') selected @endif @if(Auth::user()->caddie()->exists() && old('pga_lpga') == null && Auth::user()->caddie->pga_lpga == 2) selected @endif>Lpga Caddie</option>
                        </select> @if($errors->has('pga_lpga'))
                            <span class="help-block">{{ $errors->first('pga_lpga') }}</span> @endif
                    </div>
                    <h3>Addtional Information</h3>
                    <hr>
                    <p style="margin-top: 15px; margin-bottom: 0; color: black; font-size: 18px;">Allergies to Pet(Dogs/Cats) ?</p>
                    <div class="sc_form_item sc_form_field label_over">
                        <label class="required" for="sc_form_type">Alerges to pets (Cats/ Dogs)</label>
                        <select name="pet" id="sc_form_type" @if($errors->has('pet')) style="width: 100%; border: 2px solid red" @else style="width: 100%; border: 2px solid #282828" @endif>
                            <option value="">Alerges to pets (Cats/ Dogs)</option>
                            <option value="1" @if(old('pet') != null && old('pet') == 1) selected @endif @if(Auth::user()->caddie()->exists() && old('pet') == null && Auth::user()->caddie->pet != null && Auth::user()->caddie->pet == '1') selected @endif >Yes</option>
                            <option value="0" @if(old('pet') != null && old('pet') == 0) selected @endif @if(Auth::user()->caddie()->exists() && old('pet') == null && Auth::user()->caddie->pet == '0') selected @endif  >No</option>
                        </select> @if($errors->has('pet'))
                            <span class="help-block">{{ $errors->first('pet') }}</span> @endif
                    </div>
                    <p style="margin-top: 15px; margin-bottom: 0; color: black; font-size: 18px;">Allergies to Smoke ?</p>
                    <div class="sc_form_item sc_form_field label_over">
                        <label class="required" for="sc_form_type">Require Smoke free accoumodotions</label>
                        <select name="smoke" id="sc_form_type" @if($errors->has('smoke')) style="width: 100%; border: 2px solid red" @else style="width: 100%; border: 2px solid #282828" @endif>
                            <option value="">Alerges to pets (Cats/ Dogs)</option>
                            <option value="1" @if(old('smoke') != null && old('smoke') == '1' ) selected @endif  @if(Auth::user()->caddie()->exists() && old('smoke') == null && Auth::user()->caddie->smoke != null && Auth::user()->caddie->smoke == '1') selected @endif>Yes</option>
                            <option value="0" @if(old('smoke') != null && old('smoke') == '0') selected @endif @if(Auth::user()->caddie()->exists() && old('smoke') == null && Auth::user()->caddie->smoke == '0') selected @endif>No</option>
                        </select> @if($errors->has('smoke'))
                            <span class="help-block">{{ $errors->first('smoke') }}</span> @endif
                    </div>

                    <div class="sc_form_item sc_form_button">
                        <input type="submit" value="Update Account">
                    </div>
                    <div class="result sc_infobox"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop


@section('footer-scripts')
<script>
    $("document").ready(function (e) {
        $('html,body').animate({
            scrollTop:$(".sc_section_inner").offset().top
        }, 2000);
        $("#sc_form_email").focus();
    });
</script>

@stop