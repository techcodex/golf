@extends('layouts.dashboard')


@section('header-styles')
    <style>
        a:hover {
            text-decoration: none;
        }
    </style>
@stop

@section('main-section')
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session()->get('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
    @endif
    @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session()->get('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
    @endif
    <form method="POST" action="@if(auth()->user()->type == 1) {{ route('caddie.change.password.store') }} @elseif(auth()->user()->type == 2) {{ route('host.change.password.store') }} @endif">
        @csrf
        <div class="form-group">
            <label for="old_password">Old Password</label>
            <input autofocus type="password" name="old_password" class="form-control @if($errors->has('old_password')) is-invalid @endif" id="old_password">
            @if($errors->has('old_password'))
                <div class="invalid-feedback">
                    {{ $errors->first('old_password') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="new_Password">New Password</label>
            <input type="password" name="new_password" id="new_password" class="form-control @if($errors->has('new_password')) is-invalid @endif">
            @if($errors->has('new_password'))
            <div class="invalid-feedback">
                {{ $errors->first('new_password') }}
            </div>
        @endif
        </div>
        <div class="form-group">
            <label for="new_password_confirm">Confirm New Password</label>
            <input type="password" name="new_password_confirmation" id="new_password_confirm" class="form-control">
        </div>
        <div class="form-group">
            <input type="submit" value="Change Password" class="btn btn-sm btn-success">
        </div>
    </form>
@stop

@section('footer-scripts')

@stop
