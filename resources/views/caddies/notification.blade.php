@extends('layouts.dashboard')


@section('header-styles')
    <style>
        a:hover {
            text-decoration: none;
        }
    </style>
@stop

@section('main-section')
    @if(Auth::user()->notifications->count() != 0)
            <h3>Unread Notifications</h3>
            @if(Auth::user()->unReadNotifications->count() != 0)
                @foreach(Auth::user()->unReadNotifications as $notification)
                    <a href="{{ $notification->data['url'] }}">
                        <div class="alert alert-info"><b style="font-size: 16px;"><i class="{{ $notification->data['icon'] }}"></i>&nbsp; {{ $notification->data['message'] }}</b>  <span class="float-right"><i class="fa fa-clock"></i> {{ $notification->created_at->diffForHumans() }}</span></div>
                    </a>
                @endforeach
            @else
                 <div class="alert alert-success"><b><i class="fa fa-times-circle"></i> &nbsp; You Have No Notification to Read!</b></div>
            @endif
            <h3>Read Notifications</h3>
            @if(Auth::user()->readNotifications->count() != 0)
                @foreach(Auth::user()->readNotifications as $notification)
                    <a href="{{ $notification->data['url'] }}">
                        <div class="alert alert-success"><b style="font-size: 16px;"><i class="{{ $notification->data['icon'] }}"></i>&nbsp; {{ $notification->data['message'] }}</b>  <span class="float-right"><i class="fa fa-clock"></i> {{ $notification->created_at->diffForHumans() }}</span>
                            <div><span><small><i class="fa fa-check-circle"></i> {{ $notification->read_at->diffForHumans() }}</small></span></div>
                        </div>
                    </a>
                @endforeach
            @else
                <div class="alert alert-success"><b><i class="fa fa-times-circle"></i> &nbsp;You Have No Notification to Read!</b></div>
            @endif
    @else
        <div class="alert alert-success"><b><i class="fa fa-times-circle"></i> &nbsp;You Have No Notification to Read!</b></div>
    @endif
    @php
        Auth::user()->unreadNotifications()->update(['read_at' => now()]);
    @endphp
@stop

@section('footer-scripts')

@stop
