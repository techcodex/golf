@extends('layouts.dashboard')


@section('header-styles')
{{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">

@stop


@section('main-section')
<div  class="container">
    <table class="table table-bordered text-center" id="tripsData">
        <thead class="bg-primary" style="color:white;width: 100%;">
        <tr>
            <td>Host Name</td>
            <td>Event Name</td>
            <td>Host Cell No</td>
            <td>Stay Date</td>
            <td>Event City</td>
            <td>Address</td>
            <td>City</td>
            <td>Chat</td>
        </tr>
        </thead>
        <tbody>
        @if(empty($trips))
            <tr>
                <td colspan="6">No Reservation For Any Event</td>
            </tr>
        @else
            <tr>
                <td>{{ $trips->host->first_name}}</td>
                <td>{{ $trips->event->name}}</td>
                <td>{{ $trips->host->cell_number}}</td>
                <td>{{ $trips->stay_start_date->toFormattedDateString() }}</td>
                <td>{{ $trips->event->city }}</td>
                <td>{{ $trips->hostProperty->street_address }}</td>
                <td>{{ $trips->hostProperty->city }}</td>
                <td><form action="{{ route('messenger') }}" method="POST">
                        @csrf
                        <input type="hidden" name="host" value="{{ $trips->host_id }}" />
                        <button type="submit" class="btn btn-sm btn-primary" href="{{ route('messenger') }}" title="Chat">Chat  <i class="fa fa-envelope"></i></button>
                    </form>
                </td>
            </tr>
        @endif

        </tbody>
    </table>
</div>
@stop


@section('footer-scripts')
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script>
    $("#tripsData").dataTable();
</script>
@stop