@extends('layouts.dashboard')


@section('header-styles')
{{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">--}}
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">

@stop


@section('main-section')
<div  class="container">
    <table class="table table-bordered text-center" id="tripsData">
        <thead class="bg-primary" style="color:white;width: 100%;">
        <tr>
            <td>Host Name</td>
            <td>Event Name</td>
            <td>Host Cell No</td>
            <td>Stay Date</td>
            <td>Event City</td>
            <td>Address</td>
            <td>City</td>
        </tr>
        </thead>
        <tbody>
            @foreach($trips as $trip)
                <tr>
                    <td>{{ $trip->host->first_name}}</td>
                    <td>{{ $trip->event->name}}</td>
                    <td>{{ $trip->host->cell_number}}</td>
                    <td>{{ $trip->stay_start_date->toFormattedDateString() }}</td>
                    <td>{{ $trip->event->city }}</td>
                    <td>{{ $trip->hostProperty->street_address }}</td>
                    <td>{{ $trip->hostProperty->city }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop


@section('footer-scripts')
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script>
    $("#tripsData").dataTable();
</script>
@stop