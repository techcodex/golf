<header class="top_panel_wrap top_panel_style_1 scheme_original header_bg_color">
    <div class="top_panel_wrap_inner top_panel_inner_style_1 top_panel_position_above header_bg_image">
        <div class="content_wrap">
            <div class="top_panel_content">
                <div class="top_panel_content_logo">
                    <div class="logo">
                        <a href="index.html">
                            <img src="{{asset('images/logo.png')}}" class="logo_main" alt="">
                            <img src="{{asset('images/logo.png')}}" class="logo_fixed" alt="">
                        </a>
                    </div>
                </div>
                <div class="top_panel_content_menu">
                    <nav class="menu_main_nav_area">
                        <ul id="menu_main" class="menu_main_nav">
                            <li class="menu-item "><a href="{{url('/')}}"><span>Home</span></a>
                            </li>
                            </li>
                            <li class="menu-item"><a href="{{route('aboutus')}}"><span>About Us</span></a>
                            </li>
                            <li class="menu-item "><a href="{{route('contact')}}"><span>Contact</span></a>
                            </li>
                            <li class="menu-item menu-item-has-children"><a href="{{route('event.list')}}"><span>Events</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item"><a href="{{route('event.list')}}">Upcomming Events</a></li>
                                    <li class="menu-item"><a href="{{route('event.calendar')}}">Events Calendar</a></li>
                                    <li class="menu-item"><a href="{{route('event.pga')}}">Pga Events </a></li>
                                    <li class="menu-item"><a href="{{route('event.lpga')}}">LPga Events </a></li>
                                </ul>
                            </li>
                            <li class="menu-item"><a href="{{route('blog')}}"><span>Blog</span></a></li>
                            @if(Auth::check())
                                @if(Auth::user()->type == '2')
                            <li class="menu-item menu-item-has-children"><a href="#"><span>Properties</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item"><a href="{{route('web.host.index')}}"><span>My properties</span></a></li>
                                    <li class="menu-item"><a href="{{route('host.create_property')}}"><span>Create Property</span></a></li>
                                    <li class="menu-item"><a href="{{route('availability.dashboard.create')}}"><span>Availabilty</span></a></li>
                                </ul>
                            </li>
                                    @endif
                            @endif
                            @if(Auth::check())
                                <li class="menu-item menu-item-has-children"><a href="#"><span>
                                             My Account
                                            <ul class="sub-menu">
                                                <li class="menu-item"><a href="{{route('web.user.edit',['id'=>Auth::user()->id])}}"><span>Settings</span></a></li>
                                                @if(Auth::user()->type == '1')
                                                <li class="menu-item"><a href="{{route('caddie.account')}}"><span>Account Settings</span></a></li>
                                                <li class="menu-item"><a href="{{route('caddie.dashboard')}}"><span>My Dashbaord</span></a></li>
                                                @endif
                                                @if(Auth::user()->type == '2')
                                                    <li class="menu-item"><a href="{{route('host.dashboard')}}"><span>My Dashboard</span></a></li>
                                                    <li class="menu-item"><a href="{{route('adoptCaddie')}}"><span>Adopt Caddie</span></a></li>
                                                @endif
                                                <li class="menu-item"><a href="{{ route('logout') }} " onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><span>
                                                            Logout
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                                        </span></a></li>
                                            </ul>
                            @else
                                <li class="menu-item menu-item-has-children"><a href="#"><span>
                                             Account
                                            <ul class="sub-menu">
                                                <li class="menu-item"><a href="{{route('login')}}"><span>Login</span></a></li>
                                                <li class="menu-item"><a href="{{route('register')}}"><span>Registration</span></a></li>
                                                <li class="menu-item"><a href="{{ route('adoptCaddie') }}"><span>Adopt Caddie</span></a></li>
                                            </ul>

                                    </span></a></li>
                            @endif
                        </ul>
                    </nav>
                    <div class="cL"></div>
                </div>
                <div class="cL"></div>
                <div class="breadcrumbs_present">
                    <h1 class="page_title">{{$page_heading}}</h1>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="header_mobile">
    <div class="content_wrap">
        <div class="menu_button icon-menu"></div>
        <div class="logo">
            <a href="index.html"><img src="images/logo.png" class="logo_main" alt=""></a>
        </div>
    </div>
    <div class="side_wrap">
        <div class="close">Close</div>
        <div class="panel_top">
            <nav class="menu_main_nav_area">
                <ul id="menu_main" class="menu_main_nav">
                    <li class="menu-item "><a href="{{url('/')}}"><span>Home</span></a>
                    </li>
                    <li class="menu-item "><a href="{{route('aboutus')}}"><span>About Us</span></a>
                    </li>
                    <li class="menu-item "><a href="{{route('contact')}}"><span>Contact</span></a>
                    </li>
                    <li class="menu-item menu-item-has-children"><a href="{{route('event.list')}}"><span>Events</span></a>
                        <ul class="sub-menu">
                            <li class="menu-item"><a href="{{route('event.list')}}">Upcomming Events</a></li>
                            <li class="menu-item"><a href="{{route('event.calendar')}}">Events Calendar</a></li>
                            <li class="menu-item"><a href="{{route('event.pga')}}">Ppga Events </a></li>
                            <li class="menu-item"><a href="{{route('event.lpga')}}">Lpga Events </a></li>
                        </ul>
                    </li>
                    <li class="menu-item"><a href="{{route('blog')}}"><span>Blog</span></a></li>
                    @if(Auth::check())
                        @if(Auth::user()->type == '2')
                            <li class="menu-item menu-item-has-children"><a href="#"><span>Properties</span></a>
                                <ul class="sub-menu">
                                    <li class="menu-item"><a
                                                href="{{route('web.host.index')}}"><span>My Properties</span></a>
                                    </li>
                                    <li class="menu-item"><a
                                                href="{{route('host.create_property')}}"><span>Create Property</span></a>
                                    </li>
                                    <li class="menu-item"><a
                                                href="{{route('availability.dashboard.create')}}"><span>Availabilty</span></a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    @endif

                    @if(Auth::check())
                        <li class="menu-item menu-item-has-children"><a href="#"><span>
                                             My Account
                                            <ul class="sub-menu">
                                                <li class="menu-item"><a href="{{route('web.user.edit',['id'=>Auth::user()->id])}}"><span>Settings</span></a></li>
                                                @if(Auth::user()->type == '1')
                                                    <li class="menu-item"><a href="{{route('caddie.account')}}"><span>Account Settings</span></a></li>
                                                    <li class="menu-item"><a href="{{route('caddie.dashboard')}}"><span>My Dashboard</span></a></li>
                                                @endif
                                                @if(Auth::user()->type == '2')
                                                    <li class="menu-item"><a href="{{route('host.dashboard')}}"><span>My Dashboard</span></a></li>
                                                    <li class="menu-item"><a href="{{route('adoptCaddie')}}"><span>Adopt Caddie</span></a></li>
                                                @endif
                                                <li class="menu-item"><a href="{{ route('logout') }} " onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><span>
                                                            Logout
                                                        <form id="logout-form" action="{{ route('logout') }}"
                                                              method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                                        </span></a></li>
                                            </ul>
                    @else
                        <li class="menu-item menu-item-has-children"><a href="#"><span>
                                             Caddie
                                            <ul class="sub-menu">
                                                <li class="menu-item"><a
                                                            href="{{route('login')}}"><span>Login</span></a></li>
                                                <li class="menu-item"><a
                                                            href="{{route('register')}}"><span>Registration</span></a></li>
                                                <li class="menu-item"><a href="{{ route('adoptCaddie') }}"><span>Adopt Caddie</span></a></li>
                                            </ul>

                                    </span></a></li>
                        <li class="menu-item menu-item-has-children"><a href="#"><span>
                                             Host
                                            <ul class="sub-menu">
                                                <li class="menu-item"><a
                                                            href="{{route('login')}}"><span>Login</span></a></li>
                                                <li class="menu-item"><a
                                                            href="{{route('register')}}"><span>Registration</span></a></li>
                                            </ul>

                                    </span></a></li>
                    @endif
                </ul>
            </nav>
        </div>
        <div class="panel_middle">
            <div class="contact_field contact_phone">
                <span class="contact_icon icon-phone"></span>
                <span class="contact_label contact_phone">info@casadecaddie.com</span>
                <span class="contact_email"></span>
            </div>
        </div>
    </div>
    <div class="mask"></div>
</div>
