<footer class="footer_wrap widget_area scheme_original">
    <div class="footer_wrap_inner widget_area_inner">
        <div class="content_wrap">
            <div class="columns_wrap">
                <aside class="column-1_4 widget widget_socials">
                    <h5 class="widget_title">ABOUT US</h5>
                    <div class="widget_inner">
                        <div class="logo_descr"><a href="https://casadecaddie.com">Casa de Caddie</a> &nbsp; © Copyright 2018-2019 © Casa De Caddie, Inc.
                            <br/> All Rights Reserved.
                            <br/> <a href="{{ route('terms.condition') }}">Terms and Conditions</a> <br>and <a href="http://www.casadecaddie.com">Made By Casadecaddie</a></div>
                        <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                            <div class="sc_socials_item"><a href="#" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div>
                        </div>
                    </div>
                </aside><aside class="column-1_4 widget widget_recent_entries">
                    <h5 class="widget_title">Blog</h5>
                    <ul>
                        @foreach($footerBlogs as $blog)
                            <li><a href="{{route('blog.show', ['slug' => $blog->slug])}}">{{ $blog->title }}</a></li>
                        @endforeach
                    </ul>
                </aside><aside class="column-1_4 widget widget_text">
                    <h5 class="widget_title">CONTACT US</h5>
                    <div class="textwidget">
                        <br><i>Email:</i>
                        <br> <a href="#"><i>info@casadecaddie.com</i></a></div>
                </aside><aside class="column-1_4 widget null-instagram-feed">
                    <h5 class="widget_title">Instagram</h5>
                    <ul class="instagram-pics instagram-size-thumbnail">
                        <li>
                            <a href="#"><img src="{{asset('images/inst_1.jpg')}}" alt="Instagram Image" title="Instagram Image" class="" /></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{asset('images/inst_2.jpg')}}" alt="Instagram Image" title="Instagram Image" class="" /></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{asset('images/inst_3.jpg')}}" alt="Instagram Image" title="Instagram Image" class="" /></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{asset('images/inst_4.jpg')}}" alt="Instagram Image" title="Instagram Image" class="" /></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{asset('images/inst_5.jpg')}}" alt="Instagram Image" title="Instagram Image" class="" /></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{asset('images/inst_6.jpg')}}" alt="Instagram Image" title="Instagram Image" class="" /></a>
                        </li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
</footer>
