<ul class="list-group">
        <li class="list-group-item" style="background: #eee;"></li>
        <li class="list-group-item" style="background: #eee;">
            <div class="row">
                <div class="col-md-6 b-r" style="text-align: center;">
                    Member Since:
                <h4 style="font-weight: 900;font-size: 18px;">{{ Auth::user()->created_at->toFormattedDateString() }}</h4>
                </div>
                <div class="col-md-6" style="text-align: center">
                    Last login:
                    <h4 style="font-weight: 900; font-size: 18px;">{{ Auth::user()->logged_at->diffForHumans() }}</h4>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <a href="{{route('host.listing')}}"><i class="fa fa-sm fa-plus"></i> <span class="list-item">List My Room</span></a>
        </li>
        <li class="list-group-item">
            <a href="@if(Auth::user()->type == '1') {{ route('caddie.notification') }} @else {{ route('host.notification') }} @endif"><i class="fa fa-sm fa-bell"></i> <span class="list-item">Notifications @if(Auth::user()->unReadNotifications->count() > 0) <span class="badge badge-secondary">{{Auth::user()->unReadNotifications->count()}}</span> @endif</span></a>
        </li>
        <li class="list-group-item">
            <a href="{{ route('messenger') }}"><i class="fa fa-sm fa-envelope"></i> <span class="list-item">Messages</span></a>
        </li>
        <li class="list-group-item">
            <a href="{{route('web.user.edit',['id'=>Auth::user()->id])}}"><i class="fa fa-sm fa-user"></i> <span class="list-item">Profile</span></a>
        </li>
        @if(Auth::user()->type == '1')
        <li class="list-group-item">
            <a href="{{route('caddie.listing')}}"><i class="fa fa-sm fa-list-ul"></i> <span class="list-item">My Listing</span></a>
        </li>
        @else
        <li class="list-group-item">
            <a href="{{route('host.dashboard.create')}}"><i class="fa fa-sm fa-list-ul"></i> <span class="list-item">Create Property</span></a>
        </li>
        @endif
        @if(Auth::user()->type == '1')
        <li class="list-group-item">
            <a href="{{ route('caddie.trips')  }}"><i class="fa fa-sm fa-paper-plane"></i> <span class="list-item">My Trips</span></a>
        </li>
        @else
        <li class="list-group-item">
            <a href="{{route('host.dashboard.index')}}"><i class="fa fa-sm fa-paper-plane"></i> <span class="list-item">My Properties</span></a>
        </li>
        @endif
        @if(Auth::user()->type == '1')
        <li class="list-group-item">
            <a href="{{route('caddie.reservation')}}"><i class="fa fa-sm fa-file-alt"></i> <span class="list-item">My Reservation</span></a>
        </li>
        @else
        <li class="list-group-item">
            <a href="{{route('availability.dashboard.create')}}"><i class="fa fa-sm fa-file-alt"></i> <span class="list-item">Create Avliability</span></a>
        </li>
        @endif

        @if(Auth::user()->type == '2')
        <li class="list-group-item">
            <a href="{{route('avaialability.dashboard.index')}}"><i class="fa fa-sm fa-file-alt"></i> <span class="list-item">My Avliabilities</span></a>
        </li>
        @endif

        <li class="list-group-item">
            <a href="@if(Auth::user()->type == '1') {{ route('caddie.change.password') }} @else {{ route('host.change.password') }} @endif"><i class="fa fa-sm fa-key"></i> <span class="list-item">Change Password</span></a>
        </li>
        <li class="list-group-item">
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                <i class="fa fa-sm fa-sign-out-alt"></i> <span class="list-item">
                    Logout
                </span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
