<section class="slider_wrap slider_fullwide slider_engine_revo slider_alias_rsl1">

    <div id="mainslider_1" class="rev_slider_wrapper fullwidthbanner-container" >
        <!-- START REVOLUTION SLIDER 5.2.5.1 fullwidth mode -->
        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner d_none" data-version="5.2.5.1">
            <ul>
                <!-- SLIDE  1-->


                <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="500" data-thumb="images/slider-1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/slider-1.jpg" alt="" title="slider-1" width="1920" height="955" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption black tp-resizeme xs_text"
                         id="slide-1-layer-2"
                         data-x="center"
                         data-hoffset=""
                         data-y="398"
                         data-width="['893']"
                         data-height="['121']"
                         data-transform_idle="o:1;"
                         data-transform_in="y:50px;opacity:0;s:1000;e:Power3.easeOut;"
                         data-transform_out="opacity:0;s:300;"
                         data-start="2060"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on" >Casa De Caddie
                    </div>
                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption Button rev-btn tp-resizeme btn_left m_scr-l"
                         id="slide-1-layer-5"
                         data-x="center"
                         data-hoffset="-115"
                         data-y="619"
                         data-width="['auto']"
                         data-height="['auto']"
                         data-visibility="['on','on','on','off']"
                         data-transform_idle="o:1;"
                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;"
                         data-style_hover="c:rgba(40, 40, 40, 1.00);bg:rgba(255, 255, 255, 1.00);"
                         data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:600;e:Power2.easeOut;"
                         data-transform_out="opacity:0;s:300;"
                         data-mask_in="x:0px;y:0px;"
                         data-start="4000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"><a href="{{route('login')}}"> Join Us
                    </div>
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption Button rev-btn tp-resizeme btn_right m_scr-r"
                         id="slide-1-layer-7"
                         data-x="center"
                         data-hoffset="116"
                         data-y="619"
                         data-width="['auto']"
                         data-height="['auto']"
                         data-visibility="['on','on','on','off']"
                         data-transform_idle="o:1;"
                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;"
                         data-style_hover="c:rgba(40, 40, 40, 1.00);bg:rgba(255, 255, 255, 1.00);"
                         data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:600;e:Power2.easeOut;"
                         data-transform_out="opacity:0;s:300;"
                         data-mask_in="x:0px;y:0px;"
                         data-start="5000"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"><a href="{{route('event.list')}}"> MAKE RESERVATION</a>
                    </div>
                </li>


            </ul>
            <div class="tp-bannertimer tp-bottom h5 opacity_bg"></div>
        </div>
    </div>
</section>