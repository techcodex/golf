@extends('layouts.app')

@section('header-styles')

<style>
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    .btn {
        border: 2px solid gray;
        color: gray;
        background-color: white;
        padding: 8px 20px;
        border-radius: 8px;
        font-size: 20px;
        font-weight: bold;
        margin-top: 20px;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    #blah {
        padding: 5px;
        border: 1px solid #eee;
        border-radius: 5px;
    }

    #sc_form_password {
        padding: 18px;
    }
</style>
@stop
@section('main-section')

<div class="sc_section margin_top_large margin_bottom_xxxhuge aligncenter w70_per" data-animation="animated fadeInUp normal">
    <div class="sc_section_inner">
        <h2 class="sc_title margin_top_null margin_bottom_xmedium aligncenter">Registration Form</h2>
        <div class="sc_form_wrap">
            <div id="sc_form_1" class="sc_form sc_form_style_form_1 cf1LeftText ">
                <form id="sc_form_1_1" data-formtype="form_1" enctype='multipart/form-data' method="post" action="{{ url('/register') }}">
                    @csrf
                    <div class="sc_form_info">
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_firstname">First Name:</label>
                            <input id="sc_form_firstname" value="{{ old('firstName') }}" @if($errors->has('firstName')) style="border:
                            2px solid red" @endif type="text" name="firstName" placeholder="First Name"> @if($errors->has('firstName'))
                            <span class="help-block">{{ $errors->first('firstName') }}</span> @endif
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_lastname">Last Name:</label>
                            <input id="sc_form_lastname" value="{{ old('lastName') }}" type="text" name="lastName" placeholder="Last Name" @if($errors->has('lastName'))
                            style="border: 2px solid red" @endif> @if($errors->has('lastName'))
                            <span class="help-block">{{ $errors->first('lastName') }}</span> @endif
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_email">E-mail</label>
                            <input id="sc_form_email" type="text" value="{{ old('email') }}" name="email" placeholder="E-mail *" @if($errors->has('email'))
                            style="border: 2px solid red" @endif> @if($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span> @endif
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_password">Password:</label>
                            <input id="sc_form_password" type="password" name="password" placeholder="Password" @if($errors->has('password'))
                            style="border: 2px solid red" @endif> @if($errors->has('password'))
                            <span class="help-block">{{ $errors->first('password') }}</span> @endif
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_phone">Phone:</label>
                            <input id="sc_form_phone" value="{{ old('phone') }}" type="text" name="phone" placeholder="Phone (000-000-0000)" @if($errors->has('phone'))
                            style="border: 2px solid red" @endif> @if($errors->has('phone'))
                            <span class="help-block">{{ $errors->first('phone') }}</span> @endif
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_type">Type</label>
                            <select name="type" id="sc_form_type" @if($errors->has('type')) style="width: 100%; border: 2px solid red" @else style="width: 100%; border: 2px solid #282828" @endif>
                                     <option value="">-- Select Type --</option>
                                     <option value="1" @if(old('type') != null && old('type') == '1') selected @endif>Caddie</option>
                                     <option value="2" @if(old('type') != null && old('type') == '2') selected @endif>Host</option>
                                 </select> @if($errors->has('type'))
                            <span class="help-block">{{ $errors->first('type') }}</span> @endif
                        </div>
                        <div class="sc_form_item sc_form_field label_over">
                            <label class="required" for="sc_form_img">Profile Picture:</label>
                            <div class="upload-btn-wrapper">
                                <button class="btn" @if($errors->has('myImage')) style="border: 1px solid red" @endif>Upload a Profile Picture</button>
                                <input type="file" name="myImage" id="sc_form_image" />
                            </div>
                            <img id="blah" src="{{ asset('images/placeholder.png') }}" alt="your image" height="100" width="100" style="margin-left: 5%; margin-top: 5px;"
                            /> @if($errors->has('myImage'))
                            <span class="help-block" style="top: 20; left: -130; position: relative">{{ $errors->first('myImage') }}</span>                            @endif
                        </div>
                    </div>
                    <div class="sc_form_item sc_form_button">
                        <button type="submit">Register</button>
                    </div>
                    <div class="result sc_infobox"></div>
                </form>
            </div>
        </div>
    </div>
</div>









@stop
@section('footer-scripts')
<script>
    $("document").ready(function (e) {
        $('html,body').animate({
            scrollTop:$(".sc_section_inner").offset().top
        }, 2000);
        $("#sc_form_email").focus();
        $(".page_content_wrap").addClass('page_paddings_no');
    });

function readURL(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]);
    }
}

$("#sc_form_image").change(function() {
readURL(this);
});

</script>









@stop
