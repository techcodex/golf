@extends('layouts.app')


@section('header-styles')


@stop


@section('main-section')

    <div class="content_container">

        <div class="sc_section margin_top_large margin_bottom_xxxhuge aligncenter w70_per" data-animation="animated fadeInUp normal">
            <div class="sc_section_inner">
                <h2 class="sc_title margin_top_null margin_bottom_xmedium aligncenter">{{$page_heading}} </h2>
                <div  class="sc_form_wrap">
                    <div id="sc_form_1" class="sc_form sc_form_style_form_1 cf1LeftText ">
                        <form id="sc_form_1_1" data-formtype="form_1" method="post" action="{{route('login')}}">
                            {{csrf_field()}}
                            <div class="sc_form_info">
                                <div class="sc_form_item sc_form_field label_over">
                                    <label class="required" for="sc_form_username">Email:</label>
                                    <input id="sc_form_username" type="text" name="email" placeholder="Email *" style="{{$errors->has('email') ? 'border:2px solid red' : ''}}">
                                    @if($errors->has('email'))<span class="help-block">{{ $errors->first('email') }}</span> @endif
                                </div>
                                <div class="sc_form_item sc_form_field label_over">
                                    <label class="required" for="sc_form_email">Password:</label>
                                    <input id="sc_form_email" type="password" name="password" placeholder="Password *" style="padding: 18px; {{$errors->has('password') ? 'border:2px solid red' : ''}}">
                                    @if($errors->has('password'))<span class="help-block">{{ $errors->first('password') }}</span> @endif
                                </div>
                            </div>
                            <div class="sc_form_item sc_form_button">
                                <button>Login</button>
                            </div>
                            <div class="result sc_infobox"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop


@section('footer-scripts')
<script>
    $("document").ready(function (e) {
        $('html,body').animate({
            scrollTop:$(".sc_section_inner").offset().top
        }, 2000);
        $("#sc_form_email").focus();
        $(".page_content_wrap").addClass('page_paddings_no');
    });
</script>

@stop