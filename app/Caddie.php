<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caddie extends Model
{

    protected $fillable = [
        'user_id','caddie_name','phone','email','pga_lpga_id','pga_lpga','pet','smoke','created_by','updated_by'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function agreement()
    {
        return $this->hasOne('App\Agreement');
    }
}
