<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AvaiableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'property' => 'required',
            'event' => 'required',
            'startDate' => 'required|date:before:endDate',
            'endDate' => 'required|date|after:startDate',
        ];
    }
}
