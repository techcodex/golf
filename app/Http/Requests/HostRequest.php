<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullName' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required|regex:/^\d{3,4}\-\d{3}\-\d{4}$/',
            'streetAddress' => 'required',
            'city' => 'required',
            'country' => 'required',
            'state' => 'required',
            'pet' => 'required|numeric',
            'smoke' => 'required',
            'place_type_id'=>'required|numeric',
            'space_id'=>'required|numeric',
            'accommodates'=>'required|numeric|min:1|max:10',
            'property_images'=>'required',
            'property_images.*'=>'image|mimes:jpeg,jpg,gif,png',
        ];
    }
    public function messages()
    {
        return [
            'property_images.required'=>'Property are Images required',
        ];
    }
}
