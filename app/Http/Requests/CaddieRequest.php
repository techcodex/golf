<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CaddieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pga_lpga_id'=>'required|numeric',
            'pga_lpga'=>'required|numeric',
            'pet'=>'required|numeric',
            'smoke'=>'required|numeric',
        ];
    }
}
