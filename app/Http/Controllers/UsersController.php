<?php

namespace App\Http\Controllers;

use App\Notifications\CaddieAccountActivated;
use Auth;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Hash;
use App\Notifications\UserRegistered;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index')->with(['page_heading' => 'All Users List', 'page_title' => 'User List', 'active' => 'show-user']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'page_heading' => 'Add User Form',
            'page_title' => 'Add User',
            'active' => 'add-user'
        ];
        return view('admin.users.add_user')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'cell_number' => 'required|regex:/^\d{3,4}\-\d{3}\-\d{4}$/',
            'password' => 'required|min:3',
            'type' => 'required|numeric',
        ]);
        if($request->has('myImage')) {
            $profilePic = $request->myImage;

            $profilePicName = time() . $profilePic->getClientOriginalName();
            if ($profilePic->move('images/profile', $profilePicName)) {
                $profilePicName = 'images/profile/' . $profilePicName;
            } else {
                $profilePicName = 'images/profile/placeholder.png';
            }
        } else {
            $profilePicName = 'images/profile/placeholder.png';
        }

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'cell_number' => $request->cell_number,
            'password' => Hash::make($request->password),
            'profile_picture' => $profilePicName,
            'type' => $request->type,
            'home_number'=>'',
            'created_by' => 'admin',
            'updated_by' => 'admin',
            'email_verified_at' => $request->type == 3 ? Carbon::now() : null,
            'logged_at' => Carbon::now()
        ]);

        if ($request->type != 3)
            $user->sendEmailVerificationNotification();

        Session::flash('success', 'User Created Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->id != $id && Auth::user()->type != 3) {
            Session::flash('error',"Opss Something went Wrong");
            return back();
        }

        $user = User::find($id);
        if(Auth::user()->type == '3') {
            $data = [
                'page_title'=>'Edit User',
                'page_heading'=>'Edit User',
                'user'=>$user,
                'active'=>'show-user',
            ];
            return view('admin.users.edit')->with($data);
        } else {
            $data = [
                'page_heading' => 'Edit Profile',
                'page_title' => 'Edit Profile',
                'user' => $user,
            ];
            if(Auth::user()->type == '1') {
                $data['dashboard'] = "Caddie Dashboard";
            } else {
                $data['dashboard'] = "Host Dashboard";
            }
            return view('edit_user')->with($data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->id != $id && Auth::user()->type != 3) {
            Session::flash('error',"Opss Something went Wrong");
            return back();
        }
        $request->validate([
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'email' => 'required|email',
            'cell_number' => 'required|regex:/^\d{3,4}\-\d{3}\-\d{4}$/',
            'home_number' => 'required|regex:/^\d{3,4}\-\d{3}\-\d{4}$/',
            'myImage' => 'sometimes|mimes:jpg,jpeg,png,bmp',
        ]);
        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->cell_number = $request->cell_number;
        $user->home_number = $request->home_number;
        if ($request->has('myImage')) {
            $profile_image = $request->myImage;
            $imageName = time() . $profile_image->getClientOriginalName();
            $profile_image->move('images/profile', $imageName);
            $user->profile_picture = "images/profile/" . $imageName;
        }
        $user->save();
        Session::flash('success', 'User Edit Successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * get the data of user to use in datatable
     *
     * @return Yajra\DataTables\DataTables;
     */
    public function get_users()
    {
        return Datatables::of(User::query())

            ->setRowId(function ($user) {
                return $user->id;
            })
            ->rawColumns(['status'])
            ->addColumn('status', function (User $user) {
                return $user->status == 1 ? "<a href='#' class='btn btn-sm m-btn--pill m-btn--air btn-danger deactive' data-id='$user->id'>Deactive</a>" : "<a href='#' data-id='$user->id' class='btn btn-sm m-btn--pill m-btn--air btn-primary active'>Active</a>";
            })
            ->rawColumns(['status'])
            ->addColumn('action','action')
            ->editColumn('created_at', function (User $user) {
                return $user->created_at->diffForHumans();
            })
            ->editColumn('updated_at', function (User $user) {
                return $user->updated_at->diffForHumans();
            })
            ->toJson();
    }
    /**
     * Deactive the user .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deactive($id)
    {
        $response = [];
        if (Auth::id() == $id) {
            $response['error'] = true;
            $response['msg'] = "Cannot Deactive Admin";
            return $response;
        }
        $user = User::find($id);
        $user->status = 0;
        $user->save();
        $response = [];
        $response['success'] = true;
        return $response;
    }
    /**
     * Active the user .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
//        dd($id);
        $response = [];
        $user = User::find($id);
        if ($user != "") {
            $user->status = 1;
            $user->save();
            $response['success'] = true;
            return $response;
        }
//        $user->notify(new CaddieAccountActivated($user));

        $response['error'] = true;
        $response['msg'] = "User Not Found";

    }

    /**
     * Return View to Change Password
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword()
    {
        if (Auth::user()->type == 2)
            return view('host.change_password')
            ->with('page_title', 'Change Password ')
            ->with('dashboard', 'Host Dashboard')
            ->with('page_heading', 'Change Password');
        else if (Auth::user()->type == 1)
            return view('caddies.change_password')
            ->with('page_title', 'Change Password ')
            ->with('dashboard', 'Caddie Dashboard')
            ->with('page_heading', 'Change Password');;
    }

    /**
     * Validate And Change the Password
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePasswordStore(Request $request)
    {
        // dd($request->all());
        $user = Auth::user();

        /*
         * Validate all input fields
         */
        $rules = [
            'old_password'=>'required|min:8',
            'new_password'=>'confirmed|min:8|different:old_password|regex:/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/'
        ];
        $message = [
            'regex'=>'The Password Must Contain at least one letter, one number and one special character'
        ];
        $this->validate($request,$rules,$message);
//        $request->validate([
//            'old_password' => '',
//            'new_password' => '',
//        ]);

        if (Hash::check($request->old_password, $user->password)) {
            $user->fill([
                'password' => Hash::make($request->new_password)
            ])->save();

            $request->session()->flash('success', 'Password changed Successfully!');
            return redirect()->back();

        } else {
            $request->session()->flash('error', 'Old Password does not Match!');
            return redirect()->back();
        }
    }
    /**
     * returning view for show all caddies
     *
     * @return Yajra\DataTables\DataTables;
     */




}
