<?php

namespace App\Http\Controllers;

use Yajra\DataTables\DataTables;
use App\Country;
use Illuminate\Http\Request;

use App\Event;
use Session;
//use MaddHatter\LaravelFullcalendar\Calendar;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;


class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     * Calling from admin side
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.events.index')
            ->with('page_heading', 'All Events')
            ->with('page_title', 'Events')
            ->with('active', 'show-event');

    }
    /*
     * Displaying list of events
     * caaling from web site
     */
    public function eventsList() {
        $now = date("Y-m-d");
        $events = Event::where('end_date','>',$now)->paginate(3);
        $data = [
            'page_title'=>'Events List',
            'page_heading'=>'Events List',
            'events'=>$events,

        ];
        return view('events.event_list')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data= [
            'page_heading'=>'Add Event Page',
            'page_title'=>'Add Event',
            'countries'=>Country::orderBy('name','asc')->get(),
            'active'=>'add-event'
        ];
        return view('admin.events.add_event')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $request->validate([
            'name'=>'required',
            'type'=>'required|numeric',
            'source'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'country_id'=>'required|numeric',
            'state_id'=>'required|numeric',
            'country_club_id'=>'required',
            'city'=>'required',
            'event_image'=>'required|mimes:jpg,jpeg,png,bmp',
        ]);
        $event_image = $request->event_image;
        $eventImageName = time().$event_image->getClientOriginalName();

        if($event_image->move('images/events',$eventImageName)) {
            $eventImageName = "images/events/".$eventImageName;
        } else {
            $eventImageName = "images/events/demo.png";
        }
        Event::create([
            'name'=>$request->name,
            'type'=>$request->type,
            'source'=>$request->source,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'country_id'=>$request->country_id,
            'state_id'=>$request->state_id,
            'country_club_name'=>$request->country_club_id,
            'created_by'=>'Admin',
            'updated_by'=>'Admin',
            'city'=>$request->city,
            'event_image'=>$eventImageName,
            'slug'=>str_slug($request->name),
        ]);
        Session::flash('success',"Event Created Successfully");
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        $data = [
            'page_title'=>"Event",
            'page_heading'=>"Events",
            'event'=>$event,
        ];
        return view('events.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();
        Session::flash('success', 'Event Deleted Successfully!');
        return back();
    }

    public function get_events()
    {
        return Datatables::of(Event::query())

            ->setRowId(function ($event) {
                return $event->id;
            })
            ->addColumn('action','action')
            ->editColumn('type', function(Event $event){
                return $event->type == 1 ? 'PGA' : 'LPGA';
            })
            ->editColumn('start_date', function(Event $event){
                return $event->start_date->toFormattedDateString();
            })
            ->editColumn('end_date', function(Event $event){
                return $event->end_date->toFormattedDateString();
            })
            ->editColumn('created_at', function (Event $user) {
                return $user->created_at->diffForHumans();
            })
            ->editColumn('country_id',function (Event $event) {
                return $event->country->name;
            })
            ->editColumn('state_id',function (Event $event) {
                return $event->state->name;
            })
            ->editColumn('updated_at', function (Event $user) {
                return $user->updated_at->diffForHumans();
            })
            ->addColumn('participants',function (Event $event) {
                $stays = $event->stays;
                return $stays->count();
            })
            ->toJson();
    }
    /*
     * Show pga Events in admin panel
     * Get events by ajax request
     */
    public function pgaEvents() {
        $events = Event::where('type','1')->get();
        if($events->count() == 0) {
            $array = [
                "sEcho"=>0,
                "iTotalRecords"=>0,
                "iTotalDisplayRecords"=>0,
                "aaData"=>[],
            ];
            return json_encode($array);
        }
        return Datatables::of($events)
            ->setRowId(function ($event) {
                return $event->id;
            })
            ->addColumn('action','action')
            ->editColumn('type', function(Event $event){
                return "PGA";
            })
            ->editColumn('start_date', function(Event $event){
                return $event->start_date->toFormattedDateString();
            })
            ->editColumn('end_date', function(Event $event){
                return $event->end_date->toFormattedDateString();
            })
            ->editColumn('created_at', function (Event $user) {
                return $user->created_at->diffForHumans();
            })
            ->editColumn('updated_at', function (Event $user) {
                return $user->updated_at->diffForHumans();
            })
            ->toJson();
    }

    /*
     * Show Lpga Events in admin panel
     * get Events by ajax request
     */

    public function lpgaEvents() {

        $events = Event::where('type','2')->get();
        if($events->count() == 0) {
            $array = [
                "sEcho"=>0,
                "iTotalRecords"=>0,
                "iTotalDisplayRecords"=>0,
                "aaData"=>[],
            ];
            return json_encode($array);
        }
        return Datatables::of($events)
            ->setRowId(function ($event) {
                return $event->id;
            })
            ->addColumn('action','action')
            ->editColumn('type', function(Event $event){
                return "LPGA";
            })
            ->editColumn('start_date', function(Event $event){
                return $event->start_date->toFormattedDateString();
            })
            ->editColumn('end_date', function(Event $event){
                return $event->end_date->toFormattedDateString();
            })
            ->editColumn('created_at', function (Event $user) {
                return $user->created_at->diffForHumans();
            })
            ->editColumn('updated_at', function (Event $user) {
                return $user->updated_at->diffForHumans();
            })
            ->toJson();
    }
    /*
     * Dsiplay view to show pag events
     */
    public function pga() {
        return view('admin.events.pga')
            ->with('page_heading', 'Pga Events')
            ->with('page_title', 'pga Events')
            ->with('active', 'pga-event');
    }

    /*
     * Dsiplay view to show pag events
     */

    public function lpga() {
        return view('admin.events.lpga')
            ->with('page_heading', 'Lpga Events')
            ->with('page_title', 'Lgpa Events')
            ->with('active', 'lpga-event');
    }
}
