<?php

namespace App\Http\Controllers;

use App\Country;
use App\State;
use Illuminate\Http\Request;

class Locations extends Controller
{
    public function get_states(Request $request) {
//        dd("ss");
//        dd($request->all());
        $id = $request->input('id');
        $response = [];
        $obj_country = new Country();
        $obj_country->id = $id;
        $states = $obj_country->states;

        $response['success'] = true;
        $response['states'] = $states;
        return $response;
    }
}
