<?php

namespace App\Http\Controllers;
use App\Http\Middleware\Host;
use App\User;
use Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class HostController extends Controller
{
    public function dashboard()
    {
        $data = [
            'page_heading'=>'Dashboard',
            'page_title'=> Auth::user()->first_name . ' Dashboard ',
            'dashboard'=>'Host Dashboard',
        ];

        return view('host.dashboard')->with($data);
    }

    public function notification()
    {
        $data = [
            'page_heading'=>'Notifications',
            'page_title'=> Auth::user()->first_name . ' Notifications ',
            'dashboard'=>'Host Dashboard',
        ];

        return view('host.notification')->with($data);
    }
    /*
     Returning view for display hosts
     */
    public function index() {
        $data = [
            'page_title'=>'Hosts',
            'page_heading'=>'All Host Data',
            'active'=>'host',
        ];
        return view('admin.hosts.index')->with($data);
    }
    public function host_list() {
        $hosts = User::where('type','2')->get();
        return Datatables::of($hosts)

            ->setRowId(function ($user) {
                return $user->id;
            })
            ->rawColumns(['status'])
            ->addColumn('status', function (User $user) {
                return $user->status == 1 ? "<a href='#' class='btn btn-sm m-btn--pill m-btn--air btn-danger deactive' data-id='$user->id'>Deactive</a>" : "<a href='#' data-id='$user->id' class='btn btn-sm m-btn--pill m-btn--air btn-primary active'>Active</a>";
            })
            ->rawColumns(['status'])
            ->addColumn('action','action')
            ->editColumn('created_at', function (User $user) {
                return $user->created_at->diffForHumans();
            })
            ->editColumn('updated_at', function (User $user) {
                return $user->updated_at->diffForHumans();
            })
            ->toJson();
    }
}
