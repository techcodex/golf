<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if(auth()->user()->type == 1)
            return redirect()->route('caddie.dashboard');
        else if(auth()->user()->type == 2)
        {
            return redirect()->route('host.dashboard');
        } 
        else 
             return redirect()->route('admin.dashboard');

        return view('home')->with($data);
    }
}
