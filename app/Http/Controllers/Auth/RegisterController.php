<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use App\Notifications\UserRegistered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstName' => ['required', 'string', 'max:255'],
            'lastName' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
            'phone' => ['required','regex:/^\d{3,4}\-\d{3}\-\d{4}$/'],
            'type' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
//        dd($data);
        if(isset($data['myImage'])) {
            $profilePic = $data['myImage'];
            $profilePicName = time() . $profilePic->getClientOriginalName();
            if ($profilePic->move('images/profile', $profilePicName)) {
                $profilePicName = 'images/profile/' . $profilePicName;
            } else {
                $profilePicName = 'images/profile/placeholder.png';
            }
        } else {
            $profilePicName = "images/placeholder.png";
        }



        $user = User::create([
            'first_name' => $data['firstName'],
            'last_name' => $data['lastName'],
            'type' => $data['type'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'cell_number' => $data['phone'],
            'home_number' => '',
            'profile_picture' => $profilePicName,
            'created_by' => 'user',
            'updated_by' => 'user'
        ]);

        // $user->notify(new UserRegistered($user));

        return $user;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register', [
            'page_title' => 'Register',
            'page_heading' => 'Register'
        ]);
    }

     /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        if($user->type == 3)
            return redirect()->route('admin.dashboard');
    }
}
