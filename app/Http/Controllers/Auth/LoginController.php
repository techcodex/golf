<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Paypal\PaypalAgreement;
use App\Agreement;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $data = [
            'page_title' => 'Login',
            'page_heading' => 'Login Form'
        ];
        return view('auth.login')->with($data);
    }

    /**
     * Log the user out of the application & set the last login time.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function logout(Request $request)
    {
        if(Auth::user())
        {
            $user = Auth::user();
            $user->logged_at = Carbon::now();
            $user->save();
        }

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }
    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if($user->type == 3)
            return redirect()->route('admin.dashboard');
        else if($user->type == 1) {
            $agreement = $user->agreement->first();
            if($agreement != null)
            {
                $paypalAgreement = new PaypalAgreement();
                $agree = json_decode($agreement->detail);
                $agree = $paypalAgreement->getAgreement($agree->id);
                $agreement->detail = json_encode(json_decode($agree));
                $agreement->save();
            }
        }
    }

}
