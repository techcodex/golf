<?php

namespace App\Http\Controllers;

use App\Property_images;
use Auth;
use Illuminate\Support\Facades\Route;
use Session;
use App\Space;
use App\Country;
use App\Place_type;
use App\HostProperty;
use Illuminate\Http\Request;
use App\Http\Requests\HostRequest;
use Illuminate\Support\Facades\Mail;
use App\Notifications\PropertyRegistered;
use Yajra\DataTables\DataTables;

class HostPropertyController extends Controller
{

    public function index()
    {
        $properties = Auth::user()->hostProperties;

        return view('host.index')
            ->with('properties', $properties)
            ->with('page_title', 'Host Properties')
            ->with('page_heading', 'All Host Properties');

    }

    public function create()
    {
        $countries = Country::orderBy('name','asc')->get();
        return view('host.create', [
            'page_title' => 'Host Property',
            'page_heading' => 'Create Host Property',
            'types'=>Place_type::all(),
            'spaces'=>Space::all(),
            'countries'=>$countries,
        ]);
    }

    public function store(HostRequest $request)
    {
            $property = HostProperty::create([
            'owner_id' => Auth::id(),
            'owner_name' => $request->fullName,
            'host_phone' => $request->phone,
            'host_email' => $request->email,
            'street_address' => $request->streetAddress,
            'condo_apt_no' => $request->apt,
            'city' => $request->city,
            'state_id' => $request->state,
            'zip_code' => $request->zip,
            'country_id' => $request->country,
            'pet' => $request->pet,
            'smoke' => $request->smoke,
            'status' => 1,
            'place_type_id'=>$request->place_type_id,
            'space_id'=>$request->space_id,
            'accommodates'=>$request->accommodates,
            'comment' => $request->comments,
            'created_by' => 'user',
            'updated_by' => 'user'
        ]);
//        dd($property->id);
        $id = $property->id;
        foreach($request->property_images as $image) {
            $imageName = time().$image->getClientOriginalName();
            if($image->move('images/properties',$imageName)) {
                $imageName = "images/properties/".$imageName;
            } else {
                $imageName = "images/properties/default.png";
            }
            Property_images::create([
                'image'=>$imageName,
                'host_property_id'=>$id,
            ]);
        }
        Auth::user()->notify(new PropertyRegistered($property));

        Session::flash('success', 'Host Property Created Successfully!');
        return back();
    }

    public function edit(HostProperty $hostProperty)
    {
        if(Auth::id() != $hostProperty->owner_id)
        {
            Session::put('info', 'You Dont Have Permission to Access this Page!');
            return back();
        }

        return view('host.edit')
            ->with('property', $hostProperty)
            ->with('page_title', 'Edit Property')
            ->with('page_heading', 'Edit Host Property');
    }
    /*
     * Deleteing host property
     */

    public function destroy(HostProperty $hostProperty)
    {

        if($hostProperty->stays != "") {
            Session::flash('error','Cannot Delete Property');
            return back();
        }
        if($hostProperty->availability != "") {
            Session::flash('error','Cannot Delete Property');
            return back();
        }
        if($hostProperty->owner_id != Auth::id())
        {
            Session::flash('info', 'You Dont Have Permission to Access this Page!');
            return back();
        }
        $hostProperty->delete();
        Session::put('success', 'Property Deleted');

        return back();
    }
    /*
     * Deleteing host property ajax
     */

    public function destroyAjax($id)
    {
        $hostProperty = HostProperty::find($id);
        if($hostProperty->stays != "") {
            $response['error'] = true;
            $response['msg'] = "Cannot Delete Property";
            return $response;
        }
        if($hostProperty->availability != "") {
            $response['error'] = true;
            $response['msg'] = "Cannot Delete Property";
            return $response;
        }
//        dd($hostProperty->stays);
        $response = [];
        $hostProperty->delete();
        $response['success'] = true;
        $response['msg'] = "Property Deleted Successfully";
        return $response;
    }
    public function createDashboard()
    {
//        dd("sss");
        $countries = Country::orderBy('name','asc')->get();

        return view('host.dashboard.create', [
            'page_title' => 'Host Property',
            'page_heading' => 'Create Host Property',
            'types'=>Place_type::all(),
            'spaces'=>Space::all(),
            'countries'=>$countries,
            'dashboard'=>'Host Dashboard'
        ]);
    }
    public function indexDashboard() {
//        $properties = Auth::user()->hostProperties;

        return view('host.dashboard.index')
            ->with('page_title', 'Host Properties')
            ->with('page_heading', 'All Host Properties')
            ->with('dashboard','Host Dashboard');
    }
    public function getPropertiesAjax() {
        $properties = Auth::user()->hostProperties;
        return DataTables::of($properties)
            ->setRowId(function ($property) {
                return $property->id;
            })
            ->addColumn('place_type_id',function (HostProperty $property) {
                return $property->place_type->name;
            })
            ->addColumn('space_id',function(HostProperty $property) {
                return $property->space->name;
            })
            ->addColumn('action','action')
            ->toJson();
    }
    /*
     * Edit Host property From dashboard
     */
    public function editDashboard($id)
    {
        $host_property = HostProperty::find($id);
        return view('host.dashboard.edit')->with([
            'page_title' => 'Edit Property',
            'page_heading' => 'Edit Property',
            'dashboard' => 'Host Dashboard',
            'property' => $host_property,
            'countries' => Country::all(),
            'spaces' => Space::all(),
            'types' => Place_type::all(),
        ]);
    }

    public function updateDashboard(HostRequest $request,$id) {
        $property = HostProperty::find($id);

        $property->owner_name = $request->fullName;
        $property->host_phone = $request->phone;
        $property->host_email = $request->email;
        $property->street_address = $request->streetAddress;
        $property->condo_apt_no = $request->apt;
        $property->city = $request->city;
        $property->country_id = $request->country;
        $property->state_id = $request->state;
        $property->zip_code = $request->zip;
        $property->pet = $request->pet;
        $property->smoke = $request->smoke;
        $property->comment = $request->comments;
        $request->place_type_id = $request->place_type_id;
        $request->space_id = $request->space_id;

        $property->save();
        Session::flash('success','Property Updated Successfully');
        return back();
    }
    public function listMyRooms() {
        $data = [
            'page_title'=>'List My Rooms',
            'page_heading'=>'List My Rooms',
            'dashboard'=>'Host Dashboard',
            'properties'=>Auth::user()->hostProperties,
        ];
        return view('host.dashboard.listing')->with($data);
    }
}
