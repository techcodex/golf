<?php

namespace App\Http\Controllers;

use App\Availability;
use App\State;
use Auth;
use App\Event;
use Carbon\Carbon;
use App\HostProperty;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Requests\AvaiableRequest;
use Session;
use Yajra\DataTables\DataTables;

class AvaialabilityController extends Controller
{
    /*
     *  Returning host a dashboard with his property availablity
     */
    public function indexDashboard() {
        $data = [
            'page_title'=>'Availablity',
            'page_heading'=>'My Properties Availabilities',
            'dashboard'=>'Host Dashboard',
        ];
        return view('avaiable.dashboard.index')->with($data);
    }

    /*
     * index dashboard to show all host availability
     */
    public function indexAdmin() {
        $data = [
            'page_title'=>'Availabilities',
            'page_heading'=>'Host Availablities',
            'active'=>'availability'
        ];
        return view('admin.availability.index')->with($data);
    }

    public function getAllAvailabilities() {
        $availabilites = Availability::all();
        if($availabilites->count() == 0) {
                $array = [
                    "sEcho"=>0,
                    "iTotalRecords"=>0,
                    "iTotalDisplayRecords"=>0,
                    "aaData"=>[],
                ];
                return json_encode($array);
        }
        return DataTables::of($availabilites)
            ->rawColumns(['status'])
            ->addColumn('host_name',function (Availability $availability) {
                return $availability->host->first_name;
            })
            ->addColumn('event_name',function (Availability $availability) {
                return $availability->event->name;
            })
            ->addColumn('start_date',function (Availability $availability) {
                return $availability->start_date->toFormattedDateString();
            })
            ->addColumn('end_date',function (Availability $availability) {
                return $availability->end_date->toFormattedDateString();
            })
            ->addColumn('action','action')

            ->addColumn('status',function (Availability $availability) {
                return $availability->status == 1 ? "<a href='#' class='btn btn-sm m-btn--pill m-btn--air btn-danger deactive' data-id='$availability->id'>Deactive</a>" : "<a href='#' data-id='$availability->id' class='btn btn-sm m-btn--pill m-btn--air btn-primary active'>Active</a>";
            })
            ->toJson();
    }

    public function getAvailablity() {
        $host_properties =  Auth::user()->hostProperties;
//        dd($host_properties);
        $avaliabilities = [];
        foreach($host_properties as $property) {
            $avaliabilities[] = $property->availability;
        }
//        dd($avaliabilities);
        if($avaliabilities[0] == null) {
            $array = [
                "sEcho"=>0,
                "iTotalRecords"=>0,
                "iTotalDisplayRecords"=>0,
                "aaData"=>[],
            ];
            return json_encode($array);
        }

        return DataTables::of($avaliabilities)
            ->addColumn('event_name',function (Availability $availability) {
                return $availability->event->name;
            })
            ->addColumn('start_date',function (Availability $availability) {
                return $availability->start_date->toFormattedDateString();
            })
            ->addColumn('end_date',function(Availability $availability) {
                return $availability->end_date->toFormattedDateString();
            })

            ->addColumn('action','action')
            ->rawColumns(['status','state'])
            ->addColumn('status',function (Availability $availability){
                return $availability->status == 1 ? "<a href='#' class='btn-sm btn-danger deactive' data-id='".$availability->id."'>Deactive</a>" : "<a href='#' class='btn-sm btn-success active' data-id='".$availability->id."'>Active</a>";
            })
            ->addColumn('state',function (Availability $availability) {
                return $availability->status == 1 ? "<span class='badge badge-primary'>Active</span>" : "<span class='badge badge-danger'>Deactive</span>" ;
            })
            ->toJson();

    }


    public function create()
    {
        $properties = Auth::user()->hostProperties;
        $events = Event::FutureEvent()->get();
        return view('avaiable.create', [
            'page_title' => 'Property Avaiability',
            'page_heading' => 'Add Property Avaiability',
            'properties' => $properties,
            'events' => $events
        ]);
    }
    /*
     * Create Availiblity in host dashboard
     */
    public function createDashboard() {

        $properties = Auth::user()->hostProperties;
//        $events = Event::FutureEvent()->get();

        $data = [
            'properties'=>$properties,
            'page_title'=>'Property Avaiability',
            'page_heading'=>'Add Property Avaiability',
            'dashboard'=>'Host Dashboard',
        ];
        return view('avaiable.dashboard.create')->with($data);
    }
    /*
     * Store availability in db
     * Request as paramter
     */

    public function show($id) {
        $availibilty  = Availability::find($id);
        $data = [
            'page_title'=>'Host Property',
            'page_heading'=>$availibilty->display_name,
            'availability'=>$availibilty,
        ];
        return view('avaiable.show')->with($data);

    }
    /*
     * Store the availability
     * request as parameter
     */
    public function store(AvaiableRequest $request)
    {
//        $availabilities = Availability::where('host_property_id','=',$request->property)->get();
////        dd($availability);
//        foreach ($availabilities as $availability) {
//            if($availability->status == -1) {
//                Session::flash('error','Property Is Booked by caddie in '.$availability->event->name);
//            }
//        }
        $host_property = HostProperty::find($request->property);
        if($host_property->status == -1) {
            Session::flash('error','Property is Booked by caddie for an event');
            return back();
        }
        $startDate = $request->startDate;

        $temp = explode("/",$startDate);
        $newStartDate = $temp[2]."-".$temp[0]."-".$temp[1];
        $newStartDate = $newStartDate."00:00:00";
        $endDate = $request->endDate;
        $temp1 = explode("/",$endDate);
        $newEndDate = $temp[2]."-".$temp[0]."-".$temp[1];
        $newEndDate  = $newEndDate."00:00:00";
//        dd($newEndDate);
        $pro = HostProperty::findOrFail($request->property);
        $env = Event::findOrFail($request->event);
        if($pro->owner_id != Auth::id())
        {
            Session::put('error', 'Something Went Wrong!');
            return back();
        }

        Availability::create([
            'status' => '1',
            'host_property_id' => $request->property,
            'display_name' => Auth::user()->first_name,
            'host_id' => $pro->owner_id,
            'host_city' => $pro->city,
            'host_street' => $pro->street_address,
            'event_name' => $env->name,
            'event_id' => $env->id,
            'start_date' => $newStartDate,
            'end_date' => $newEndDate,
            'comment' => $request->comment,
            'created_by' => 'host',
            'updated_by' => 'host'
        ]);

        Session::flash('success', 'Avaialability Created Successfully!');
        return back();
    }
    /*
     * Get all Events
     * return ajax response
     */
    public function getEvents(Request $request) {
        $response = [];
        $property = HostProperty::find($request->id);

        $events = Event::where('state_id',$property->state_id)->get();

        if(count($events) != 0) {
            $response['success'] = true;
            $response['events'] = $events;
        } else {
            $response['error'] = true;
            $response['msg'] = "No Event To Show Near Your State";
        }
        return $response;
    }
    /*
     * For deactive the availability status
     * request as parameter
     */
    public function deactive(Request $request) {
        $response = [];
        $availability = Availability::find($request->id);
        if($availability->status == -1) {
            $response['error'] = "Property is booked by caddie";
            return back();
        }
        $availability->status = 0;
        $availability->save();
        $response['success'] = true;
        return $response;
    }
    /*
     * For active the availability status
     * request as parameter
     */
    public function active(Request $request) {
        $response = [];
        $availability = Availability::find($request->id);
        if($availability->status == 2) {
            $response['error'] = true;
            $response['msg'] = "Deactive By Admin Contact Admin";
            return $response;
        }
        if($availability->status == -1) {
            $response['error'] = "Property is booked by caddie";
            return back();
        }
        $availability->status = 1;
        $availability->save();
        $response['success'] = true;
        return $response;
    }
    /*
     * Deleting availability
     * id as parameter
     */
    public function destroy($id) {
        $response = [];

        $availability = Availability::find($id);
        if(Auth::user()->id != $availability->host_id) {
            $response['error']  = true;
            return $response;
        }
        if($availability->status == -1) {
            $response['error'] = "Property is booked by caddie";
            return back();
        }

        $availability->delete();
        $response['success'] = true;
        return $response;

    }
    /*
     * edting availability
     * return view
     */
    public function edit($id) {
        $availability = Availability::find($id);
        $data = [
            'page_title'=>'Edit Availability',
            'page_heading'=>'Edit Availability',
            'dashboard'=>'Host Dashboard',
            'availability'=>$availability,
            'properties'=>Auth::user()->hostProperties,
        ];
        return view('avaiable.dashboard.edit')->with($data);
    }
    /*
     * Update the availability
     * Request as parameter
     */
    public function update(Request $request,$id) {

        $startDate = $request->startDate;
        $temp = explode('/',$startDate);

        $endDate = $request->endDate;
        $temp1 = explode('/',$endDate);
        $newStartDate = $request->startDate;
        $newEndDate = $request->endDate;
        if(count($temp) != 1) {
            $newStartDate = $temp[2]."-".$temp[0]."-".$temp[1];
        }
        if(count($temp1) != 1) {
            $newEndDate = $temp1[2]."-".$temp1[0]."-".$temp1[1];
        }

        $pro = HostProperty::findOrFail($request->property);
        if($pro->owner_id != Auth::id())
        {
            Session::put('error', 'Something Went Wrong!');
            return back();
        }

        $availability = Availability::find($id);
        $availability->host_property_id = $request->property;
        $availability->event_id = $request->event;
        $availability->comment = $request->comment;
        $availability->start_date = $newStartDate;
        $availability->end_date = $newEndDate;
        $availability->save();

        Session::flash('success',"Availability Updated Successfully");
        return back();
    }
    /*
     * Get host available propeties
     */
    public function getAvailableProperties($id) {
        $event = Event::find($id);
        $properties =  $event->availableProperties()->paginate(3);
        $data = [
            'page_title'=>'Properties',
            'page_heading'=>'Host Properties',
            'properties'=>$properties,
        ];
        return view('avaiable.available_properties_list')->with($data);

    }
    public function deactiveByAdmin(Request $request) {
        $availability = Availability::find($request->id);
        $response = [];
        if($availability->count() == 0) {
            $response['error'] = true;
            $response['msg'] = "Availablity Not Found";
            return $response;
        }
        if($availability->status == -1) {
            $response['error'] = "Property is booked by caddie";
            return back();
        }
        $availability->status = 2;
        $availability->save();
        $response['success'] = true;
        $response['msg'] = "Deactive Successfully";
        return $response;
    }
    public function activeByAdmin(Request $request) {
        $availability = Availability::find($request->id);
        $response = [];
        if($availability->count() == 0) {
            $response['error'] = true;
            $response['msg'] = "Availablity Not Found";
            return $response;
        }
        if($availability->status == -1) {
            $response['error'] = "Property is booked by caddie";
            return back();
        }
        $availability->status = 1;
        $availability->save();
        $response['success'] = true;
        $response['msg'] = "Active Successfully";
        return $response;
    }
}
