<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $data = Message::select('from')->where('to', auth()->id())->orWhere('from', auth()->id())->groupBy('from')->get();
        $data1 = Message::select('to')->where('to', auth()->id())->orWhere('from', auth()->id())->groupBy('to')->get();
        $fromArray = $data->map(function($d){
            return $d->from;
        });

        $toArray = $data1->map(function($d){
            return $d->to;
        });

        $merged = $fromArray->merge($toArray);
        $merged = $merged->unique();
        $merged = $merged->filter(function($item){
            return $item != auth()->id();
        });

        $users = User::find($merged->toArray());

        $unReadIds = Message::select(\DB::raw(" `from` as sender_id, count(`from`) as message_count"))
            ->where('to', auth()->id())
            ->where('read_at', null)
            ->groupBy('from')
            ->get();

        $users = $users->map(function($user) use ($unReadIds){
            $userUnread = $unReadIds->where('sender_id', $user->id)->first();

            $user->unread = $userUnread ? $userUnread->message_count : 0;

            return $user;
        });

        return response()->json($users);
    }
}
