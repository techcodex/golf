<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paypal\SubscriptionPlan;
use Session;
use App\Plan;

class PlanController extends Controller
{
    private $plan;

    public function __construct()
    {
        $this->plan = new SubscriptionPlan();
    }

    public function create()
    {
        return view('admin.plan.create')->with(['page_heading' => 'Create New Plan', 'page_title' => 'Create Plan', 'active' => 'add-plan']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'frequency' => 'required',
            'interval' => 'required|numeric'
        ]);

        $plan = $this->plan->create($request->all());
        $plan = json_decode($plan);
        Plan::create([
            'plan_id' => $plan->id,
            'name' => $plan->name,
            'description' => $plan->description,
            'price' => $plan->payment_definitions[0]->amount->value,
            'frequency' => $plan->payment_definitions[0]->frequency
        ]);
        return redirect()->route('plan.index');
    }

    public function index()
    {
        $plans = json_decode($this->plan->list());
        if(!property_exists($plans, 'plans'))
            $plans->plans = [];
        return view('admin.plan.index', ['plans' => $plans->plans, 'page_heading' => 'All Plans', 'page_title' => 'Plans', 'active' => 'show-plan']);
    }

    public function show($id)
    {
        $detail = json_decode($this->plan->getDetail($id));
        return view('admin.plan.show', ['page_heading' => 'Plan Detail', 'page_title' => $detail->name, 'plan' => $detail, 'active' => 'show-plan']);
    }

    public function destroy($id)
    {
        $res = $this->plan->deletePlan($id);
        if($res)
            Session::flash('success', 'Deleted Successfully!');
        else
            Session::flash('success', 'Some Error Occur');

        $plan = Plan::where('plan_id', $id)->firstOrFail();
        $plan->delete();
        Session::flash('success', 'Successfully Deleted!');

        return back();
    }

}
