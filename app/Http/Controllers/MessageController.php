<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;
use Carbon\Carbon;
use App\Events\NewMessage;
use Illuminate\Http\Request;
use App\Notifications\NewMessage as NewMessageReceived;

class MessageController extends Controller
{

    public function index(Request $request, $any = null)
    {
        $host = \json_encode(new \stdClass());
        if($request->has('host'))
        {
            $host = User::findOrFail($request->host);
        }
        return view('messenger', compact('host'));
    }

    public function show($id)
    {
        Message::where('from', $id)->where('to', auth()->id())->update(['read_at' => Carbon::now()]);

        $messages = Message::where(function($query) use ($id){
            $query->where('to', $id);
            $query->where('from', auth()->id());
        })->orWhere(function($query) use ($id){
            $query->where('from', $id);
            $query->where('to', auth()->id());
        })->orderBy('created_at', 'asc')->get();

        $messages = $messages->map(function($message){
            $message->created_at_formatted = $message->created_at->toFormattedDateString();
            return $message;
        });

        return response()->json($messages);
    }

    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);

        // Send Message
        $message = $this->sendMessage($id, auth()->id(), $request->message);

        return response()->json($message);
    }

    private function sendMessage($to, $from, $message)
    {
        $messagesCount = Message::where(function($query) use ($to, $from){
            $query->where('to', $to);
            $query->where('from', $from);
        })->get()->count();

        $message = Message::create([
            'to' => $to,
            'from' => $from,
            'message' => $message
        ]);

        $message->created_at_formatted = $message->created_at->toFormattedDateString();

        broadcast(new NewMessage($message));

        if($messagesCount == 0){
            $userTo = User::find($to);
            $userFrom = User::find($from);
            $userTo->notify(new NewMessageReceived($userFrom, $message));
        }

        return $message;
    }

    public function bulkMessage()
    {
        $users = User::where('type', '!=', 3)->get();
        return view('admin.message', [
            'page_title' => 'Bulk Message',
            'page_heading' => 'Bulk Message',
            'active' => 'message',
            'users' => $users
        ]);
    }

    public function bulkSend(Request $request)
    {
        $request->validate([
            'bulkmessage' => 'required',
            'users' => 'nullable|array'
        ], [
            'bulkmessage.required' => 'Message Should not be empty.'
        ]);

        $messageUser = User::where('email', 'support@casadecaddie.com')->first();

        if(!$messageUser)
            return response()->json(['message' => 'Support Account Not Found to Send Email'], 422);

        if($request->has('users') && count($request->users) > 0){
            $users = User::find($request->users);
            foreach($users as $user){
                if($user->type != 3){
                    $this->sendMessage($user->id, $messageUser->id, $request->bulkmessage);
                }
            }
        } else {
            User::chunk(50, function($users) use ($messageUser, $request){
                foreach($users as $user){
                    if($user->type != 3){
                        $this->sendMessage($user->id, $messageUser->id, $request->bulkmessage);
                    }
                }
            });
        }

        return response()->json(['message' => $request->bulkmessage]);
    }
}
