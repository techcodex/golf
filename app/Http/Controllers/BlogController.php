<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Blog;
use Session;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.blog.index')
            ->with('page_title', 'Blogs')
            ->with('page_heading', 'All Blogs')
            ->with('active', 'show-blog');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create')
            ->with('page_title', 'Blog')
            ->with('active', 'add-blog')
            ->with('page_heading', 'Create New Blog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:240',
            'description' => 'required',
            'featured' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $featured = $request->featured;
        $featuredName = time() . $featured->getClientOriginalName();
        if($featured->move('images/blog', $featuredName))
        {
            Blog::create([
                'title' => $request->title,
                'description' => $request->description,
                'slug' => str_slug($request->title),
                'featured' => 'images/blog/' . $featuredName
            ]);

            Session::flash('success', 'Blog Successfully Created!');

            return back();
        }

        abort(500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $blog = Blog::where('slug', $slug)->firstOrFail();
        return view('blog_detail')
                ->with('blog', $blog)
                ->with('page_title', $slug)
                ->with('page_heading', $blog->title);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);

        return view('admin.blog.edit')
            ->with('page_title', 'Edit Blog')
            ->with('active', 'show-blog')
            ->with('page_heading', 'Edit Blog')
            ->with('blog', $blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $this->validate($request, [
            'title' => 'required|max:240',
            'description' => 'required'
        ]);

        $updatedFeatured = $blog->featured;

        if($request->has('featured'))
        {
            $featured = $request->featured;
            $featuredName = time() . $featured->getClientOriginalName();
            if($featured->move('images/blog', $featuredName))
            {
                $updatedFeatured = 'images/blog/' . $featuredName;
                if(file_exists($blog->featured))
                    unlink($blog->featured);
            }
        }
        $blog->title = $request->title;
        $blog->description = $request->description;
        $blog->slug = str_slug($request->title);
        $blog->featured = $updatedFeatured;
        $blog->save();
        Session::flash('success', 'Blog Updated Successfully!');
        return redirect()->route('blog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::findOrFail($id);
        if(file_exists($blog->featured))
            unlink($blog->featured);
        $blog->delete();
        Session::flash('success', 'Blog Deleted Successfully!');
        return back();
    }

    public function get_blogs()
    {
        return Datatables::of(Blog::query())

        ->setRowId(function ($blog) {
            return $blog->id;
        })
        ->addColumn('action','action')
        ->editColumn('featured', function(Blog $blog){
            $imageUrl = asset($blog->featured);
            return "<img src='$imageUrl' class='img-thumbnail text-center' height='100' width='120'>";
        })
        ->rawColumns(['featured'])
        ->editColumn('created_at', function (Blog $blog) {
            return $blog->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function (Blog $blog) {
            return $blog->updated_at->diffForHumans();
        })
        ->toJson();
    }
}
