<?php

namespace App\Http\Controllers;

use App\Caddie;
use App\Http\Requests\CaddieRequest;
use App\Notifications\CaddieAccountActivated;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Auth;
use Session;

class CaddiesController extends Controller
{
    /**
     * Display all caddies
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = [
            'page_title'=>'All Caddies Data',
            'page_heading'=>'All Caddies',
            'active'=>'caddies',
        ];
        return view('admin.caddies.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    //    dd(Auth::user());
//        dd("ssss");
        if(Auth::user()->type != '1') {
            return back();
        }
        $user = User::find(Auth::user()->id);

//        dd($user_profile);
        $data = [
            'page_title'=>'My Account',
            'page_heading'=>'My Account',
            'user'=>$user,
            'full_name'=>$user->first_name.$user->last_name,
        ];
        return view('caddies.caddie_account')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CaddieRequest $request)
    {
        $caddie_profile = Auth::user()->caddie;
        if(!$caddie_profile) {
            Caddie::create([
                'user_id'=>Auth::id(),
                'phone'=>$request->phone,
                'email'=>$request->email,
                'caddie_name'=>$request->caddie_name,
                'pga_lpga_id'=>$request->pga_lpga_id,
                'pga_lpga'=>$request->pga_lpga,
                'pet'=>$request->pet,
                'smoke'=>$request->smoke,
                'created_by'=>'user',
                'updated_by'=>'user',
            ]);
        } else {
            $caddie_profile->pga_lpga_id = $request->pga_lpga_id;
            $caddie_profile->pga_lpga = $request->pga_lpga;
            $caddie_profile->pet = $request->pet;
            $caddie_profile->smoke = $request->smoke;
            $caddie_profile->save();
        }
        Session::put('success','Account Updates Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $caddie = Caddie::find($id);
        if($caddie ==  "") {
            Session::flash('error','Caddie Not Update his Profile Yet');
            return back();
        }
        $data = [
            'page_title'=>'Edit Caddie Profile',
            'page_heading'=>'Edit Caddie Profile',
            'caddie'=>$caddie,
            'active'=>'caddies',
        ];
        return view('admin.caddies.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $caddie = Caddie::findOrFail($id);
        $caddie->caddie_name = $request->caddie_name;
        $caddie->phone = $request->phone;
        $caddie->email = $request->email;
        $caddie->pga_lpga_id = $request->pga_lpga_id;
        $caddie->pet = $request->pet;
        $caddie->smoke = $request->smoke;
        $caddie->save();
        Session::put('success','Caddie Profile Updated Succssfully');
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function dashboard() {
        $data = [
            'page_heading'=>'Caddie Dashboard',
            'page_title'=>'Caddie Dashboard',
            'dashboard'=>'Caddie Dashboard',
        ];
        return view('caddies.caddie_dashboard')->with($data);
    }

    public function notification()
    {
        $data = [
            'page_heading'=>'Notification',
            'page_title'=> Auth::user()->first_name . ' Notification ',
            'dashboard'=>'Caddie Dashboard',
        ];
        return view('caddies.notification')->with($data);
    }

    /**
     * get the data of caddies in users table
     *
     * @return Yajra\DataTables\DataTables;
     */
    public function caddies_list() {
        $caddies = User::where('type',1)->get();
        return Datatables::of($caddies)

            ->setRowId(function ($user) {
                return $user->id;
            })
            ->rawColumns(['status','featured'])
            ->addColumn('status', function (User $user) {
                return $user->status == 1 ? "<a href='#' class='btn btn-sm m-btn--pill m-btn--air btn-danger deactive' data-id='$user->id'>Deactive</a>" : "<a href='#' data-id='$user->id' class='btn btn-sm m-btn--pill m-btn--air btn-primary active'>Active</a>";
            })
            ->addColumn('featured',function (User $user) {
                if($user->caddie != "") {
                    return $user->caddie->featured == 1 ? "<a href='#' data-id='$user->id' class='btn btn-sm m-btn--pill m-btn--air btn-danger disable'>Disable</a>" : "<a href='#' data-id='$user->id' class='btn btn-sm m-btn--pill m-btn--air btn-primary featured'>Featured</a>";
                } else {
                    return "n/a";
                }
            })
            ->rawColumns(['status','featured'])
            ->addColumn('action','action')
            ->editColumn('created_at', function (User $user) {
                return $user->created_at->diffForHumans();
            })
            ->editColumn('updated_at', function (User $user) {
                return $user->updated_at->diffForHumans();
            })
            ->addColumn('caddie_name',function (User $user) {
                if($user->caddie != "") {
                    return $user->caddie->caddie_name;
                } else {
                    return "n\a";
                }

            })
            ->addColumn('pga_lpga_id',function (User $user) {
                if($user->caddie != "") {
                    return $user->caddie->pga_lpga_id;
                } else {
                    return "n\a";
                }

            })
            ->addColumn('pga_lpga',function (User $user) {
                if($user->caddie != "") {
                    return $user->caddie->pga_lpga == '1' ? "PGA" : "LPGA";
                } else {
                    return "n\a";
                }

            })->addColumn('status',function(User $user) {
                if($user->caddie != "") {
                    return $user->caddie->status == 1 ? "<a href='#' class='btn-sm btn-danger deactive' data-id='".$user->caddie->user_id."'>Deactive</a>" : "<a href='#' data-id='".$user->caddie->user_id."' class='btn-sm btn-primary active'>Active</a>";
                } else {
                    return "n/a";
                }
            })
            ->toJson();
    }
    /*
        For actiive caddie profile
    */
    public function activeCaddieProfile($id) {
//        dd($id);
        $response = [];
        $caddie = Caddie::where('user_id',$id)->get()->first();
        $user = User::find($id);
        if($caddie) {
            $caddie->status = 1;
            $caddie->save();
            $user->notify(new CaddieAccountActivated($user));
            $response['success'] = true;
        } else {
            $response['error'] = true;
            $response['msg'] = "User Not Found";
        }
        return $response;

    }
    /*
        For deactive caddie profile
    */
    public function deactiveCaddieProfile($id) {
//        dd($id);
        $response = [];
        $caddie = Caddie::where('user_id',$id)->get()->first();
        $user = User::find($id);
        if($caddie) {
            $caddie->status = 0;
            $caddie->save();



            $response['success'] = true;
        } else {
            $response['error'] = true;
            $response['msg'] = "User Not Found";
        }
        return $response;

    }
    public function featured($id) {
        $response = [];
        $users = User::where('type','=','1')->get();
        $i = 0;
        foreach($users as $user) {
            $caddie = $user->caddie;
            if($caddie && $caddie->featured == 1) {
                $i++;
            }
        }
        if($i <= 3) {
            $caddie = Caddie::find($id);
            $caddie->featured = 1;
            $caddie->save();

            $response['success'] = true;
            return $response;
        }
        $response['error'] = true;
        $response['msg'] = "Please Disbale other caddie first limit 3";
        return $response;
    }

    public function disable($id) {
        $response = [];

        $caddie = Caddie::find($id);
        $caddie->featured = 0;
        $caddie->save();

        $response['success'] = true;
        return $response;
    }
}
