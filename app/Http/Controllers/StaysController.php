<?php

namespace App\Http\Controllers;

use App\Availability;
use App\Event;
use App\HostProperty;
use App\Http\Middleware\Host;
use App\Stay;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Yajra\DataTables\DataTables;

class StaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'page_title'=>'Stays',
            'page_heading'=>'Stays',
            'active'=>'stay',
        ];
        return view('admin.stays.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $availability = Availability::find($request->availability_id);
        $event = Event::find($request->event_id);
        $host = User::find($request->host_id);
        $host_property = HostProperty::find($request->property_id);
        $now = date("Y-m-d H:i:s");
        if($availability->comment == "") {
            $availability_summary = "N/A";
        } else {
            $availability_summary = $availability->comment;
        }
        Stay::create([
            'availability_id'=>$request->availability_id,
            'availability_summary'=>$availability_summary,
            'event_id'=>$request->event_id,
            'event_name'=>$event->name,
            'event_start_date'=>$event->start_date,
            'event_end_date'=>$event->end_date,
            'host_id'=>$request->host_id,
            'host_name'=>$host->first_name,
            'caddie_id'=>Auth::user()->id,
            'caddie_name'=>Auth::user()->first_name,
            'stay_start_date'=>$now,
            'stay_end_date'=>$event->end_date,
            'host_property_id'=>$request->property_id,
            'stay_notes'=>'Stay by '.Auth::user()->first_name.' in '.$host->first_name,
            'created_by'=>'Caddie',
            'updated_by'=>'Caddie',
        ]);

        $availability->status = -1;
        $availability->save();

        $host_property->status = -1;
        $host_property->save();

        Session::flash('success','property book successfully');
        return redirect()->route('event.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getStays() {
        return Datatables::of(Stay::query())
            ->setRowId(function ($stay) {
                return $stay->id;
            })
            ->addColumn('caddie_name',function (Stay $stay) {
                return $stay->caddie->first_name;
            })
            ->addColumn('host_name',function (Stay $stay) {
                return $stay->host->first_name;
            })
            ->addColumn('caddie_cell_no',function (Stay $stay) {
                return $stay->caddie->cell_number;
            })
            ->addColumn('host_cell_no',function (Stay $stay) {
                return $stay->host->cell_number;
            })
            ->toJson();
    }
    public function nearestStay($id) {
        $now = date("Y-m-d");
        $event = Event::find($id);
        // dd($event->city);
        $availabilities = Availability::where([
            ['event_id','=',$id],
            ['host_city','like','%'.$event->city.'%']
        ])->get();
        
        if(count($availabilities) == 0) {
            Session::flash('error','No Property Is Available Near to Event Please Choice Any Property you like By Clicking
                on Host Properties
            ');
        }
        $host =  $availabilities[0]->host;
        $host_property = $availabilities[0]->host_property;

        // dd($availabilities[0]);
        if($availabilities[0]->comment == null) {
            $availability_summary = "N/A";
        } else {
            $availability_summary = $availabilities[0]->comment;
        }
        // dd($availability_summary);
        Stay::create([
            'availability_id'=>$availabilities[0]->id,
            'availability_summary'=>$availability_summary,
            'event_id'=>$event->id,
            'event_name'=>$event->name,
            'event_start_date'=>$event->start_date,
            'event_end_date'=>$event->end_date,
            'host_id'=>$host->id,
            'host_name'=>$host->first_name,
            'caddie_id'=>Auth::user()->id,
            'caddie_name'=>Auth::user()->first_name,
            'stay_start_date'=>$now,
            'stay_end_date'=>$event->end_date,
            'host_property_id'=>$host_property->id,
            'stay_notes'=>'Stay by '.Auth::user()->first_name.' in '.$host->first_name,
            'created_by'=>'Caddie',
            'updated_by'=>'Caddie',
        ]);
        $availabilities[0]->status = -1;
        $availabilities[0]->save();

        $host_property->status = -1;
        $host_property->save();

        Session::flash('success','Property Booked Successfully Check your Reservations');
        return redirect()->route('event.list');
    }   
}
