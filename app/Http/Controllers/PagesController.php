<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class PagesController extends Controller
{
    //
    public function index() {
        $users = User::where('type','=','1')->get();

        $featured = [];
        foreach($users as $user) {
            $caddie = $user->caddie;
            if($caddie && $caddie->featured == '1') {
                $featured[] = $user;
            }
        }
        $data = [
            'page_title'=>'Home',
            'page_heading'=>'Home',
            'caddies'=>$featured,

        ];


        return view('index')->with($data);
    }

    public function contact() {
        $data = [
            'page_heading'=>'Contact Us',
            'page_title'=>'Contact',
        ];
        return view('contact_us')->with($data);
    }

    public function aboutus() {
        $data = [
            'page_heading'=>'About Us',
            'page_title'=>'About Us',
        ];
        return view('about_us')->with($data);
    }

    public function events() {
        $data = [
            'page_heading'=>'Events',
            'page_title'=>'Events',
        ];
        return view('events')->with($data);
    }

    public function blog() {
        $blogs = Blog::paginate(3);
        $data = [
            'page_title'=>'Our Blog',
            'page_heading'=>'Our Blog',
            'blogs' => $blogs
        ];
        return view('blog')->with($data);
    }

    public function terms()
    {
        $data = [
            'page_heading' => 'Terms & Conditions',
            'page_title' => 'Terms & Conditions'
        ];
        return view('terms')->with($data);
    }

    public function adoptCaddie() {
        if(auth()->check() && auth()->user()->type != 2){
            abort(403);
        }

        $caddies = User::where('type','=','1')->paginate(6);
        $data = [
            'page_title'=>'Adopt Caddie',
            'page_heading'=>'Adopt Caddie',
            'caddies'=> $caddies,
        ];
        return view('adopt_caddie')->with($data);
    }
    public function store(Request $request) {
//        dd($request->all());
        $request->validate([
            'username'=>'required|min:3',
            'email'=>'required|email',
            'subject'=>'required|min:3',
            'message'=>'required|min:6',
        ]);
        Mail::send(new ContactMail($request));
        Session::flash('success','Message Send Successfully');
        return back();
    }
}
