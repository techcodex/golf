<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function create()
    {
        return view('admin.setting')
            ->with('page_title', 'Settings')
            ->with('page_heading', 'Profile Settings')
            ->with('active', 'settings');
    }

    public function email(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255|unique:users,email,' . auth()->id()
        ]);

        auth()->user()->email = $request->email;
        auth()->user()->save();
        session()->flash('success', 'Email Updated Successfully!');
        return back();
    }

    public function name(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
        ]);

        auth()->user()->first_name = $request->first_name;
        auth()->user()->last_name = $request->last_name;
        session()->flash('success', 'Name Updated Successfully');
        return back();
    }

    public function password(Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'old_password' => 'required|min:8',
            'new_password' => 'confirmed|min:8|different:old_password',
        ]);

        if (Hash::check($request->old_password, $user->password)) {
            $user->fill([
                'password' => Hash::make($request->new_password)
            ])->save();

            session()->flash('success', 'Password changed Successfully!');
            return back();

        } else {
            session()->flash('error', 'Old Password does not Match!');
            return back();
        }
    }
}
