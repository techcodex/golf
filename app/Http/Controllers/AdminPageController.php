<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Event;
use App\Stay;

class AdminPageController extends Controller
{
    //
    public function index()
    {
        $data = [
            'page_heading' => 'Admin Dashboard',
            'page_title' => 'Admin Dashboard',
        ];
        return view('admin.login')->with($data);
    }
    public function dashboard()
    {
        $caddies_count = User::where('type',1)->count();
        $host_count = User::where('type',2)->count();
        $admin_count = User::where('type',3)->count();
        $events = Event::all()->count();
        $stays = Stay::all()->count();
        $data = [
            'page_heading' => 'Admin Dashboard',
            'page_title' => 'Admin Dashboard',
            'active' => 'home',
            'caddie_count'=>$caddies_count,
            'host_count'=>$host_count,
            'admin_count'=>$admin_count,
            'events'=>$events,
            'stays'=>$stays
        ];
        return view('admin.dashboard')->with($data);
    }
}
