<?php

namespace App\Http\Controllers;

use App\Plan;
use App\Caddie;
use App\Agreement;
use App\Paypal\PaypalAgreement;
use App\Notifications\CaddieAdopted;
use App\Notifications\HostAdoptCaddie;

class SubscriptionController extends Controller
{

    public function index($caddieID = null)
    {
        if($caddieID){
            session()->push('caddieID', $caddieID);
        }
        $plans = Plan::all();
        // dd($plans);
        return view('pricing', ['page_title' => 'Pricing', 'page_heading' => 'Subscription Pricing', 'plans' => $plans]);
    }

    public function create($id)
    {
        $agreement = new PaypalAgreement();
        $approvalLink = $agreement->create($id);
        return redirect($approvalLink);
    }


    public function agreement($status)
    {
        $caddieID = null;
        if(session()->has('caddieID')){
            $caddieID = session()->pull('caddieID');
            session()->forget('caddieID');
        }
        if($status == 'success' && request()->has('token'))
        {
            $paypalAgreement = new PaypalAgreement();
            $agreement = $paypalAgreement->exceute(request()->token);
            $agreement = $paypalAgreement->getAgreement($agreement->getId());
            $jsonAgreement = json_decode($agreement);
            $agreem = json_encode($jsonAgreement);
            $agree = Agreement::create([
                'detail' => $agreem,
                'caddie_id' => $caddieID ? $caddieID[0] : auth()->user()->caddie->id
            ]);

            if($caddieID){
                $caddie = Caddie::find($caddieID);
                $caddie[0]->user->notify(new HostAdoptCaddie(auth()->user()));
                auth()->user()->notify(new CaddieAdopted($caddie[0]));
            }

            session()->flash('success', 'Subscribed Successfully! Please Login Again to Verify!');
            auth()->logout();
            return redirect()->route('login');
        }
        else
            abort(404);
    }
}
