<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class CalendarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
//        dd($events);
        $cal_events = [];
        foreach($events as $event) {
            $cal_events[] = Calendar::event($event->name, true, $event->start_date, $event->end_date->addDays(1),$event->id);
        }
//        dd($cal_events);

        $calendar = Calendar::addEvents($cal_events) //add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'firstDay' => 1
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            'eventClick'=>'function(calEvent, jsEvent, view){
                    var id = calEvent.id;
                    window.location.replace(route("event.show",[id]));
                }',
        ]);

        $data = [
            'page_title'=>'Events Calendar',
            'page_heading'=>'Events Calendar',
            'calendar'=>$calendar,
            'active'=>'show-event',
        ];

        return view('events.event_calendar')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /*
     * For geeting pga events
     */
    public function pgaEvents() {
        $events = Event::where('type','1')->get();
        $cal_events = [];
        foreach($events as $event) {
            $cal_events[] = Calendar::event(
                $event->name, true,
                $event->start_date,
                $event->end_date->addDays(1),
                $event->id);
        }
        $calendar = Calendar::addEvents($cal_events) //add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'firstDay' => 1
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            'eventClick'=>'function(calEvent, jsEvent, view){
                    var id = calEvent.id;
                    window.location.replace(route("event.show",[id]));
                }',
        ]);

        $data = [
            'page_title'=>'Pga Events Calendar',
            'page_heading'=>'Pga Events Calendar',
            'calendar'=>$calendar,
        ];
        return view('events.event_calendar')->with($data);
    }
    /*
     * for getting lpga events
     */
    public function lpgaEvents() {
        $events = Event::where('type','2')->get();
        $cal_events = [];
        foreach($events as $event) {
            $cal_events[] = Calendar::event(
                $event->name, true,
                $event->start_date,
                $event->end_date->addDays(1),
                $event->id);
        }
        $calendar = Calendar::addEvents($cal_events) //add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'firstDay' => 1
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            'eventClick'=>'function(calEvent, jsEvent, view){
                    var id = calEvent.id;
                    window.location.replace(route("event.show",[id]));
                }',
        ]);

        $data = [
            'page_title'=>'LPga Events Calendar',
            'page_heading'=>'LPga Events Calendar',
            'calendar'=>$calendar,
        ];
        return view('events.event_calendar')->with($data);
    }
}
