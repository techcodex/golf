<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TripsController extends Controller
{
    public function index() {
        $trips = Auth::user()->stays;
//        dd($trips)
        $data = [
            'page_title'=>'My Trips',
            'page_heading'=>'My Trips',
            'trips'=>$trips,
            'dashboard'=>'Caddie Dashboard',
        ];
        return view('caddies.dashboard.trips')->with($data);
    }
    public function reservation() {
        $reservations = Auth::user()->reservation;
//        dd($reservations);
        $data = [
            'page_title'=>'My Reservations',
            'page_heading'=>'My Reservations',
            'trips'=>$reservations,
            'dashboard'=>'Caddie Dashboard'
        ];
        return view('caddies.dashboard.reservations')->with($data);
    }
    public function listing() {
        $trips = Auth::user()->stays;
//        dd($trips)
        $data = [
            'page_title'=>'My Listing',
            'page_heading'=>'My Listing',
            'trips'=>$trips,
            'dashboard'=>'Caddie Dashboard',
        ];
        return view('caddies.dashboard.listing')->with($data);
    }
}
