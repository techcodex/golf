<?php

namespace App\Http\Middleware;

use Closure;

class IfCaddieSubscribe
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check())
        {
            $agreement = auth()->user()->agreement->first();

            if($agreement != null && $agreement->detail['state'] == 'Pending')
            {
                session()->flash('info', 'Your Subscription is pending try again after some time!');
                return back();
            }

            if($agreement == null || $agreement->detail['state'] != 'Active')
            {
                session()->flash('error', 'Your are not subscribed');
                return redirect()->route('pricing');
            }
        }
        else
            back();
        return $next($request);
    }
}
