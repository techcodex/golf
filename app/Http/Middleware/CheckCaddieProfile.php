<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckCaddieProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
//            dd(count(Auth::user()->caddie));
            if(Auth::user()->caddie()->exists() && Auth::user()->caddie->status == 0) {
                Session::flash('error','Please Update Your Account Or Your Account is no Active Yet');
                return redirect()->back();
            }
        }
        return $next($request);
    }
}
