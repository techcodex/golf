<?php

namespace App\Http\Middleware;

use Session;
use Auth;
use Closure;

class Host
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->type != '2')
        {
            Session::flash('info', 'You Dont Have Permission to Access the Page!');
            return back();
        }
        return $next($request);
    }
}
