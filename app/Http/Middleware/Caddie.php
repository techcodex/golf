<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class Caddie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->type != '1')
        {
            Session::flash('info', 'You Dont Have Permission to Access the Page!');
            return back();
        }
        return $next($request);
    }
}
