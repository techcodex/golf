<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    protected $guarded = [];

    protected $casts = [
        'detail' => 'array'
    ];

    public function caddie()
    {
        return $this->belongsTo('App\Caddie');
    }
}
