<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    //
    protected $fillable = ['name'];

    public function hostProperty() {
        return $this->hasMany('App\Space');
    }
}
