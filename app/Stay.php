<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stay extends Model
{
    protected $fillable = [
        'status','availability_id','availability_summary','event_id','event_name','event_start_date','event_end_date',
        'host_id','caddie_id','host_name','caddie_name','stay_start_date','stay_end_date','stay_notes','created_by','updated_by',
        'host_property_id'
    ];
    protected $dates = [
        'stay_start_date','stay_end_date',
    ];
    public function caddie() {
        return $this->belongsTo('App\User','caddie_id');
    }
    public function event() {
        return $this->belongsTo('App\Event');
    }
    public function host() {
        return $this->belongsTo('App\User','host_id');
    }
    public function availability() {
        return $this->belongsTo('App\Availability');
    }
    public function hostProperty() {
        return $this->belongsTo('App\HostProperty');
    }
}
