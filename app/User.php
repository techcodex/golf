<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'type', 'home_number','cell_number', 'profile_picture', 'email', 'password', 'created_by', 'updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['logged_at'];

    public function caddie() {
        return $this->hasOne('App\Caddie');
    }

    public function agreement()
    {
        return $this->hasManyThrough('App\Agreement', 'App\Caddie');
    }

    public function hostProperties()
    {
        return $this->hasMany('App\HostProperty', 'owner_id');
    }
    public function stays() {
        return  $this->hasMany('App\Stay','caddie_id');
    }
    public function reservation() {
        $now = date("Y-m-d H:i:s");
//        dd($now);
        return $this->hasOne('App\Stay','caddie_id')->where('stay_end_date','>',$now);
    }
}
