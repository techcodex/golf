<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    protected $fillable = [
        'status','host_property_id','display_name','host_id','host_city','host_street','event_name','event_id','start_date','end_date'
        ,'comment','created_by','updated_by'
    ];
    public function event() {
        return $this->belongsTo('App\Event');
    }
    public function host_property() {
        return $this->belongsTo('App\HostProperty');
    }
    public function host() {
        return $this->belongsTo('App\User');
    }
    protected $dates = ['start_date','end_date'];


}
