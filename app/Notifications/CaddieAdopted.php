<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CaddieAdopted extends Notification
{
    use Queueable;
    private $caddie;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($caddie)
    {
        $this->caddie = $caddie;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Caddie Adopted')
            ->line('You Have Adopt the ' . $this->caddie->user->first_name . ' ' . $this->caddie->user->last_name)
            ->action('View Dashboard', route('caddie.dashboard'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' =>  'You Have Adopt the ' . $this->caddie->user->first_name . ' ' . $this->caddie->user->last_name,
            'url' => route('host.dashboard'),
            'icon' => 'fa fa-user-circle'
        ];
    }
}
