<?php

namespace App\Notifications;

use App\HostProperty;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PropertyRegistered extends Notification
{
    use Queueable;
    private $property;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(HostProperty $property)
    {
        $this->property = $property;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Property Registered on Casa de Caddie')
                    ->view('email.property-register', ['property' => $this->property]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => 'You have registered your property!',
            'url' => route('host.index'),
            'icon' => 'fa fa-home'
        ];
    }
}
