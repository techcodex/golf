<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserRegistered extends Notification
{
    use Queueable;
    private $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $type = $this->user->type == 1 ? 'Caddie' : 'Host';
        $path = $type == 'caddie' ? 'caddie.dashboard' : 'host.dashboard';
        return (new MailMessage)
                        ->subject('Casa de Caddie Registration!')
                        ->greeting('Thanks for Registering on Casa de Caddie')
                        ->line('Hi ' . $this->user->first_name . ',')
                        ->line("We're excited to help you grow as " . $type)
                        ->action('Get Started', route($path))
                        ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => 'Welcome to Casa de Caddie! Thanks for using our application.',
            'url' => $this->user->type == 1 ? route('caddie.dashboard') : route('host.dashboard'),
            'icon' => 'fa fa-user'
        ];
    }
}
