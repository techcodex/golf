<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class HostAdoptCaddie extends Notification
{
    use Queueable;
    private $host;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($host)
    {
        $this->host = $host;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Adopted')
            ->line('Your Have been Adopt by ' . $this->host->first_name . ' ' . $this->host->last_name)
            ->action('View Dashboard', route('caddie.dashboard'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' =>  'Your Have been Adopt by ' . $this->host->first_name . ' ' . $this->host->last_name,
            'url' => route('caddie.dashboard'),
            'icon' => 'fa fa-user-circle'
        ];
    }
}
