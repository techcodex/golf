<?php

namespace App\Paypal;

use Carbon\Carbon;
use PayPal\Api\Plan;
use PayPal\Api\Payer;
use PayPal\Api\Agreement;
use App\Paypal\SubscriptionPlan;

class PaypalAgreement extends Paypal
{
    public function create($planId)
    {

        $agreement = new Agreement();
        $subscriptionPlan = new SubscriptionPlan();
        $plan = $subscriptionPlan->getDetail($planId);

        // calculate date
       $now = $this->calculateDate($plan);

        $agreement->setName($plan->name)
            ->setDescription($plan->description)
            ->setStartDate($now->format(\Datetime::ISO8601));

        $plan = new Plan();
        $plan->setId($planId);
        $agreement->setPlan($plan);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);

        $agreement = $agreement->create($this->apiContext);
        $approvalUrl = $agreement->getApprovalLink();
        return $approvalUrl;
    }

    private function calculateDate($plan)
    {
        $now = Carbon::now();
        $frequency = $plan->payment_definitions[0]->frequency;
        $frequencyInterval = $plan->payment_definitions[0]->frequency_interval;
        if($frequency == 'Month')
        {
            $now->addMonths($frequencyInterval);
        }
        else if($frequency == 'Year')
        {
            $now->addYears($frequencyInterval);
        }
        else
            abort(404);

        return $now;
    }

    public function exceute($token)
    {
        $agreement = new Agreement();
        $agreement->execute($token, $this->apiContext);
        return $agreement;
    }

    public function getAgreement($id)
    {
        return Agreement::get($id, $this->apiContext);
    }
}
