<?php
/**
 * Created by PhpStorm.
 * User: SohaiB
 * Date: 4/10/2019
 * Time: 7:49 PM
 */

namespace App\Paypal;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use phpDocumentor\Reflection\Types\Parent_;

class Checkout extends Paypal
{
    public function create() {
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
        $item1 = new Item();
        $item1->setName('Ground Coffee 40 oz')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku("123123") // Similar to `item_number` in Classic API
            ->setPrice(7.5);



        $details = new Details();
        $details->setShipping(1.2)
            ->setTax(1.3)
            ->setSubtotal(17.50);
        $itemList = new ItemList([$item1]);
        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal(20)
            ->setDetails($details);


        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        $base_url = "http://".$_SERVER['HTTP_HOST']."/";
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("http://golf.test/ExecutePayment.php?success=true")
            ->setCancelUrl("http://golf.test/ExecutePayment.php?success=false");


        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $request = clone $payment;
        $payment->create($this->apiContext);

        $approvalUrl = $payment->getApprovalLink();


        return $payment;
    }
}