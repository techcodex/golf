<?php

namespace App\Paypal;

use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\Currency;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\MerchantPreferences;

class SubscriptionPlan extends Paypal
{

    public function list()
    {
        $params = array('page_size' => '10', 'status' => 'ACTIVE');
        $planList = Plan::all($params, $this->apiContext);
        return $planList;
    }

    public function deletePlan($id)
    {
        $plan = $this->getDetail($id);
        $result = $plan->delete($this->apiContext);
        return $result;
    }

    public function getDetail($id)
    {
        return Plan::get($id, $this->apiContext);
    }

    public function activate($plan)
    {
        $createdPlan = $plan;
        $patch = new Patch();

        $value = new PayPalModel('{
            "state":"ACTIVE"
            }');

        $patch->setOp('replace')
            ->setPath('/')
            ->setValue($value);
        $patchRequest = new PatchRequest();
        $patchRequest->addPatch($patch);

        $createdPlan->update($patchRequest, $this->apiContext);

        return $createdPlan;
    }

    public function create($data)
    {
        $plan = $this->createPlan($data);

        $paymentDefinition = $this->createPaymentDefinition($data);

        $merchantPreferences = $this->createMerchantPreferences($data);

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        $output = $plan->create($this->apiContext);

        $updatePlan = $this->activate($output);

        return $updatePlan;
    }

    protected function createPlan($data)
    {
        $plan = new Plan();

        $plan->setName($data['title'])
            ->setDescription($data['description'])
            ->setType('INFINITE');
        return $plan;
    }

    protected function createPaymentDefinition($data)
    {
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Regular Payments')
            ->setType('REGULAR')
            ->setFrequency($data['frequency'])
            ->setFrequencyInterval($data['interval'])
            ->setCycles(0)
            ->setAmount(new Currency(array('value' => $data['price'], 'currency' => 'USD')));
        return $paymentDefinition;
    }

    protected function createMerchantPreferences($data)
    {
        $merchantPreferences = new MerchantPreferences();

        $merchantPreferences->setReturnUrl(config('services.paypal.url.success'))
            ->setCancelUrl(config('services.paypal.url.cancel'))
            ->setAutoBillAmount("yes")
            ->setInitialFailAmountAction("CONTINUE")
            ->setMaxFailAttempts("0")
            ->setSetupFee(new Currency(array('value' => $data['price'], 'currency' => 'USD')));

        return $merchantPreferences;
    }
}
