<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = [];

    protected $dates = ['read_at'];

    public function fromContact()
    {
        return $this->belongsTo('App\User', 'from', 'id');
    }
}
