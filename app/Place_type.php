<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place_type extends Model
{
    //
    protected $fillable = ['name'];
    public function host_property() {
        return $this->hasMany('App\HostProperty');
    }
}
