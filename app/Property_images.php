<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property_images extends Model
{
    //
    protected $fillable = [
        'image','host_property_id'
    ];
    public function HostProperty() {
        return $this->belongsTo('App\HostProperty');
    }
}
