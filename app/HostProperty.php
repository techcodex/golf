<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostProperty extends Model
{
    protected $fillable = ['status', 'owner_id', 'city',
        'owner_name', 'host_phone', 'host_email', 'street_address',
        'condo_apt_no', 'city', 'state_id', 'zip_code', 'country_id', 'pet',
        'smoke', 'comment', 'created_by', 'updated_by','place_type_id','space_id',
        'accommodates'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function place_type() {
        return $this->belongsTo('App\Place_type');
    }
    public function space() {
        return $this->belongsTo('App\Space');
    }
    public function availability() {
        return $this->hasOne('App\Availability');
    }
    public function propertyImages() {
        return $this->hasMany('App\Property_images');
    }
    public function country() {
        return $this->belongsTo('App\Country');
    }
    public function state() {
        return $this->belongsTo('App\State');
    }
    public function stays() {
        return $this->hasMany('App\Stay');
    }
}
