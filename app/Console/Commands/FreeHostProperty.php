<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Stay;

class FreeHostProperty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:FreeHostProperty';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = date("Y-m-d H:i:s");
        $stays = Stay::where('event_end_date','<',$now)->get();
        if($stays->count() !=  0) {
            foreach ($stays as $stay) {
                $host_property = $stay->hostProperty;
                $host_property->status = 1;
                $host_property->save();
            }
        }

    }
}
