<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'name','type','source','start_date','end_date','country_club_name','country_id','city','state_id','country_club_id','created_by','updated_by','event_image','slug'
    ];
    public function scopeFutureEvent($query)
    {
        return $query->whereDate('start_date', '>=', Carbon::now());
    }
    public function state() {
        return $this->belongsTo('App\State');
    }
    public function country() {
        return $this->belongsTo('App\Country');
    }
    protected $dates = ['start_date','end_date'];

    public function availabilities() {
        return $this->hasMany('App\Availability');
    }
    public function properties() {
        return $this->hasMany('App\HostProperty');
    }
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function availableProperties() {
        return $this->hasMany('App\Availability')->where('status','=','1');
    }
    public function stays() {
        return $this->hasMany('App\Stay');
    }
}
