<?php
/*
|--------------------------------------------------------------------------
| Caddie Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Caddie routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Caddie dashboard and settings
Route::get('caddie_account','CaddiesController@create')->name('caddie.account');
Route::post('store_caddie_account','CaddiesController@store')->name('caddie.store');
Route::get('dashboard','CaddiesController@dashboard')->name('caddie.dashboard');
Route::get('notifications','CaddiesController@notification')->name('caddie.notification');

//change password
Route::get('change_password', 'UsersController@changePassword')->name('caddie.change.password');
Route::post('change_password', 'UsersController@changePasswordStore')->name('caddie.change.password.store');

// Plan & Subscription Controller
Route::get('pricing', 'SubscriptionController@index')->name('pricing');


//trips
Route::get('caddie/trips','TripsController@index')->name('caddie.trips');

//trips
Route::get('caddie/listing','TripsController@listing')->name('caddie.listing');

//reservation
Route::get('caddie/reservation','TripsController@reservation')->name('caddie.reservation');
