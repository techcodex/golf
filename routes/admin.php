<?php
/*
|--------------------------------------------------------------------------
| Caddie Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Caddie routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/dashboard','AdminPageController@dashboard')->name('admin.dashboard');

//Users route
Route::get('/users/add_user','UsersController@create')->name('user.create');
Route::post('/users/store_user','UsersController@store')->name('user.store');
Route::get('/users/users_list','UsersController@index')->name('user.users_list');
Route::get('/users/get_users','UsersController@get_users')->name('users.get_users');
Route::post('/users/deactive/{id}','UsersController@deactive')->name('users.deactive');
Route::post('/users/active/{id}','UsersController@active')->name('users.active');
Route::get('users/edit_user/{id}','UsersController@edit')->name('user.edit');
Route::post('users/update/{id}','UsersController@update')->name('user.update');

//caddies
Route::get('users/caddies','CaddiesController@index')->name('caddie.index');
Route::get('user/caddies_list','CaddiesController@caddies_list')->name('caddie.list');

Route::post('user/active_caddie/{id}','CaddiesController@activeCaddieProfile')->name('caddie.active');
Route::post('user/deactive_caddie/{id}','CaddiesController@deactiveCaddieProfile')->name('caddie.deactive');

Route::get('user/caddie_edit/{id}','CaddiesController@edit')->name('caddie.edit');
Route::post('user/caddie_update/{id}','CaddiesController@update')->name('caddie.update');

//Caddie featured
Route::post('user/featured_caddie/{id}','CaddiesController@featured')->name('admin.caddie.featured');
Route::post('user/disable_caddie/{id}','CaddiesController@disable')->name('admin.caddie.disable');

// Clubs controller
Route::get('clubs/get_clubs','ClubsController@index')->name('club.get_clubs');

//host
Route::get('users/hosts','HostController@index')->name('host.index');
Route::get('user/host_list','HostController@host_list')->name('host.host_list');

//events route
Route::get('/events/add_event','EventsController@create')->name('event.create');
Route::post('/events/store','EventsController@store')->name('event.store');
/* Show All events view Routes and api route to get All Events*/
Route::get('/events','EventsController@index')->name('event.index');
Route::get('/events/get_events','EventsController@get_events')->name('events.get_events');

/* Show All pga events view Routes and api route to get All pga Events*/
Route::get('events/pga','EventsController@pga')->name('admin.event.pga');
Route::get('events/get_pga_events','EventsController@pgaEvents')->name('event.get_pga_events');

/* Show All lpga events view Routes and api route to get All lpga Events*/
Route::get('events/lpga','EventsController@lpga')->name('admin.event.lpga');
Route::get('events/get_lpga_events','EventsController@lpgaEvents')->name('event.get_lpga_events');

Route::get('event/{id}/destroy', 'EventsController@destroy')->name('event.destroy');
//Ending Events Route



// blog routes
Route::get('/blog/create', 'BlogController@create')->name('blog.create');
Route::post('/blog/store', 'BlogController@store')->name('blog.store');
Route::get('/blogs', 'BlogController@index')->name('blog.index');
Route::get('/blog/{id}/edit', 'BlogController@edit')->name('blog.edit');
Route::post('/blog/{id}/update', 'BlogController@update')->name('blog.update');
Route::get('/blog/{id}/destroy', 'BlogController@destroy')->name('blog.destroy');

// Datatable Blog
Route::get('/blogs/get_blogs', 'BlogController@get_blogs')->name('blogs.get_blogs');

//geting states
//Route::post('/get_states','Locations@get_states')->name('get_states');

// Plan Routes
Route::get('plan/create', 'PlanController@create')->name('plan.create');
Route::post('plan/store', 'PlanController@store')->name('plan.store');
Route::get('plan/{id}/delete', 'PlanController@destroy')->name('plan.destroy');
Route::get('plans', 'PlanController@index')->name('plan.index');
Route::get('plan/{id}', 'PlanController@show')->name('plan.show');

// Bulk Message Routes
Route::get('/message/broadcast', 'MessageController@bulkMessage')->name('admin.message');
Route::post('/message/broadcast', 'MessageController@bulkSend')->name('admin.message.send');

//Availability

Route::get('/availability/','AvaialabilityController@indexAdmin')->name('admin.availability.index');
Route::get('/get-availabilities/','AvaialabilityController@getAllAvailabilities')->name('admin.availability.getAvailabilities');
Route::post('/availability/deactive','AvaialabilityController@deactiveByAdmin')->name('admin.availability.deactive');
Route::post('/availability/active','AvaialabilityController@activeByAdmin')->name('admin.availability.active');

//stay

Route::get('/stay','StaysController@index')->name('admin.stay.index');
Route::get('/stay/list','StaysController@getStays')->name('admin.stay.getStays');

// Setting Routes
Route::get('profile/settings', 'SettingsController@create')->name('admin.profile.settings');
Route::post('profile/settings/email', 'SettingsController@email')->name('admin.update.email');
Route::post('profile/settings/name', 'SettingsController@name')->name('admin.update.name');
Route::post('profile/settings/password', 'SettingsController@password')->name('admin.update.password');

