<?php

use App\Message;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Home Pages Routes
Route::get('/','PagesController@index')->name('home');
Route::get('/contact','PagesController@contact')->name('contact');
Route::post('/contact/store','PagesController@store')->name('contact.store');

Route::get('/aboutus','PagesController@aboutus')->name('aboutus');
Route::get('/events','PagesController@events')->name('events');
Route::get('/terms-condition', 'PagesController@terms')->name('terms.condition');

Route::get('/blog','PagesController@blog')->name('blog');

Route::get('/blogs','PagesController@blog')->name('blog');
Route::get('/blog/{slug}', 'BlogController@show')->name('blog.show');

Route::get('events/calendar','CalendarsController@index')->name('event.calendar');
Route::get('events/calendar/{id}','EventsController@show')->name('event.show');

Route::get('events/pga/','CalendarsController@pgaEvents')->name('event.pga');
Route::get('events/lpga/','CalendarsController@lpgaEvents')->name('event.lpga');
Route::get('events/events-list','EventsController@eventsList')->name('event.list');

//availablility
Route::get('availabilites/host-available-properties/{id}','AvaialabilityController@getAvailableProperties')->name('web.available_properties');
Route::get('availabilities/property/{id}','AvaialabilityController@show')->name('web.avaialability.show');


//Stay //middleware subscribe need to be attach
Route::post('stay/store','StaysController@store')->name('stay.store');
Route::get('stay/nearest_stay/{id}','StaysController@nearestStay')->name('stay.nearestStay');


//User Edit Ruotes
Route::get('users/edit/{id}','UsersController@edit')->name('web.user.edit')->middleware(['auth', 'verified']);
Route::post('users/update/{id}','UsersController@update')->name('web.user.update')
    ->middleware(['auth', 'verified']);

//Auth Routes
Auth::routes(['verify' => true]);

Route::get('/dashboard', 'HomeController@index')->name('dashboard');


//Admin Login Page
Route::get('/admin/login','AdminPageController@index')->middleware('guest');

Route::post('/get_states','Locations@get_states')->name('get_states');

// Message Controller
Route::any('/messenger', 'MessageController@index')->where('any', '.*')->name('messenger')->middleware('auth');
Route::get('contacts', 'ContactController@index')->name('contacts')->middleware('auth');
Route::get('messages/{id}', 'MessageController@show')->name('contact.message')->middleware('auth');
Route::post('message/{id}/send', 'MessageController@store')->name('message.send')->middleware('auth');


//adopt a caddie
Route::get('/adopt_caddie','PagesController@adoptCaddie')->name('adoptCaddie');

Route::get('/checkout','CheckoutController@index');

Route::get('/ExecutePayment','CheckoutController@execute');


//Subscription Routes
Route::get('plan/{id}/agreement', 'SubscriptionController@create')->name('plan.agreement')->middleware('auth');
Route::get('paypal/exceute-agreement/{status}', 'SubscriptionController@agreement')->name('agreement.store')->middleware('auth');

//Route::get('/ExecutePayment','CheckoutControoler@execute');

