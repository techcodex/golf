<?php
/*
|--------------------------------------------------------------------------
| Host Routes
|--------------------------------------------------------------------------
|
| Here is where you can register host routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    // Host Property CRUD
Route::get('host-properties', 'HostPropertyController@index')->name('web.host.index');
Route::get('host-properties/{hostProperty}/delete', 'HostPropertyController@destroy')->name('host.destroy');
Route::get('/host-property/create', 'HostPropertyController@create')->name('host.create_property');
Route::post('/host-property/store', 'HostPropertyController@store')->name('host.store_property');
// Avaiablility CRUD
Route::get('host-property/add-avaialability', 'AvaialabilityController@create')->name('web.avaiable.create');
Route::post('host-property/add-avaialability', 'AvaialabilityController@store')->name('web.avaiable.store');

// dashbaord
Route::get('dashboard', 'HostController@dashboard')->name('host.dashboard');
Route::get('notifications', 'HostController@notification')->name('host.notification');

//change password
Route::get('change_password', 'UsersController@changePassword')->name('host.change.password');
Route::post('change_password', 'UsersController@changePasswordStore')->name('host.change.password.store');

//create host property in dashboard
Route::get('host-property/dashboard/create','HostPropertyController@createDashboard')->name('host.dashboard.create');
Route::get('host-property/dashboard/my_properties','HostPropertyController@indexDashboard')->name('host.dashboard.index');
Route::get('host-property/dashboard/get_properties','HostPropertyController@getPropertiesAjax')->name('host.dashboard.get_properties');
Route::get('host-property/dashboard/delete/{id}','HostPropertyController@destroyAjax')->name('host.dashboard.destroy');
Route::get('host-property/dashboard/edit/{id}','HostPropertyController@editDashboard')->name('hostproperty.dashboard.edit');
Route::post('host-property/dashboard/update/{id}','HostPropertyController@updateDashboard')->name('hostproperty.dashboard.update');


//availiility
Route::get('availability/dashboard/','AvaialabilityController@createDashboard')->name('availability.dashboard.create');
Route::post('availability/get_events','AvaialabilityController@getEvents')->name('availability.dashboard.get_events');
Route::get('avaialability/dashboard/index','AvaialabilityController@indexDashboard')->name('avaialability.dashboard.index');
Route::get('avaialability/dashboard/get-availablity','AvaialabilityController@getAvailablity')->name('avaialability.dashboard.getAvailability');
Route::post('avaialability/dashboard/deactive','AvaialabilityController@deactive')->name('avaialability.dashboard.deactive');
Route::post('avaialability/dashboard/active','AvaialabilityController@active')->name('avaialability.dashboard.active');
Route::get('avaialability/dashboard/destroy/{id}','AvaialabilityController@destroy')->name('avaialability.dashboard.destroy');
Route::get('avaialability/dashboard/edit/{id}','AvaialabilityController@edit')->name('avaialability.dashboard.edit');
Route::post('avaialability/dashboard/update/{id}','AvaialabilityController@update')->name('avaialability.dashboard.update');

//list my rooms
Route::get('/listing','HostPropertyController@listMyRooms')->name('host.listing');


// Adopt Caddie Subscription
Route::get('/caddie/subscribe/{caddieID}', 'SubscriptionController@index')->name('host.adopt.caddie');
